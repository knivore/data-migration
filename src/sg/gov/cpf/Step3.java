package sg.gov.cpf;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hibernate.Session;
import org.hibernate.query.Query;

import sg.gov.cpf.hibernate.model.AvivaFileDetails;
import sg.gov.cpf.utilities.DMLogger;
import sg.gov.cpf.utilities.HibernateDatabaseUtil;
import sg.gov.cpf.utilities.MigrationUtil;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.List;
import java.util.Properties;

public class Step3 {

	private JTextField step3_browse_textField;
	private JFormattedTextField step3_xfer_textField;
	private JFormattedTextField step3_reAuth_textField;
	private Properties prop;
	private static final DMLogger logger = new DMLogger(Step3.class);

	public Step3(Properties prop) {
		this.prop = prop;
	}
	
	/**
	 * Initialize the contents of step 2 panel.
	 */
	public void initializeStep3(JTabbedPane tabbedPane) {
		/* Setup Number format */
		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);
		formatter.setCommitsOnValidEdit(true);

		/*
		 * Setting up Panel for Step 3
		 */
		JPanel step3_panel = new JPanel();
		step3_panel.setToolTipText("Upload files to RCube");
		step3_panel.setLayout(null);
		tabbedPane.addTab("<html><body><table width='260'>Step 3: Upload to RCube</table></body></html>", step3_panel);

		JLabel step3_lbl = new JLabel("Step 3: Upload Files From Selected Decrypted File Folder Location");
		step3_lbl.setBounds(200, 19, 480, 19);
		step3_lbl.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step3_panel.add(step3_lbl);

		/* Label & Textbox for File Location */
		JLabel lblDecryptedFileLocation = new JLabel("Decrypted File Location :");
		lblDecryptedFileLocation.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDecryptedFileLocation.setBounds(40, 71, 170, 23);
		step3_panel.add(lblDecryptedFileLocation);

		step3_browse_textField = new JTextField();
		step3_browse_textField.setEnabled(false);
		step3_browse_textField.setBounds(221, 66, 320, 29);
		step3_browse_textField.setColumns(10);
		step3_browse_textField.setToolTipText(lblDecryptedFileLocation.getText());
		step3_panel.add(step3_browse_textField);

		JButton step3_browse_btn = new JButton("Browse to File Location");
		step3_browse_btn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					step3_browse_textField.setText(selectedFile.getAbsolutePath());
					System.out.println(selectedFile.getName());
				}
			}
		});
		step3_browse_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step3_browse_btn.setBounds(575, 64, 186, 31);
		step3_panel.add(step3_browse_btn);


		/* Label & Textbox for Number of Images to Transfer */
		JLabel lblXferImagesCount = new JLabel("No. of Images to Xfer :");
		lblXferImagesCount.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblXferImagesCount.setBounds(40, 120, 170, 23);
		step3_panel.add(lblXferImagesCount);

		step3_xfer_textField = new JFormattedTextField(formatter);
		step3_xfer_textField.setBounds(221, 115, 320, 29);
		step3_xfer_textField.setColumns(10);
		step3_xfer_textField.setToolTipText(lblXferImagesCount.getText());
		step3_panel.add(step3_xfer_textField);


		/* Label & Textbox for Re-Authentication Interval */
		JLabel lblReAuthenticateInterval = new JLabel("RCube Re-Auth Interval :");
		lblReAuthenticateInterval.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblReAuthenticateInterval.setBounds(40, 169, 170, 23);
		step3_panel.add(lblReAuthenticateInterval);

		step3_reAuth_textField = new JFormattedTextField(formatter);
		step3_reAuth_textField.setBounds(221, 164, 320, 29);
		step3_reAuth_textField.setColumns(10);
		step3_reAuth_textField.setToolTipText(lblReAuthenticateInterval.getText());
		step3_panel.add(step3_reAuth_textField);

		/* Execute Button for Step 3 */
		JButton step3_run_btn = new JButton("Execute");
		step3_run_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*TODO:
				 * Step 1: Validate all 3 inputs
				 * 			- Not empty, Valid file location
				 * Step 2: Retrieve records from DB based on number of files to xfer (Include DB pagination)
				 * Step 3: Read folder location and search for the file from the record
				 * Step 4: Check if record & file exists
				 * Step 5: Check process count with re-auth count
				 * 			- If required, re-authenticate with RCube
				 * Step 6: Check process count with RCube Auth count
				 * Step 7: Check if RCube still authenticated
				 * 			- Trigger send file API
				 * 			- Update DB record with API response
				 * 			- Increase DB process count
				 */

				if (!MigrationUtil.validateInput(logger, step3_browse_textField, step3_xfer_textField, step3_reAuth_textField)
						|| !MigrationUtil.validateFileLocation(logger, step3_browse_textField.getText())) {
					return;
				}

				int reAuthLimit = Integer.parseInt(step3_reAuth_textField.getText());
				int intervals = Integer.parseInt(step3_xfer_textField.getText()) / reAuthLimit;

				logger.print("Step 3 File Location: " + step3_browse_textField.getText());
				logger.print("Step 3 - Total No. of Images to Xfer: " + step3_xfer_textField.getText());
				logger.print("Step 3 - Total Xfer rounds: " + intervals + ". Each round will Xfer " + reAuthLimit + " records before re-authenticating with RCube again.");

				for (int i = 0; i <= intervals; i++) {
					//TODO : re-authenticate with RCube
					String OTCSTicket = rCubeAuthentication();

					//TODO Step 2: Retrieve records from DB based on number of files to xfer (Include DB pagination)
					//TODO : Replicate the sql for the 3 tables based on the insurer settings from properties file you are looking to run on
					Session session = HibernateDatabaseUtil.getSessionFactory().openSession();
					Query q = session.createQuery("From T_FILE_DETAILS_AVI WHERE rcubeFileSentDateTime is null");
					q.setMaxResults(reAuthLimit);

					List<AvivaFileDetails> resultList = q.list();
					logger.print("Interval " + intervals + 1 + ". Num of Aviva Details: " + resultList.size());
					for (AvivaFileDetails next : resultList) {
						logger.print("Aviva Detail: " + next);

						//TODO Step 3: Read folder location and search for the file from the record
						//TODO Step 4: Check if record & file exists
						File file = new File(step3_browse_textField.getText() + "/" + next.getFilename());
						/* Check input file existence */
						if (!file.exists()) {
							String errorMsg = "File not found ";
							logger.print("ERROR : " + errorMsg + file.getAbsolutePath());
						} else {
							logger.print("Interval " + intervals + 1 + ". Currently processing " + file.getAbsolutePath() + " to upload to rcube... ");
							//TODO : Upload file to rcube
							if (uploadFileToRCube(file, OTCSTicket)) {
								//TODO : Update DB record after file upload successfully

							}
						}
					} // Finish looping each record
					logger.print("Completed Interval " + intervals + 1 + ". ");
				} // Finish intervals
			}
		});
		step3_run_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step3_run_btn.setBounds(531, 259, 131, 31);
		step3_panel.add(step3_run_btn);

		/* Stop Button for Step 3 */
		JButton step3_stop_btn = new JButton("Stop");
		step3_stop_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Terminate process

			}
		});
		step3_stop_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step3_stop_btn.setBounds(680, 259, 131, 31);
		step3_panel.add(step3_stop_btn);

		/* Export Logs Button for Step 3 */
		JButton step3_export_logs_btn = new JButton("Export Logs");
		step3_export_logs_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Export Logs to txt
				try {
					MigrationUtil.alert("Log exported to " + logger.export());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		step3_export_logs_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step3_export_logs_btn.setBounds(10, 259, 170, 31);
		step3_panel.add(step3_export_logs_btn);

		MigrationUtil.terminateLoggerThread(logger);
	}

	private String rCubeAuthentication() {
		String OTCSTicket = "";
		try {
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost postRequest = new HttpPost(prop.getProperty("rcube_uat") + "/api/v1/auth");
			StringEntity input = new StringEntity("{\"username\":???,\"password\":\"???\"}");
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);

			if (response.getStatusLine().getStatusCode() == 200) {

				BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

				String output;
				logger.print("Response from RCube Server .... \n");
				while ((output = br.readLine()) != null) {
					logger.print(output);
					OTCSTicket = output;
				}

			} else {
				logger.print("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			httpClient.close();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return OTCSTicket;
	}

	private boolean uploadFileToRCube(File file, String OTCSTicket) {
		// Full list of APIs -> https://appworksdeveloper.opentext.com/webaccess/#url=%2Fawd%2Fresources%2Fapis%2Fcs-rest-api-for-cs-16-s&tab=501

		boolean result = false;

		try {
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost postRequest = new HttpPost(prop.getProperty("rcube_uat") + "/api/v1/nodes");
			MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
			reqEntity.addTextBody("type", "144"); //Subtype of the node
			reqEntity.addTextBody("parent_id", "someValue?"); //Parent node ID
			reqEntity.addTextBody("name", "someValue?"); //Name of the node
			reqEntity.addBinaryBody("file", file); //The file for the document
			postRequest.setEntity(reqEntity.build());
			postRequest.addHeader("OTCSTicket", OTCSTicket);

			HttpResponse response = httpClient.execute(postRequest);

			if (response.getStatusLine().getStatusCode() == 200) {

				BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

				String output;
				logger.print("Response from RCube Server .... \n");
				while ((output = br.readLine()) != null) {
					logger.print(output);
				}

				result = true;
			} else {
				logger.print("RCube upload failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			httpClient.close();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
