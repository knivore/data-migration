package sg.gov.cpf;

import java.awt.EventQueue;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import sg.gov.cpf.utilities.EncryptionUtil;

import sg.gov.cpf.utilities.JdbcDatabaseUtil;
import sg.gov.cpf.utilities.MigrationUtil;

public class MigrationApp {

	private JFrame frame;
	private static Logger logger = Logger.getLogger(MigrationApp.class.getName());
	
	public static String insurer;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		String missingInsurerParamMessage = "Please start the program with param AVI, NTUC or GE";

		for(int x = 0; x < args.length; x++) {
			logger.info("args " + x + " : " + args[x]);
		}

		if (!((args.length > 0) && (args[0].equals("AVI") || args[0].equals("NTUC") || args[0].equals("GE") ))) {
			MigrationUtil.alert(missingInsurerParamMessage);
			System.exit(1);
		}else {
			insurer = args[0];
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MigrationApp window = new MigrationApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MigrationApp() {
		Properties prop = readProperties();
		initialize(prop);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Properties prop) {
		ConnectionCredentials credentials;
		frame = new JFrame();
		frame.setBounds(100, 100, 1050, 470);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1050, 431);
		frame.getContentPane().add(tabbedPane);
		
		EncryptionUtil.initialize(null);
		credentials = new ConnectionCredentials();


		/*
		 * Setting up Step 1
		 */
		Step1 step1 = new Step1();
		step1.initializeStep1(tabbedPane);

		/*
		 * Setting up Step 2
		 * 
		 * @Jack: add credentials object reference to step 2
		 */
		Step2 step2 = new Step2(insurer); 
		step2.initializeStep2(tabbedPane);

		/*
		 * Setting up Step 3
		 * 
		 * @Jack: add credentials object reference to step 3
		 */
		Step3 step3 = new Step3(prop);
		step3.initializeStep3(tabbedPane);

		/*
		 * Setting up Configurations
		 */
		Configurations config = new Configurations();
		config.initializeConfig(tabbedPane, credentials);

		
		JdbcDatabaseUtil.setupConnectionUrl(credentials);
		
	}

	private Properties readProperties() {
		Properties prop = new Properties();

		try {
			// reading properties
			URL propUrl = this.getClass().getResource("/application.conf");
			FileInputStream in = new FileInputStream(propUrl.getPath());
			prop.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}

	public class ConnectionCredentials {
		private String databasePw = "";
		private String rCubePw = "";

		public ConnectionCredentials() {
			FileReader reader1;
			FileReader reader2;
			Properties p1 = new Properties();
			Properties p2 = new Properties();
			String encryptedDBPassword = null;
			String encryptedRCPassword = null;
			String encryptedSCKey = null;
			String scKey = null;
			//TODO use try with resources
			try {
				reader1 = new FileReader("configuration.properties");
				p1.load(reader1);
				reader2 = new FileReader("configuration2.properties");
				p2.load(reader2);
				encryptedDBPassword = p1.getProperty("encrypteddbpassword");
				encryptedRCPassword = p1.getProperty("encryptedrcpassword");
				encryptedSCKey = p2.getProperty("encryptedsecretkey");
				reader1.close();
				reader2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(encryptedSCKey != null) {
				scKey = EncryptionUtil.decryptText(encryptedSCKey);
				EncryptionUtil.initialize(scKey);
				if(encryptedDBPassword != null) {
					databasePw = EncryptionUtil.decryptText(encryptedDBPassword);
				}
				if(encryptedRCPassword!= null) {
					rCubePw = EncryptionUtil.decryptText(encryptedRCPassword);
				}
			}
		}

		public String getDatabasePw() {
			return databasePw;
		}

		public void setDatabasePw(String databasePw) {
			this.databasePw = databasePw;
		}

		public String getRCubePw() {
			return rCubePw;
		}

		public void setRCubePw(String rCubePW) {
			this.rCubePw = rCubePW;
		}

	}
}
