package sg.gov.cpf;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;

import sg.gov.cpf.MigrationApp.ConnectionCredentials;
import sg.gov.cpf.utilities.EncryptionUtil;
import sg.gov.cpf.utilities.JdbcDatabaseUtil;

public class Configurations {
	private ConnectionCredentials credentials;
	private JPasswordField dbPasswordField;
	private JPasswordField rCubePasswordField;
	private JPasswordField encryptPasswordField;
	private JButton editBtn;
	private JButton cancelBtn;
	private JButton encryptBtn;
	private boolean editMode;
	private boolean isActiveTab;
	private static final String UI_FONT = "Tahoma";
	
	/**
	 * Initialize the contents of configurations panel.
	 */
	public void initializeConfig(JTabbedPane tabbedPane, ConnectionCredentials credentials) {
		
		
		JPanel configPanel = new JPanel();
		configPanel.setToolTipText("Set Database and RCube connection passwords");
		configPanel.setLayout(null);
		tabbedPane.addTab("<html><body><table width='140'>Configure</table></body></html>", configPanel);
		
		this.credentials = credentials;
		editMode = true;
		isActiveTab = tabbedPane.indexOfComponent(configPanel) == tabbedPane.getSelectedIndex();
		
		/* Label and Text Box for database password */
		JLabel dbPasswordLabel = new JLabel("Database Password");
		dbPasswordLabel.setBounds(80, 50, 150, 20);
		dbPasswordLabel.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		configPanel.add(dbPasswordLabel);
		dbPasswordField = new JPasswordField(this.credentials.getDatabasePw());
		dbPasswordField.setBounds(250, 45, 400, 30);
		dbPasswordField.setFont(new Font(UI_FONT, Font.PLAIN, 18));
		configPanel.add(dbPasswordField);
		
		
		/* Label and Text Box for RCube password */
		JLabel rCubePasswordLabel = new JLabel("RCube Password");
		rCubePasswordLabel.setBounds(80, 100, 150, 20);
		rCubePasswordLabel.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		configPanel.add(rCubePasswordLabel);
		rCubePasswordField = new JPasswordField(this.credentials.getRCubePw());
		rCubePasswordField.setBounds(250, 95, 400, 30);
		rCubePasswordField.setFont(new Font(UI_FONT, Font.PLAIN, 18));
		configPanel.add(rCubePasswordField);
		
		editBtn = new JButton("Save");
		editBtn.setBounds(500, 140, 150, 30);
		editBtn.addMouseListener(new EditAction());
		configPanel.add(editBtn);
		
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(330, 140, 150, 30);
		cancelBtn.addMouseListener(new CancelAction());
		configPanel.add(cancelBtn);
		
		int index = tabbedPane.indexOfComponent(configPanel);
		
		tabbedPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(!isActiveTab && tabbedPane.getSelectedIndex() == index) {
					isActiveTab = true;
				}
				if(isActiveTab && tabbedPane.getSelectedIndex() != index) {
					isActiveTab = false;
					if(editMode)
						revert();
				}
			}
		});
		
		
		/* Label and Text Box for encrypt password */
		JLabel encryptPasswordLabel = new JLabel("Password Encryptor");
		encryptPasswordLabel.setBounds(80, 230, 150, 20);
		encryptPasswordLabel.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		configPanel.add(encryptPasswordLabel);
		encryptPasswordField = new JPasswordField();
		encryptPasswordField.setBounds(250, 225, 400, 30);
		encryptPasswordField.setFont(new Font(UI_FONT, Font.PLAIN, 18));
		configPanel.add(encryptPasswordField);
		
		encryptBtn = new JButton("Encrypt");
		encryptBtn.setBounds(500, 270, 150, 30);
		encryptBtn.addMouseListener(new EncryptAction());
		configPanel.add(encryptBtn);
		
	}
	
	private class EditAction extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
		 editMode = !editMode;
		 dbPasswordField.setEnabled(editMode);
		 rCubePasswordField.setEnabled(editMode);
		 String editBtnText = editMode ? "Save" : "Edit";
		 editBtn.setText(editBtnText);
		 cancelBtn.setVisible(editMode);
		 /* Update passwords */
		 if(!editMode) {
			 credentials.setDatabasePw(String.valueOf(dbPasswordField.getPassword()));
			 credentials.setRCubePw(String.valueOf(rCubePasswordField.getPassword()));
			 
			 
			 JdbcDatabaseUtil.setupConnectionUrl(credentials);
		 }
		}
	}
	
	private class CancelAction extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			revert();
		}
	}
	
	private class EncryptAction extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			String password = String.valueOf(encryptPasswordField.getPassword());
			JOptionPane.showInputDialog("Encrypted text", EncryptionUtil.encryptText(password));
		}
	}
	
	private void revert() {
		editMode = !editMode;
		 /* Restore Passwords */
		 dbPasswordField.setText(credentials.getDatabasePw());
		 rCubePasswordField.setText(credentials.getRCubePw());
		 dbPasswordField.setEnabled(editMode);
		 rCubePasswordField.setEnabled(editMode);
		 String editBtnText = editMode ? "Save" : "Edit";
		 editBtn.setText(editBtnText);
		 cancelBtn.setVisible(editMode);
	}

}
