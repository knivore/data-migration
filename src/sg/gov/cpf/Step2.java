package sg.gov.cpf;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import sg.gov.cpf.controller.ControllerForStep2;
import sg.gov.cpf.utilities.DMLogger;
import sg.gov.cpf.utilities.MigrationUtil;

public class Step2 {

	private JTextField step2_browse_meta_textField;

	private static final DMLogger dmLogger = new DMLogger(Step2.class);
	private static ControllerForStep2 controllerForStep2 = new ControllerForStep2();
	
	private static File metaDataFile = null;
	private JTextField step2_browse_document_root_textField;
	
	public Step2(String insurer) {
		controllerForStep2.setInsurer(insurer);
	}

	/**
	 * Initialize the contents of step 2 panel.
	 * @wbp.parser.entryPoint
	 */
	public void initializeStep2(JTabbedPane tabbedPane) {
		/*
		 * Setting up Panel for Step 2
		 */
		JPanel step2_panel = new JPanel();
		step2_panel.setToolTipText("Validate Header, Detail & Trailer. Save to Database");
		step2_panel.setLayout(null);
		tabbedPane.addTab("<html><body><table width='260'>Step 2: Validate Metadata</table></body></html>", step2_panel);
		
		JLabel step2_lbl = new JLabel("Step 2: Validate Meta File Header, Detail & Trailer. Save to Database");
		step2_lbl.setBounds(190, 19, 480, 19);
		step2_lbl.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_panel.add(step2_lbl);

		/* Label & Textbox for File Location */
		JLabel lblMetaFileLocation = new JLabel("MetaFile Location   :");
		lblMetaFileLocation.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMetaFileLocation.setBounds(54, 71, 141, 23);
		step2_panel.add(lblMetaFileLocation);
		
		step2_browse_meta_textField = new JTextField();
		step2_browse_meta_textField.setEnabled(false);
		step2_browse_meta_textField.setBounds(221, 66, 320, 29);
		step2_browse_meta_textField.setColumns(10);
		step2_browse_meta_textField.setToolTipText(lblMetaFileLocation.getText());

		step2_panel.add(step2_browse_meta_textField);
		
		JButton step2_browse_meta_btn = new JButton("Browse to Meta File Location");
		step2_browse_meta_btn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
		        int returnValue = fileChooser.showOpenDialog(null);
		        if (returnValue == JFileChooser.APPROVE_OPTION) {
		          metaDataFile = fileChooser.getSelectedFile();
		          step2_browse_meta_textField.setText(metaDataFile.getAbsolutePath());
		        }
			}
		});
		step2_browse_meta_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_browse_meta_btn.setBounds(575, 64, 220, 31);
		step2_panel.add(step2_browse_meta_btn);

		/* Execute Button for Step 2 */
		JButton step2_run_btn = new JButton("Execute");
		step2_run_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*TODO: 
				 * Step 1: Validate all inputs
				 * 			- Not empty, Valid file location
				 * Step 2: Open metadata file from location textbox
				 * Step 3: Validate string header & trailer
				 * Step 4: Validate file count
				 * Step 5: Create validator to validate metadata detail
				 * Step 6: Insert into DB
				*/
				
				try {

					boolean processMetaData = true;
					
					if(metaDataFile == null) {
						MigrationUtil.alert("Please select Meta Data File");
						processMetaData = false;
					}

					if(metaDataFile != null && !MigrationUtil.validateFileExtension(dmLogger, metaDataFile, false, "csv")) {
						MigrationUtil.alert("Please ensure that meta data ends with .csv");
						processMetaData = false;
					}

					if(!MigrationUtil.validateFileLocation(dmLogger, false, step2_browse_document_root_textField.getText())) {
						MigrationUtil.alert("Please check that Document Root is selected.");
						processMetaData = false;
					}

					if(processMetaData) {
						String isMetaDataHeaderTrailerFormatValid = controllerForStep2.isMetaDataHeaderTrailerFormatValid(metaDataFile);
						if (!isMetaDataHeaderTrailerFormatValid.isEmpty()) {
							MigrationUtil.alert(isMetaDataHeaderTrailerFormatValid);
							processMetaData = false;
						}

						String countTrailerResult = controllerForStep2.isTrailerDetailCountCorrect(metaDataFile);
						if (!countTrailerResult.isEmpty()) {
							MigrationUtil.alert(countTrailerResult);
							processMetaData = false;
						}
					}
					
					if(processMetaData) {
						controllerForStep2.assignInsurerToAndSnapshotDateToController(metaDataFile);
						controllerForStep2.processMetaData(metaDataFile, step2_browse_document_root_textField.getText());
						MigrationUtil.alert("Meta data has been uploaded. Export master/error tables to view results.");
					}

				} catch (Exception exception) {
					exception.printStackTrace();
					dmLogger.print("In Step2 View : " + exception.toString());
					MigrationUtil.alert(exception.toString());
				}
			}
		});
		step2_run_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_run_btn.setBounds(531, 259, 131, 31);
		step2_panel.add(step2_run_btn);

		
		/* Export Logs Button for Step 2 */
		JButton step2_export_logs_btn = new JButton("Export Logs");
		step2_export_logs_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Export Logs to txt
				try {
					MigrationUtil.alert("Log exported to " + dmLogger.export());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		step2_export_logs_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_export_logs_btn.setBounds(10, 259, 170, 31);
		step2_panel.add(step2_export_logs_btn);

		/* Export DB Master Table Button for Step 2 */
		JButton step2_export_mast_btn = new JButton("Export Master Table");
		step2_export_mast_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Export Master DB Tables to CSV
				//      - T_FILE_DETAILS_AVI
				//      - T_FILE_DETAILS_GE
				//      - T_FILE_DETAILS_NTUC
				
				//TODO, insurer loaded from param or csv?
				try {
					controllerForStep2.exportMasterTable(controllerForStep2.getInsurer(), "./");
					MigrationUtil.alert("Logs have been exported to " + System.getProperty("user.dir") + "/logs/MasterTable");
				} catch (SQLException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		step2_export_mast_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_export_mast_btn.setBounds(10, 295, 170, 31);
		step2_panel.add(step2_export_mast_btn);
		
		/* Export DB Error Table Button for Step 2 */
		JButton step2_export_err_btn = new JButton("Export Error Table");
		step2_export_err_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Export Error DB Tables to CSV
				//      - T_ERR_FILE_DETAILS_AVI
				//      - T_ERR_FILE_DETAILS_GE
				//      - T_ERR_FILE_DETAILS_NTUC
				try {
					controllerForStep2.exportErrorTable(controllerForStep2.getInsurer(), "./");
					MigrationUtil.alert("Logs have been exported to " + System.getProperty("user.dir") + "/logs/ErrorTable");

				} catch (SQLException | IOException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		step2_export_err_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_export_err_btn.setBounds(10, 331, 170, 31);
		step2_panel.add(step2_export_err_btn);
		
		JLabel lblAbcd = new JLabel("Document Root Dir :");
		lblAbcd.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAbcd.setBounds(54, 120, 141, 16);
		step2_panel.add(lblAbcd);
		
		step2_browse_document_root_textField = new JTextField();
		step2_browse_document_root_textField.setBounds(220, 116, 321, 26);
		step2_panel.add(step2_browse_document_root_textField);
		step2_browse_document_root_textField.setColumns(10);
		
		JButton step2_browse_doc_root_btn = new JButton("Browse to Document Root");
		step2_browse_doc_root_btn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		step2_browse_doc_root_btn.setBounds(575, 116, 207, 29);
		step2_panel.add(step2_browse_doc_root_btn);
		
		
		step2_browse_doc_root_btn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Select document root location");
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					step2_browse_document_root_textField.setText(selectedFile.getAbsolutePath());
				}
			}
		});

		MigrationUtil.terminateLoggerThread(dmLogger);
	}
}
