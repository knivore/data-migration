package sg.gov.cpf.utilities;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import sg.gov.cpf.meta.model.RecordDetail;
import sg.gov.cpf.meta.model.RecordDetailError;
import sg.gov.cpf.meta.model.RecordHeader;
import sg.gov.cpf.meta.model.RecordTrailer;

public class RecordMapperUtil {
	
	/**
	 * Maps a Record Detail
			0) recordType
			1) systemId
			2) uploadedBy
			3) fileName
			4) "fileType", //Check permissible file type
			5) "uploadedOn", //YYYYMMDDHH24MMSS
			6) "attachmentType", //Upper case
			7) "policyHolderReferenceId", //NRIC, Passport Number
			8) "policyReferenceId", //Policy Number
			9) "archivedRecordTag", //Y, N
			10) "hash"
	 * 
	 * @param recordFromFile
	 * @return RecordDetail or null if there are missing fields in the record
	 */
	public static RecordDetail mapToRecordDetail(String[] recordFromFile) {
		
		if(doesRecordHaveSufficientFieldsToMap(recordFromFile, RecordDetail.class)) {
			RecordDetail recordDetail = new RecordDetail();
			recordDetail.setRecordType(recordFromFile[0]);
			recordDetail.setSystemId(recordFromFile[(1)]);
			recordDetail.setUploadedBy(recordFromFile[(2)]);
			recordDetail.setFileName(recordFromFile[3]);
			recordDetail.setFileType(recordFromFile[4]);
			recordDetail.setUploadedOn(recordFromFile[5]);
			recordDetail.setAttachmentType(recordFromFile[6]);
			recordDetail.setPolicyHolderReferenceId(recordFromFile[7]);
			recordDetail.setPolicyReferenceId(recordFromFile[8]);
			recordDetail.setArchivedRecordTag(recordFromFile[9]);
			recordDetail.setHash(recordFromFile[10]);
			return recordDetail;
		}else {
			return null;
		}
	}
	
	
	/**
	 * Maps a Record Header
	 * 		0) recordType
	 * 		1) insurerTag
	 * 		2) snapShotDate
	 * @param recordFromFile
	 * @return RecordHeader or null if there are missing fields in the record
	 */
	public static RecordHeader mapToRecordHeader(String[] recordFromFile) {
		if(doesRecordHaveSufficientFieldsToMap(recordFromFile, RecordHeader.class)) {
			RecordHeader recordHeader = new RecordHeader();
			recordHeader.setRecordType(recordFromFile[0]);
			recordHeader.setInsurerTag(recordFromFile[1]);
			recordHeader.setSnapshotDate(recordFromFile[2]);
			return recordHeader;
		}else {
			return null;
		}
	}
	
	/**
	 * Maps a Record Trailer
	 * 		0) recordType 
	 * 		1) recordCount
	 * @param recordFromFile
	 * @return RecordTrailer or null if there are missing fields in the record
	 */
	public static RecordTrailer mapToRecordTrailer(String[] recordFromFile) {
		if(doesRecordHaveSufficientFieldsToMap(recordFromFile, RecordTrailer.class)) {
			RecordTrailer recordTrailer = new RecordTrailer();
			recordTrailer.setRecordType(recordFromFile[0]);
			recordTrailer.setRecordCount(recordFromFile[1]);
			return recordTrailer;
		}else {
			return null;
		}
	}
	
	/**
	 * Checks whether the records have sufficient fields to map.
	 * @param <T>
	 * @param record
	 * @param clazz
	 * @return boolean
	 */
	public static <T> boolean doesRecordHaveSufficientFieldsToMap(String[] record, Class<T> clazz) {
		return record.length >= clazz.getDeclaredFields().length;
	}
	public static Timestamp convertStringToTimeStamp(String dateString, String dateFormat) throws ParseException {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
		LocalDateTime parsedDate = LocalDateTime.parse(dateString, dateTimeFormatter);
		return Timestamp.valueOf(parsedDate);
	}
	
	
//	public static AvivaFileDetails createAviFileDetails(RecordDetail recordDetail) throws ParseException {
//		AvivaFileDetails fileDetails = new AvivaFileDetails();
//
//		fileDetails.setSnapshotDate(new Timestamp(System.currentTimeMillis()));
//		fileDetails.setSystemId(recordDetail.getSystemId());
//		fileDetails.setUploadedBy("Jack");
//		fileDetails.setFilename(recordDetail.getFileName());
//		fileDetails.setFiletype(recordDetail.getFileType());
//		fileDetails.setUploadedOn(new Timestamp(1580434201000L));
//		fileDetails.setAttachmentType(recordDetail.getAttachmentType());
//		fileDetails.setReferenceIdPolicyholder(recordDetail.getPolicyHolderReferenceId());
//		fileDetails.setReferenceIdPolicy(recordDetail.getPolicyReferenceId());
//		fileDetails.setArchivedRecordTag(recordDetail.getArchivedRecordTag());
//		fileDetails.setHash(recordDetail.getHash());
//
//		fileDetails.setRcubeSentFlag(null);
////		fileDetails.setRcubeFileSentDateTime(convertStringToTimeStamp(recordDetail.getUploadedOn(), Constants.META_DATE_FORMAT));
//		fileDetails.setRcubeErrorMessage(null);
//		fileDetails.setRcubeDocumentCreatedCode(-1);
//
////		fileDetails.setCreatedDate(convertStringToTimeStamp(recordDetail.getUploadedOn(), Constants.META_DATE_FORMAT));
//		fileDetails.setCreatedBy("Jack");
////		fileDetails.setUpdatedDate(convertStringToTimeStamp(recordDetail.getUploadedOn(), Constants.META_DATE_FORMAT));
//		fileDetails.setUpdatedBy("Jack");
//
//		return fileDetails;
//	}
//	

	public static RecordDetailError createFileDetailsErrorRecord(RecordDetail recordDetail) {

		RecordDetailError recordDetailError = new RecordDetailError();

		recordDetailError.setSnapShotDate(recordDetail.getSnapShotDate());
		recordDetailError.setSystemId(recordDetail.getSystemId());
		recordDetailError.setFilename(recordDetail.getFileName());
		recordDetailError.setReferenceIdPolicyholder(recordDetail.getPolicyHolderReferenceId());
		recordDetailError.setReferenceIdPolicy(recordDetail.getPolicyReferenceId());

		recordDetailError.setCreatedBy(recordDetail.getCreatedBy());
		recordDetailError.setCreatedDate(recordDetail.getCreatedDate());
		recordDetailError.setUpdatedBy(null);
		recordDetailError.setUpdatedDate(null);

		return recordDetailError;
	}
}
