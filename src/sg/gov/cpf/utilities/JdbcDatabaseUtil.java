package sg.gov.cpf.utilities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sg.gov.cpf.MigrationApp.ConnectionCredentials;
import sg.gov.cpf.batch.UploadMetaData;
import sg.gov.cpf.meta.model.UnsentImages;
import sg.gov.cpf.meta.model.RecordDetail;

/**
 * Creating a JDBC util as there are 
 * problems with date time precision when setting the date time from meta data file via Hibernate.
 * @author jack
 *
 */
public class JdbcDatabaseUtil {
	
	private static String filePathSeperator = MigrationUtil.getSystemPathSeperator();

	private static Logger logger = LogManager.getLogger(JdbcDatabaseUtil.class.getName());

	//TODO convert to ad login
	public static String connectionUrl = "jdbc:sqlserver://192.168.0.112:1433;"  //TODO retrieve from properties
										 + "database=RCube;" //TODO retrieve from properties
										 + "user=jack;" 
										 + "password=12345;" 
										 //+ "encrypt=true;"
										 + "trustServerCertificate=false;" 
										 + "loginTimeout=30;";
	
	//TODO
	//index
	
	public static void setupConnectionUrl(ConnectionCredentials connectionCredentials) {
		connectionUrl = "jdbc:sqlserver://localhost:1433;"  
				 + "database=RCube;" 
				 + "user=jack;" 
				 + "password=" + connectionCredentials.getDatabasePw()+ ";" 
				 + "loginTimeout=30;";
		
		connectionUrl = "jdbc:sqlserver://192.168.0.112:1433;"  
				 + "database=RCube;" 
				 + "user=jack;" 
				 + "password=12345;" 
				 + "loginTimeout=30;";
		
	}

	
	public static void updateImageSentStatus(String tableName, int dbid, char rCubeSentFlag, String errorMessage, int rcubeDocumentCreatedCode) throws SQLException {
		
		String updateSql = "UPDATE " + tableName
					+ " SET RCUBE_SENT_FLAG = ?," //1
					+ " RCUBE_ERR_MSG = ?," 	  //2
					+ " RCUBE_DOCUMENT_CREATED_CODE = ?," //3
					+ " UPDATED_DATE = ?,"       //4
					+ " UPDATED_BY = ?" 		  //5
					+ " WHERE DBID = ?"; 		  //6

		try (Connection connection = DriverManager.getConnection(connectionUrl);
				PreparedStatement preparedStatement  = connection.prepareStatement(updateSql);) {

			logger.info("updateSql " + updateSql);

			preparedStatement.setString(1, String.valueOf(rCubeSentFlag));
			preparedStatement.setString(2, errorMessage);
			preparedStatement.setInt(3, rcubeDocumentCreatedCode);
			preparedStatement.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));
			preparedStatement.setString(5, "HCD"); 
			preparedStatement.setInt(6, dbid);
			int rowsAffected = preparedStatement.executeUpdate();
		}	
		logger.info("End updateImageSentStatus" );
	}
	
	
	public static List<UnsentImages> retrieveUnsentImages(int noOfRecords, String tableName) throws SQLException{

		ResultSet resultSet = null;

		List<UnsentImages> dbids = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(connectionUrl);
				Statement statement = connection.createStatement();
						) {

			String selectSql = "SELECT TOP (" + noOfRecords + ") DBID, ATTACHMENT_TYPE, FILENAME"
					+ " FROM " + tableName 
					+ " WHERE RCUBE_SENT_FLAG IS NULL OR RCUBE_SENT_FLAG = 'N'";

			logger.info("selectSql " + selectSql);

			resultSet = statement.executeQuery(selectSql);

			while (resultSet.next()) {
				UnsentImages rcubeRecordToUpload = new UnsentImages();
				
				rcubeRecordToUpload.setDbid(resultSet.getInt(1)); //dbid
				rcubeRecordToUpload.setAttachmentType(resultSet.getString(2)); 
				rcubeRecordToUpload.setFileName(resultSet.getString(3)); 
				dbids.add(rcubeRecordToUpload); 
			}
			logger.info("End retrieveUnsentImagesDbid " );

		}	
		
		return dbids;
	}
	

	public static void insertRecordDetailErrorBatch(List<Integer> recordDetailErrorBatchIndexes, List<String> metaDataRecords, String snapShotDate, String tableName) throws SQLException, ParseException {

		String insertStatement = "INSERT INTO " +   tableName
			    + "(SNAPSHOT_DATE, "     			//1
				+ "SYSTEM_ID, " 					//2
				+ "FILENAME, " 						//3
				+ "REFERENCE_ID_POLICYHOLDER, " 	//4
				+ "REFERENCE_ID_POLICY, " 			//5
				+ "ERROR_RECORD, " 					//6
				+ "ERROR_MESSAGE, " 				//7
				+ "CREATED_DATE, " 					//8
				+ "CREATED_BY, " 					//9
				+ "UPDATED_DATE, " 					//10
				+ "UPDATED_BY) " 					//11
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try (Connection connection = DriverManager.getConnection(connectionUrl);
				PreparedStatement pstmt = connection.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);) {
		
			final int BATCH_SIZE = 10000;
		
			connection.setAutoCommit(false);
			for (int x = 0; x <= recordDetailErrorBatchIndexes.size() - 1; x++) {
		
				String[] detokenizedMetaDataRecord = metaDataRecords.get(recordDetailErrorBatchIndexes.get(x)).split(Constants.CSV_DELIMITER_FOR_SPLIT);
				RecordDetail recordDetail = RecordMapperUtil.mapToRecordDetail(detokenizedMetaDataRecord);
				recordDetail.setSnapShotDate(snapShotDate);
				recordDetail.setCreatedDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.META_DETAIL_DATE_FORMAT)));
				recordDetail.setCreatedBy("HCD"); 
				
				//Error Summary
				StringBuilder errorStringBuilder = new StringBuilder();
				errorStringBuilder.append(UploadMetaData.retrieveErrorFromConstraintViolation(UploadMetaData.validator
																												    .validate(recordDetail)));
				
				boolean computedMd5OfRecordDetail = UploadMetaData.isRecordDetailMd5Correct(recordDetail);
				boolean doesFileExists = UploadMetaData.doesFileExists(UploadMetaData.getInsurerImageDocumentRoot() + filePathSeperator + recordDetail.getFileName());
				
				if (!computedMd5OfRecordDetail) {
					errorStringBuilder.append(Constants.CSV_DELIMITER);
					errorStringBuilder.append("INCORRECT MD5 VALUE");
				}				
				if (!doesFileExists) {
					errorStringBuilder.append(Constants.CSV_DELIMITER);
					errorStringBuilder.append("NON EXISTENT PHYSICAl FILE");
				}
				//TODO Kok bin increase Rec error to 1500
				//End Error summary
		
				pstmt.setTimestamp(1, convertDateStringToTimeStamp(recordDetail.getSnapShotDate(), Constants.META_HEADER_DATE_FORMAT));
				pstmt.setString(2, recordDetail.getSystemId());
				pstmt.setString(3, recordDetail.getFileName());
				pstmt.setString(4, recordDetail.getPolicyHolderReferenceId());
				pstmt.setString(5, recordDetail.getPolicyReferenceId());
				pstmt.setString(6, metaDataRecords.get(recordDetailErrorBatchIndexes.get(x))); //error record
				pstmt.setString(7, errorStringBuilder.toString()); //error message
				pstmt.setTimestamp(8, convertDateTimeStringToTimeStamp(recordDetail.getCreatedDate(), Constants.META_DETAIL_DATE_FORMAT));
				pstmt.setString(9, recordDetail.getCreatedBy()); 
				pstmt.setTimestamp(10, null);
				pstmt.setString(11, null);
				
				pstmt.addBatch();
				
				if (x % BATCH_SIZE == BATCH_SIZE - 1)
				    pstmt.executeBatch();	
				
			}
			
			if(recordDetailErrorBatchIndexes.size() % BATCH_SIZE != 0) {
				pstmt.executeBatch();
			}

			connection.setAutoCommit(true);

		}
	}

	
	
	/**
	 * Inserts to an insurer file detail table 
	 * @param recordDetailBatchIndex, meta data indexes to read from 
	 * @param metaDataRecords, meta data file contents
	 * @param snapShotDate, meta data snapshot date
	 * @param tableName, insurer's table name, T_AVI_FILE_DETAILS, T_NTUC_FILE_DETAILS, T_GE_FILE_DETAILS
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static void insertRecordDetailBatch(List<Integer> recordDetailBatchIndex, List<String> metaDataRecords, String snapShotDate, String tableName) throws SQLException, ParseException {

		String insertStatement = "INSERT INTO " +   tableName
					    + "(SNAPSHOT_DATE, "     			//1
						+ "SYSTEM_ID, " 					//2
						+ "UPLOADED_BY, " 					//3
						+ "FILENAME, " 						//4
						+ "FILETYPE, " 						//5
						+ "UPLOADED_ON, " 					//6
						+ "ATTACHMENT_TYPE, " 				//7
						+ "REFERENCE_ID_POLICYHOLDER, " 	//8
						+ "REFERENCE_ID_POLICY, " 			//9
						+ "ARCHIVED_RECORD_TAG, " 			//10
						+ "HASH, " 							//11
						+ "RCUBE_SENT_FLAG, " 				//12
						+ "RCUBE_FILE_SENT_DATE_TIME, " 	//13
						+ "RCUBE_ERR_MSG, " 				//14
						+ "RCUBE_DOCUMENT_CREATED_CODE, " 	//15
						+ "CREATED_DATE, " 					//16
						+ "CREATED_BY, " 					//17
						+ "UPDATED_DATE, " 					//18
						+ "UPDATED_BY) " 					//19
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		

		try (Connection connection = DriverManager.getConnection(connectionUrl);
				PreparedStatement pstmt = connection.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);) {

			final int BATCH_SIZE = 10000;

			connection.setAutoCommit(false);
			for (int x = 0; x <= recordDetailBatchIndex.size() - 1; x++) {

				String[] detokenizedMetaDataRecord = metaDataRecords.get(recordDetailBatchIndex.get(x)).split(Constants.CSV_DELIMITER_FOR_SPLIT);
				RecordDetail recordDetail = RecordMapperUtil.mapToRecordDetail(detokenizedMetaDataRecord);
				recordDetail.setSnapShotDate(snapShotDate);
				recordDetail.setCreatedDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.META_DETAIL_DATE_FORMAT)));
				recordDetail.setCreatedBy("HCD");

				pstmt.setTimestamp(1, convertDateStringToTimeStamp(recordDetail.getSnapShotDate(), Constants.META_HEADER_DATE_FORMAT));
				pstmt.setString(2, recordDetail.getSystemId());
				pstmt.setString(3, recordDetail.getUploadedBy());
				pstmt.setString(4, recordDetail.getFileName());
				pstmt.setString(5, recordDetail.getFileType());
				pstmt.setTimestamp(6, convertDateTimeStringToTimeStamp(recordDetail.getUploadedOn(), Constants.META_DETAIL_DATE_FORMAT));
				pstmt.setString(7, recordDetail.getAttachmentType());
				pstmt.setString(8, recordDetail.getPolicyHolderReferenceId());
				pstmt.setString(9, recordDetail.getPolicyReferenceId());
				pstmt.setString(10, recordDetail.getArchivedRecordTag());
				pstmt.setString(11, recordDetail.getHash());
				pstmt.setString(12, null);
				pstmt.setTimestamp(13, null);
				pstmt.setString(14, null);
				pstmt.setInt(15, -1);
				pstmt.setTimestamp(16, convertDateTimeStringToTimeStamp(recordDetail.getCreatedDate(), Constants.META_DETAIL_DATE_FORMAT));
				pstmt.setString(17, recordDetail.getCreatedBy()); 
				pstmt.setTimestamp(18, null);
				pstmt.setString(19, null);
				
				pstmt.addBatch();
				
				if (x % BATCH_SIZE == BATCH_SIZE - 1)
				    pstmt.executeBatch();	
				
			}
			
			if(recordDetailBatchIndex.size() % BATCH_SIZE != 0) {
				pstmt.executeBatch();
			}

			connection.setAutoCommit(true);

		}
	}

	
	public static Timestamp convertDateTimeStringToTimeStamp(String dateString, String dateFormat) throws ParseException {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
		LocalDateTime localDateTime = LocalDateTime.parse(dateString, dateTimeFormatter);
		return Timestamp.valueOf(localDateTime);
		
	}

	public static Timestamp convertDateStringToTimeStamp(String dateString, String dateFormat) throws ParseException {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
		LocalDate localDate = LocalDate.parse(dateString, dateTimeFormatter);
		return Timestamp.valueOf(localDate.atStartOfDay());
		
	}
	

	public static void exportExceptionTable(String tableName, String startDate, String endDate, String destination) throws SQLException, IOException {
		ResultSet resultSet = null;
		
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
		String fileName = "error_table_" + dateTimeFormatter.format(LocalDateTime.now()) + ".csv";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl);
				Statement statement = connection.createStatement();
				BufferedWriter bufferedWrite = new BufferedWriter(new FileWriter(destination + filePathSeperator + fileName))) {


			String selectSql = "SELECT * FROM " + tableName 
					+ " WHERE CREATED_DATE >= '" + startDate + "'"
					+ " AND"
					+ " CREATED_DATE < DATEADD(day, 1, '" + endDate + "')";

			logger.info("Exporting " + tableName);
			logger.info("selectSql " + selectSql);

			resultSet = statement.executeQuery(selectSql);
		
			while (resultSet.next()) {
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append(resultSet.getTimestamp(2)).append("|"); // snapshot date
				stringBuilder.append(resultSet.getString(3)).append("|"); // system id
				stringBuilder.append(resultSet.getString(4)).append("|"); // file name
				stringBuilder.append(resultSet.getString(5)).append("|"); // ref id policyholder
				stringBuilder.append(resultSet.getString(6)).append("|"); // ref id policy
				stringBuilder.append(resultSet.getString(7)).append("|"); // error_record
				stringBuilder.append(resultSet.getString(8)).append("|"); // error_message

				stringBuilder.append(resultSet.getTimestamp(9)).append("|"); // created date
				stringBuilder.append(resultSet.getString(10)).append("|"); // created by
				stringBuilder.append(resultSet.getTimestamp(11)).append("|"); // updated date
				stringBuilder.append(resultSet.getString(12)); // updated by

				stringBuilder.append("\n");

//				Debug
//				logger.info(stringBuilder.toString());
				
				bufferedWrite.write(stringBuilder.toString());
				
				
			}
			logger.info("End Exporting " + tableName);

		}

	}


	public static void exportMasterTable(String tableName, String startDate, String endDate, String destination) throws SQLException, IOException {
		ResultSet resultSet = null;

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
		String fileName = "master_table_" + dateTimeFormatter.format(LocalDateTime.now()) + ".csv";

		try (Connection connection = DriverManager.getConnection(connectionUrl);
			 Statement statement = connection.createStatement();
				BufferedWriter bufferedWrite = new BufferedWriter(new FileWriter(destination + filePathSeperator + fileName))) {

			String selectSql = "SELECT * FROM " + tableName 
					+ " WHERE CREATED_DATE >= '" + startDate + "'"
					+ " AND"
					+ " CREATED_DATE < DATEADD(day, 1, '" + endDate + "')";

			logger.info("Exporting " + tableName);
			logger.info("selectSql " + selectSql);

			resultSet =  statement.executeQuery(selectSql);
			
			while(resultSet.next()) {
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append(resultSet.getTimestamp(2)).append("|"); //snapshot date
				stringBuilder.append(resultSet.getString(3)).append("|"); //system id
				stringBuilder.append(resultSet.getString(4)).append("|"); //uploaded by
				stringBuilder.append(resultSet.getString(5)).append("|"); //file name
				stringBuilder.append(resultSet.getString(6)).append("|"); //file type
				stringBuilder.append(resultSet.getTimestamp(7)).append("|"); //uploaded on
				stringBuilder.append(resultSet.getString(8)).append("|"); //attachment type
				stringBuilder.append(resultSet.getString(9)).append("|"); //ref id policyholder
				stringBuilder.append(resultSet.getString(10)).append("|"); //ref id policy
				stringBuilder.append(resultSet.getString(11)).append("|"); //archived record tag
				stringBuilder.append(resultSet.getString(12)).append("|"); //hash
				stringBuilder.append(resultSet.getString(13)).append("|"); //rcube sent flag
				stringBuilder.append(resultSet.getTimestamp(14)).append("|"); //rcube file sent date time
				stringBuilder.append(resultSet.getString(15)).append("|"); //rcube error message
				stringBuilder.append(resultSet.getString(16)).append("|"); //rcube document created code
				stringBuilder.append(resultSet.getTimestamp(17)).append("|"); //created date
				stringBuilder.append(resultSet.getString(18)).append("|"); //created by
				stringBuilder.append(resultSet.getTimestamp(19)).append("|"); //updated date
				stringBuilder.append(resultSet.getString(20)); //updated by

				stringBuilder.append("\n");

//Debug
//				logger.info(stringBuilder.toString());
				bufferedWrite.write(stringBuilder.toString());
				
			}
			logger.info("End Exporting " + tableName);

		}
		
	}




}
