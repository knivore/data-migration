package sg.gov.cpf.utilities;

public class Constants {

	public static final int HEADER = 1;
	public static final int DETAIL = 2;
	public static final int TRAILER = 9;
	public static final String CSV_DELIMITER_FOR_SPLIT = "\\|%\\|";
	public static final String CSV_DELIMITER = "|%|";
	public static final String META_DETAIL_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String META_HEADER_DATE_FORMAT = "yyyyMMdd";
	
	
	public static final String AVI_FILE_DETAILS_TABLE = "T_AVI_FILE_DETAILS";
	public static final String GE_FILE_DETAILS_TABLE = "T_GE_FILE_DETAILS";
	public static final String NTUC_FILE_DETAILS_TABLE = "T_NTUC_FILE_DETAILS";
	
	public static final String AVI_ERROR_FILE_DETAILS_TABLE = "T_AVI_ERR_FILE_DETAILS";
	public static final String GE_ERROR_FILE_DETAILS_TABLE = "T_GE_ERR_FILE_DETAILS";
	public static final String NTUC_ERROR_FILE_DETAILS_TABLE = "T_NTUC_ERR_FILE_DETAILS";

	public static final String AVI_INSURER_NAME = "AVI";
	public static final String GE_INSURER_NAME  = "GE";
	public static final String NTUC_INSURER_NAME  = "NTUC";
	
	public static final String CORRESPONDENCE_ATTACHMENT_TYPE  = "CORRESPONDENCE";
	public static final String APPLICATION_ATTACHMENT_TYPE  = "APPLICATION";
	
	public static final String REPORT_TYPE_DETAIL = "DETAIL";
	public static final String REPORT_TYPE_EXCEPTION = "EXCEPTION";
	public static final String REPORT_TYPE_SUMMARY = "SUMMARY";
	public static final String REPORT_TYPE_POSTLOAD = "POSTLOAD";
	public static final String REPORT_TYPE_ALL_DETAIL = "ALL_DETAIL";	
	public static final String REPORT_TYPE_ALL_EXCEPTION = "EXCEPTION";

	public static final int RCUBE_APPLICATION_FOLDER_PARENT_ID = 2519959;
	public static final int RCUBE_CORRESPONDENCE_FOLDER_PARENT_ID = 2520397;	

}
