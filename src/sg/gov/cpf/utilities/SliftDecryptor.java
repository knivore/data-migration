package sg.gov.cpf.utilities;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
/**
 * For multi-threaded decryption of multiple .p7 encrypted files using SLIFT application via windows shell command.
 * @author john
 * @deprecated
 */
public class SliftDecryptor {
	private boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
	private String privateKeyLocation;
	private String decryptCommand;
	private String sliftDirectory;
	private String destDirectory;
	private final DMLogger logger;

	public SliftDecryptor(String privateKeyLocation, String decryptCommand, String destDirectory, DMLogger logger) {
		this.privateKeyLocation = privateKeyLocation;
		this.decryptCommand = decryptCommand;
		this.destDirectory = destDirectory;
		this.logger = logger;
		this.sliftDirectory = null;
		Properties prop = new Properties();
		FileReader reader;
		try {
			reader = new FileReader("configuration.properties");
			prop.load(reader);
			this.sliftDirectory = prop.getProperty("sliftdirectory");
		} catch (IOException e) {
			logger.print(e.getMessage());
		}
		
	}

	public List<String> decryptFiles(List<File> encryptedFiles) {
		List<String> decryptedFiles = new ArrayList<>();
		/* create thread pool based on number of cores to run concurrent SLIFT instances */
		ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		if (!isWindows) {
			logger.print("ERROR: Invalid OS. Decryption can only be performed in Windows.");
			return null;
		}
		String[] files = encryptedFiles.stream().map(file -> file.getAbsolutePath())
				.collect(Collectors.toList()).toArray(new String[0]);
		if (!MigrationUtil.validateFileLocation(logger, files))
			return null;

		List<Callable<Boolean>> decryptTasks = new ArrayList<>();

		for (File f : encryptedFiles) {
			decryptTasks.add(new DecryptTask(f));
		}
		try {
			List<Future<Boolean>> futures = service.invokeAll(decryptTasks);
			for (Future<Boolean> future : futures) {
				String filePath = files[futures.indexOf(future)];
				if(future.get().equals(Boolean.FALSE)) {
					logger.print("ERROR : Failed to decrypt : {0}", filePath);
				} else {
					decryptedFiles.add(filePath);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		service.shutdown();
		return decryptedFiles;
	}

	private class DecryptTask implements Callable<Boolean> {
		private String cmd;
		private String sourceLocation;
		private String destFilePath;

		public DecryptTask(File file) {
			this.sourceLocation = file.getAbsolutePath();
			String fileName = file.getName();
			int extIndex = fileName.lastIndexOf('.');
			this.destFilePath = new File(destDirectory, fileName.substring(0, extIndex)).getAbsolutePath();
			this.cmd = String.format(decryptCommand, sourceLocation, destFilePath, privateKeyLocation, "1234");
			
		}

		@Override
		public Boolean call() throws Exception {
			logger.print("running command >>> " + cmd);
			ProcessBuilder builder = new ProcessBuilder();
			builder.command("cmd.exe", "/c", cmd);
			builder.directory(new File(sliftDirectory));

			try {
				long start = System.currentTimeMillis();
				Process process = builder.start();
				int exitCode = process.waitFor();
				logger.print("process exitCode for " + sourceLocation + " = " + exitCode + ", time taken = "
						+ (System.currentTimeMillis() - start) + "ms");
				if (exitCode == 0)
					return true;

			} catch (Exception e) {
				logger.print(e.getMessage());
				return false;
			}
			return false;
		}
	}

}
