package sg.gov.cpf.utilities;

import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.AES256TextEncryptor;

import com.ibm.icu.text.MessageFormat;
/**
 * For encryption and decryption of credentials
 * @author John
 *
 */
public class EncryptionUtil {
	private static AES256TextEncryptor textEncryptor;
	private static final String KB1 = "1R%s^{1}#%s90H*";
	private static final String KB2 = "pUo2%sTi$=o3:%s";
	private static final String KB3 = "z&6Fo09J{0}%s%s";
	private static final String KB4 = "%s$$s6gopG51??+";
	private static final String KB5 = "$e2%s3{2}!Fd%sd";

	private EncryptionUtil() {
	}
	
	static {
		Properties prop = new Properties();
		try {
			URL propUrl = EncryptionUtil.class.getResource("/application.conf");
			FileInputStream in = new FileInputStream(propUrl.getPath());
			prop.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		textEncryptor = new AES256TextEncryptor();
		textEncryptor.setPassword(jumble());
		String sc = textEncryptor.decrypt(prop.getProperty("encrypted_secret_key"));
		textEncryptor = new AES256TextEncryptor();
		textEncryptor.setPassword(sc);
	}
	
	/**  
	 * for manual encryption
	 * @param key
	 */
	public static void initialize(String key) {

		if (key == null) {
			textEncryptor = new AES256TextEncryptor();
			textEncryptor.setPassword(jumble());
		} else {
			textEncryptor = new AES256TextEncryptor();
			textEncryptor.setPassword(key);
		}

	}
	
	public static String decryptText(String text) {
		if (text.length() >= 64) {
			try {
				return textEncryptor.decrypt(text);
			} catch (EncryptionOperationNotPossibleException e) {
				// decryption failed
			}
		}
		return "";

	}

	public static String encryptText(String text) {
		return textEncryptor.encrypt(text);
	}

	private static String jumble() {
		return MessageFormat.format(String.format(KB5, KB1, KB2, KB3, KB4, KB5), KB2, KB1, KB3, KB4, KB5).substring(3,
				18);
	}

}
