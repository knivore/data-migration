package sg.gov.cpf.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.MTOMFeature;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.opentext.ecm.api.OTAuthentication;
import com.opentext.livelink.service.core.Authentication;
import com.opentext.livelink.service.core.Authentication_Service;
import com.opentext.livelink.service.core.ContentService;
import com.opentext.livelink.service.core.ContentService_Service;
import com.opentext.livelink.service.core.FileAtts;
import com.opentext.livelink.service.docman.DocumentManagement;
import com.opentext.livelink.service.docman.DocumentManagement_Service;
import com.sun.xml.internal.ws.api.message.Header;
import com.sun.xml.internal.ws.api.message.Headers;
import com.sun.xml.internal.ws.developer.JAXWSProperties;
import com.sun.xml.internal.ws.developer.WSBindingProvider;

@SuppressWarnings("restriction")
public class RCubeService {

	private static Logger logger = LogManager.getLogger(RCubeService.class.getName());

	//TODO remove sysout
	public String retrieveAuthenticationToken(String username, String password) {

		// Store the authentication token
		String authToken = null;

		//Call the AuthenticateUser() method to get an authentication token
		try {
			System.out.print("Authenticating User...");
			// Create the Authentication service client
			Authentication_Service authService = new Authentication_Service();
			Authentication authClient = authService.getBasicHttpBindingAuthentication();
			authToken = authClient.authenticateUser(username, password);

			System.out.println("SUCCESS!\n");
		} catch (WebServiceException e) {
			System.out.println("FAILED!\n");
			//System.out.println(e.getFault().getFaultCode() + " : " + e.getMessage());
			logger.warn("In retrieveAuthenticationToken: " + e.getMessage());
		}
		return authToken;
	}

	public String uploadFile(String authToken, String filePath, int parentId) {
		
		// Store the information for the local file
		File file = new File(filePath);

		if (!file.exists())
		{
			System.out.println("ERROR!\n");
			System.out.println("File does not exist at : " + filePath);
			return "File does not exist at : " + filePath;
		}

		// Create the DocumentManagement service client
		DocumentManagement_Service docManService = null;
		DocumentManagement docManClient = null;

		// Create the OTAuthentication object and set the authentication token
		OTAuthentication otAuth = null;

		// We need to manually set the SOAP header to include the authentication token
		try
		{
			docManService = new DocumentManagement_Service();
			docManClient = docManService.getBasicHttpBindingDocumentManagement();

			otAuth = new OTAuthentication();
			otAuth.setAuthenticationToken(authToken);
			
			// The namespace of the OTAuthentication object
			final String ECM_API_NAMESPACE = "urn:api.ecm.opentext.com";

			// Create a SOAP header
			SOAPHeader header = MessageFactory.newInstance().createMessage().getSOAPPart().getEnvelope().getHeader();

			// Add the OTAuthentication SOAP header element
			SOAPHeaderElement otAuthElement = header.addHeaderElement(new QName(ECM_API_NAMESPACE, "OTAuthentication"));

			// Add the AuthenticationToken SOAP element
			SOAPElement authTokenElement = otAuthElement.addChildElement(new QName(ECM_API_NAMESPACE, "AuthenticationToken"));
			authTokenElement.addTextNode(otAuth.getAuthenticationToken());

			// Set the SOAP header on the docManClient
			((WSBindingProvider) docManClient).setOutboundHeaders(Headers.create(otAuthElement));
		}
		catch (SOAPException | WebServiceException e)
		{
			System.out.println("Failed to set authentication SOAP header!\n");
			System.out.println(e.getMessage());
			return "Failed to set authentication SOAP header!\n: " + e.getMessage();
		}

		// Store the context ID for the upload
		String contextID = null;

		// Call the createDocumentContext() method to create the context ID
		try
		{
			System.out.print("Generating context ID...");
			contextID = docManClient.createDocumentContext(parentId, file.getName(), null, false, null);
			System.out.println("SUCCESS!\n");
		}
		catch (SOAPFaultException e)
		{
			System.out.println("Unable to create contextID!\n");
			System.out.println(e.getFault().getFaultCode() + " : " + e.getMessage());
			return "Unable to create contextID!: " + e.getFault().getFaultCode() + " : " + e.getMessage();
		}

		// Create the ContentService client
		// NOTE: ContentService is the only service that requires MTOM support
		ContentService_Service contentService = null;
		ContentService contentServiceClient = null;
		try {
			contentService = new ContentService_Service();
			contentServiceClient = contentService.getBasicHttpBindingContentService(new MTOMFeature());

			// The number of bytes to write in each chunk
			final int CHUNK_SIZE = 10240;

			// Enable streaming and use chunked transfer encoding to send the request body
			// to support large files
			((BindingProvider) contentServiceClient).getRequestContext().put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, CHUNK_SIZE);

		} catch (WebServiceException | UnsupportedOperationException | ClassCastException | NullPointerException | IllegalArgumentException e) {
			System.out.println("Unable to create ContentService!\n");
			System.out.println(e.getMessage());
			return "Unable to create ContentService!: " + e.getMessage();
		}


		// Get the file attributes
		BasicFileAttributes fileAttributes;
		try
		{
			fileAttributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
		}
		catch (IOException e)
		{
			System.out.println("Failed to read file attributes!\n");
			System.out.println(e.getMessage());
			return "Failed to read file attributes!: " + e.getMessage();
		}

		// Create the FileAtts object to send in the upload call
		FileAtts fileAtts = new FileAtts();

		try
		{
			fileAtts.setCreatedDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(fileAttributes.creationTime().toString()));
			fileAtts.setFileName(file.getName());
			fileAtts.setFileSize(file.length());
			fileAtts.setModifiedDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(fileAttributes.lastModifiedTime().toString()));
		}
		catch (DatatypeConfigurationException e)
		{
			System.out.println("Failed to set file attributes!\n");
			System.out.println(e.getMessage());
			return "Failed to set file attributes!: " + e.getMessage();
		}

		// We need to manually set the SOAP headers to include the authentication token, context ID, and file attributes
		try
		{
			// Namespaces for the SOAP headers
			final String ECM_API_NAMESPACE = "urn:api.ecm.opentext.com";
			final String CORE_NAMESPACE = "urn:Core.service.livelink.opentext.com";

			// Create a SOAP header
			SOAPHeader header = MessageFactory.newInstance().createMessage().getSOAPPart().getEnvelope().getHeader();

			// Add the OTAuthentication SOAP header element
			SOAPHeaderElement otAuthElement = header.addHeaderElement(new QName(ECM_API_NAMESPACE, "OTAuthentication"));

			// Add the AuthenticationToken
			SOAPElement authTokenElement = otAuthElement.addChildElement(new QName(ECM_API_NAMESPACE, "AuthenticationToken"));
			authTokenElement.addTextNode(otAuth.getAuthenticationToken());

			// Add the ContextID SOAP header element
			SOAPHeaderElement contextIDElement = header.addHeaderElement(new QName(CORE_NAMESPACE, "contextID"));
			contextIDElement.addTextNode(contextID);

			// Add the FileAtts SOAP header element
			SOAPHeaderElement fileAttsElement = header.addHeaderElement(new QName(CORE_NAMESPACE, "fileAtts"));

			// Add the CreatedDate element
			SOAPElement createdDateElement = fileAttsElement.addChildElement(new QName(CORE_NAMESPACE, "CreatedDate"));
			createdDateElement.addTextNode(fileAtts.getCreatedDate().toString());

			// Add the ModifiedDate element
			SOAPElement modifiedDateElement = fileAttsElement.addChildElement(new QName(CORE_NAMESPACE, "ModifiedDate"));
			modifiedDateElement.addTextNode(fileAtts.getModifiedDate().toString());

			// Add the FileSize element
			SOAPElement fileSizeElement = fileAttsElement.addChildElement(new QName(CORE_NAMESPACE, "FileSize"));
			fileSizeElement.addTextNode(fileAtts.getFileSize().toString());

			// Add the FileName element
			SOAPElement fileNameElement = fileAttsElement.addChildElement(new QName(CORE_NAMESPACE, "FileName"));
			fileNameElement.addTextNode(fileAtts.getFileName());

			// Set the headers on the binding provider
			List<Header> headers = new ArrayList<Header>();
			headers.add(Headers.create(otAuthElement));
			headers.add(Headers.create(contextIDElement));
			headers.add(Headers.create(fileAttsElement));

			((WSBindingProvider) contentServiceClient).setOutboundHeaders(headers);
		}
		catch (SOAPException | WebServiceException e)
		{
			System.out.println("Failed to set SOAP headers!\n");
			System.out.println(e.getMessage());
			return "Failed to set SOAP headers! : " + e.getMessage();
		}

		// Call the UploadContent() method to upload the file
		String objectID = "";
		try
		{
			System.out.print("Uploading document...");
			objectID = contentServiceClient.uploadContent(new DataHandler(new FileDataSource(file)));
			System.out.println("SUCCESS!\n");
			System.out.println("New document uploaded with ID = " + objectID);

		}
//		catch (SOAPFaultException e)
		catch (WebServiceException e)
		{
			System.out.println("SOAPFaultException when uploading document!\n");
//			System.out.println(getFault().getFaultCode() + " : " + e.getMessage());
			System.out.println(e.getMessage());

			return "SOAPFaultException when uploading document!: " + e.getMessage();
		}
		return objectID;
	 
	}
}
