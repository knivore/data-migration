package sg.gov.cpf.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;


/**
 * For multi-threaded decryption of .p7 encrypted files using SLIFT application
 * via windows shell command.
 * 
 * @author John
 *
 */
public class SliftUtil {
	private static Logger logger = Logger.getLogger(SliftUtil.class.getName());
	private static final boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().startsWith("windows");
	private static final String DECRYPT_COMMAND = "sliftc /d \"%s\" \"%s\" /pfx \"%s\" %s";
	private static String privateKeyLocation;
	private static String privateKeyPassword;
	private static String sliftDirectory;

	private SliftUtil() {
	}

	static {
		Properties prop = new Properties();
		try {
			URL propUrl = SliftUtil.class.getResource("/slift.conf");
			FileInputStream in = new FileInputStream(propUrl.getPath());
			prop.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		sliftDirectory = prop.getProperty("slift_directory");
		privateKeyLocation = prop.getProperty("private_key_location");
		privateKeyPassword = prop.getProperty("encrypted_private_key_password");
	}

	/**
	 * Decrypt files using concurrent SLIFT instances
	 * 
	 * @param encryptedFiles
	 * @return
	 */
	public static List<File> runDecryption(List<File> encryptedFiles, String destDirectory) {
		if (!IS_WINDOWS) {
			logger.warning("Please execute in Windows OS.");
			return null;
		}
		/*
		 * Create thread pool based on number of cores to run concurrent SLIFT instances
		 */
		List<File> decryptedFiles = new ArrayList<>();
		ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		String[] files = encryptedFiles.stream().map(file -> file.getAbsolutePath()).collect(Collectors.toList())
				.toArray(new String[0]);
		/* Check file locations */
		if (!MigrationUtil.validateFileLocation(files)) {
			return null;
		}
		
		/* Create callable tasks */
		List<Callable<Boolean>> decryptTasks = new ArrayList<>();
		for (File f : encryptedFiles) {
			decryptTasks.add(new DecryptTask(f, destDirectory));
		}

		/* Invoke all tasks */
		try {
			List<Future<Boolean>> futures = service.invokeAll(decryptTasks);
			for (Future<Boolean> future : futures) {
				File output = encryptedFiles.get(futures.indexOf(future));
				String filePath = output.getAbsolutePath();
				if (future.get().equals(Boolean.FALSE)) {
					logger.log(Level.WARNING, "Failed to decrypt : {0}", filePath);
				} else {
					decryptedFiles.add(output);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		service.shutdown();

		return decryptedFiles;
	}

	private static class DecryptTask implements Callable<Boolean> {
		private String cmd;
		private File sourceFile;
		private String destFilePath;

		public DecryptTask(File file, String destDirectory) {
			this.sourceFile = file;
			String fileName = file.getName();
			int extIndex = fileName.lastIndexOf('.');
			this.destFilePath = new File(destDirectory, fileName.substring(0, extIndex)).getAbsolutePath();
			this.cmd = String.format(DECRYPT_COMMAND, sourceFile.getAbsolutePath(), destFilePath, privateKeyLocation,
					privateKeyPassword);
		}

		@Override
		public Boolean call() throws Exception {
			logger.log(Level.FINE, "running command : {0}", cmd);
			ProcessBuilder builder = new ProcessBuilder();
			builder.command("cmd.exe", "/c", cmd);
			builder.directory(new File(sliftDirectory));

			try {
				long start = System.currentTimeMillis();
				Process process = builder.start();
				int exitCode = process.waitFor();
				logger.fine("process exitCode for " + sourceFile.getAbsolutePath() + " = " + exitCode
						+ ", time taken = " + (System.currentTimeMillis() - start) + "ms");
				if (exitCode == 0) {
					return true;
				}

			} catch (Exception e) {
				logger.warning(e.getMessage());
				return false;
			}
			return false;
		}
	}
}
