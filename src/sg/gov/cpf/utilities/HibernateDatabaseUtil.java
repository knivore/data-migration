package sg.gov.cpf.utilities;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateDatabaseUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
        	
            // Create the SessionFactory from hibernate.cfg.xml
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

//	private Connection connection = null;
//	private Properties prop;
//	private DMLogger logger;
//
//	public Database(Properties prop, DMLogger logger) {
//		this.prop = prop;
//		this.logger = logger;
//	}
//
//	/**
//	 * Initialize Database
//	 */
//	public Connection initializeDatabase() {
//		String url = prop.getProperty("db.url");
//		String username = prop.getProperty("db.username");
//		String password = prop.getProperty("db.password");
//
//		try {
//			connection = DriverManager.getConnection(url, username, password);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return connection;
//	}
//
//	public List query(String insurerCompany, int limit, int offsetCount) {
//		List queryList = new ArrayList();
//
//		try {
//			String query = "SELECT * FROM T_FILE_DETAILS_" + insurerCompany + " LIMIT " + limit + " OFFSET " + (offsetCount * limit);
//			Statement st = connection.createStatement();
//			ResultSet rs = st.executeQuery(query);
//
//			while (rs.next()) {
//				FileDetails fileDetails = new FileDetails(rs);
//				fileDetails.print();
//				queryList.add(fileDetails);
//			}
//			st.close();
//
//		} catch (Exception e) {
//			logger.print(e.getMessage());
//		}
//
//		return queryList;
//	}
}
