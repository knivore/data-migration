package sg.gov.cpf.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.jgoodies.common.base.SystemUtils;
import com.mysql.cj.util.StringUtils;

public class MigrationUtil {

	private MigrationUtil() {
	}

	public static String getSystemPathSeperator() {

		String seperator = null;

		if (SystemUtils.IS_OS_WINDOWS) {
			seperator = "\\";
		}

		if (SystemUtils.IS_OS_LINUX) {
			seperator = "/";
		}

		if (SystemUtils.IS_OS_MAC) {
			seperator = "/";

		}

		return seperator;

	}

	public static void alert(String s) {
		JOptionPane.showMessageDialog(null, s);
	}

	public static void alert(String format, Object... arguments) {
		JOptionPane.showMessageDialog(null, java.text.MessageFormat.format(format, arguments));
	}

	public static boolean validateInput(DMLogger logger, JTextField... inputs) {
		for (JTextField input : inputs) {
			if (StringUtils.isNullOrEmpty(input.getText())) {
				String errorMsg = input.getToolTipText().replace(":", "") + "Required ";
				logger.print(errorMsg + input.getText());
				alert(errorMsg);
				input.requestFocus();
				return false;
			}
		}
		return true;
	}

	/** Check file locations to confirm existence and is not a directory */
	public static boolean validateFileLocation(String... inputLocations) {
		for (String location : inputLocations) {
			File file = new File(location);
			if (!file.exists() || !file.isFile()) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean validateFileLocation(DMLogger logger, String... inputLocations) {
		return validateFileLocation(logger, true, inputLocations);
	}

	public static boolean validateFileLocation(DMLogger logger, boolean toAlert, String... inputLocations) {
		for (String location : inputLocations) {
			File file = new File(location);
			/* Check input file existence */
			if (!file.exists()) {
				String errorMsg = "file or directory not found. ";
				logger.print("ERROR : " + errorMsg + location);
				if (toAlert) {
					alert(errorMsg);
				}
				return false;
			}
		}
		return true;
	}

	public static boolean validateFileExtension(DMLogger logger, File file, String... extensions) {
		return validateFileExtension(logger, file, true, extensions);
	}

	public static boolean validateFileExtension(DMLogger logger, File file, boolean toAlert, String... extensions) {
		for (String extension : extensions) {
			/* Check file extension */
			int extIndex = file.getAbsolutePath().lastIndexOf(".");
			if (extIndex == -1 || !file.getAbsolutePath().substring(extIndex).equalsIgnoreCase("." + extension)) {
				String errorMsg = "Input file is not a " + extension + " file ";
				logger.print("ERROR : " + errorMsg);
				if (toAlert) {
					alert(errorMsg);
				}
				return false;
			}
		}
		return true;
	}
	
	/** Check single file extension */
	public static boolean validateFileExtension(File file, String extension) {
		
		int extIndex = file.getAbsolutePath().lastIndexOf(".");
		return extIndex != -1 && file.getAbsolutePath().substring(extIndex).equalsIgnoreCase("." + extension);
			
	}

	public static void terminateLoggerThread(DMLogger logger) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// set to true to auto delete temp logs
				logger.terminate(false);
			}
		});
	}

	/**
	 * Open and read a file, and return the lines in the file as a list of Strings.
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static List<String> readFile(String filePath) throws IOException {
		List<String> results = new ArrayList<>();
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				results.add(line);
			}
			return results;
		}
	}

	/**
	 * Get array of files in a directory of a given extension (case-insensitive),
	 * example extension: "zip"
	 */
	public static File[] getFilesByType(String directoryPath, String extension) {
		if (directoryPath == null)
			return new File[0];
		File f = new File(directoryPath);
		if (!f.exists())
			return new File[0];
		FilenameFilter filter = (File file, String name) -> name.endsWith("." + extension.toLowerCase())
				|| name.endsWith("." + extension.toUpperCase());
		return f.listFiles(filter);
	}

	
	public static String convertMillisecondsToDuration(long ms) {
		long durationInSeconds = ms / 1000;
		long s = durationInSeconds % 60;
		long m = (durationInSeconds / 60) % 60;
	    long h = (durationInSeconds / (60 * 60)) % 24;
	    long d = (durationInSeconds / (60 * 60 * 24));
	    if(d > 0) {
	    	return String.format("%ddays %dhrs %dmins %ds", d, h, m, s);
	    } else if(h > 0) {
	    	return String.format("%dhrs %dmins %dseconds", h, m, s);
	    } else if(m > 0) {
	    	return String.format("%dmins %ds", m, s);
	    } else if(s > 0) {
	    	return String.format("%ds %dms", s, ms % 1000);
	    } else {
	    	return String.format("%dms", ms % 1000);
	    }
	}
	
}
