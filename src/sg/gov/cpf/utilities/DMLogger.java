package sg.gov.cpf.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class DMLogger {
		
	private static final String filePathSeperator = MigrationUtil.getSystemPathSeperator();
	private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd - HH:mm:ssa");
	private static final DateTimeFormatter fileNameFormatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	private static final String PATH = System.getProperty("user.dir") + filePathSeperator + "logs" + filePathSeperator;
	
	private static Map<Class,BufferedWriter> writers = new HashMap<>();
	private Class inputSourceClass;
	private String tempFilePath;
	
	public DMLogger(Class inputSourceClass) {
		this.inputSourceClass = inputSourceClass;
		new File(PATH).mkdir();
		new File(PATH + this.inputSourceClass.getSimpleName()).mkdir();
		new File(PATH + "temp").mkdir();
		File tempDir = new File(PATH + "temp");
		try {
			File tempFile = File.createTempFile(this.inputSourceClass.getSimpleName() + "_", ".txt", tempDir);
			tempFilePath = tempFile.getAbsolutePath();
			writers.put(inputSourceClass, new BufferedWriter(new FileWriter(tempFile)));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		print(this.inputSourceClass.getSimpleName() + " logger started");
	}

	public String export() throws IOException {
		copyFile(PATH + this.inputSourceClass.getSimpleName() + "/" + this.inputSourceClass.getSimpleName() + "-" + fileNameFormatter.format(LocalDateTime.now()) + ".txt");
		return PATH + this.inputSourceClass.getSimpleName();
	}
	
	public void print(String message) {
		print(message, new Object[0]);
	}
	
	public void print(String message, Object... arguments) {
		String msg = java.text.MessageFormat.format(message, arguments);
		String output = dtf.format(LocalDateTime.now()) + " [" + this.inputSourceClass.getSimpleName() + "] " + " --> " + msg;
		System.out.println(output);
		BufferedWriter writer = writers.get(this.inputSourceClass);
		try {
			writer.write(output + "\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes temporary log files
	 */
	public void terminate(boolean deleteTempFile) {
		print(this.inputSourceClass.getSimpleName() + " logger terminating");
		try {
			writers.get(this.inputSourceClass).close();
			System.out.println("Terminated " + this.inputSourceClass + "logger" );
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(deleteTempFile) {
			File f = new File(this.tempFilePath);
			if(f.delete())
				System.out.println("deleted " + this.tempFilePath);
		}	
		
	}

	private void copyFile(String destPath) throws IOException {
		File destFile = new File(destPath);
		InputStream in = new BufferedInputStream(new FileInputStream(this.tempFilePath));
		OutputStream out = new BufferedOutputStream(new FileOutputStream(destFile));
		byte[] buffer = new byte[1024];
		int lengthRead;
		while ((lengthRead = in.read(buffer)) > 0) {
			out.write(buffer, 0, lengthRead);
			out.flush();
		}
		out.close();
		in.close();
	}
}
