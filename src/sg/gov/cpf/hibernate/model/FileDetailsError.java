package sg.gov.cpf.hibernate.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class FileDetailsError {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", updatable = false, nullable = false)
	private int id;

	@Column
	private String systemId;

	@Column
	private String filename;

	@Column
	private String referenceIdPolicyholder;

	@Column
	private String referenceIdPolicy;

	@Column
	private String errorMessage;

	@Column
	private Date createdDate;

	@Column
	private String createdBy;

	@Column
	private Date updatedDate;

	@Column
	private String updatedBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getReferenceIdPolicyholder() {
		return referenceIdPolicyholder;
	}

	public void setReferenceIdPolicyholder(String referenceIdPolicyholder) {
		this.referenceIdPolicyholder = referenceIdPolicyholder;
	}

	public String getReferenceIdPolicy() {
		return referenceIdPolicy;
	}

	public void setReferenceIdPolicy(String referenceIdPolicy) {
		this.referenceIdPolicy = referenceIdPolicy;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "FileDetailsError [id=" + id + ", systemId=" + systemId + ", filename=" + filename
				+ ", referenceIdPolicyholder=" + referenceIdPolicyholder + ", referenceIdPolicy=" + referenceIdPolicy
				+ ", errorMessage=" + errorMessage + ", createdDate=" + createdDate + ", createdBy=" + createdBy
				+ ", updatedDate=" + updatedDate + ", updatedBy=" + updatedBy + "]";
	}

}
