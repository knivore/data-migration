package sg.gov.cpf.hibernate.model;

import javax.persistence.Entity;

@Entity(name = "T_ERR_FILE_DETAILS_AVI")
public class AvivaFileDetailsError extends FileDetailsError {

    public AvivaFileDetailsError() {
		super();
	}
}
