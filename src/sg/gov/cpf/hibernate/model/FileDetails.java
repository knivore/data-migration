package sg.gov.cpf.hibernate.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class FileDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DBID", updatable = false, nullable = false)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="SNAPSHOT_DATE")
    private Date snapshotDate;
    
    public Date getSnapshotDate() {
		return snapshotDate;
	}

	public void setSnapshotDate(Timestamp snapshotDate) {
		this.snapshotDate = snapshotDate;
	}

	@Column(name="SYSTEM_ID")
    private String systemId;

    @Column(name="UPLOADED_BY")
    private String uploadedBy;

    @Column(name="FILENAME")
    private String filename;

    @Column(name="FILETYPE")
    private String filetype;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="UPLOADED_ON")
    private Date uploadedOn;

    @Column(name="ATTACHMENT_TYPE")
    private String attachmentType;

    @Column(name="REFERENCE_ID_POLICYHOLDER")
    private String referenceIdPolicyholder;

    @Column(name="REFERENCE_ID_POLICY")
    private String referenceIdPolicy;

    @Column(name="ARCHIVED_RECORD_TAG")
    private String archivedRecordTag;

    @Column(name="HASH")
    private String hash;

    @Column(name="RCUBE_SENT_FLAG")
    private String rcubeSentFlag;

    @Column(name="RCUBE_FILE_SENT_DATE_TIME")
    private Timestamp rcubeFileSentDateTime;

    @Column(name="RCUBE_ERR_MSG")
    private String rcubeErrorMessage;
    
    

	@Column(name="RCUBE_DOCUMENT_CREATED_CODE")
    private int rcubeDocumentCreatedCode;

    @Column(name="CREATED_DATE")
    private Timestamp createdDate;

    @Column(name="CREATED_BY")
    private String createdBy;

    @Column(name="UPDATED_DATE")
    private Timestamp updatedDate;

    @Column(name="UPDATED_BY")
    private String updatedBy;

//    public FileDetails(int id, String systemId, String uploadedBy, String filename, String filetype, String attachmentType, String referenceIdPolicyholder, String referenceIdPolicy, String archivedRecordTag, String hash, String rcubeSentFlag, Timestamp rcubeFileSentDateTime, int rcubeDocumentCreatedCode, Timestamp uploadedOn, Timestamp createdDate, String createdBy, Timestamp updatedDate, String updatedBy) {
//        this.id = id;
//        this.systemId = systemId;
//        this.uploadedBy = uploadedBy;
//        this.filename = filename;
//        this.filetype = filetype;
//        this.attachmentType = attachmentType;
//        this.referenceIdPolicyholder = referenceIdPolicyholder;
//        this.referenceIdPolicy = referenceIdPolicy;
////        this.archivedRecordTag = archivedRecordTag;
//        this.hash = hash;
//        this.rcubeSentFlag = rcubeSentFlag;
//        this.rcubeFileSentDateTime = rcubeFileSentDateTime;
//        this.rcubeDocumentCreatedCode = rcubeDocumentCreatedCode;
//        this.uploadedOn = uploadedOn;
//        this.createdDate = createdDate;
//        this.createdBy = createdBy;
//        this.updatedDate = updatedDate;
//        this.updatedBy = updatedBy;
//    }

    public FileDetails() {
		super();
	}

//	public FileDetails(ResultSet rs) throws SQLException {
//        this.id = rs.getInt("ID");
//        this.systemId = rs.getString("SYSTEM_ID");
//        this.uploadedBy = rs.getString("UPLOADED_BY");
//        this.filename = rs.getString("FILENAME");
//        this.filetype = rs.getString("FILETYPE");
//        this.attachmentType = rs.getString("ATTACHMENT_TYPE");
//        this.referenceIdPolicyholder = rs.getString("REFERENCE_ID_POLICYHOLDER");
//        this.referenceIdPolicy = rs.getString("REFERENCE_ID_POLICY");
////        this.archivedRecordTag = rs.getString("ARCHIVED_RECORD_TAG");
//        this.hash = rs.getString("HASH");
//        this.rcubeSentFlag = rs.getString("RCUBE_SENT_FLAG");
////        this.rcubeFileSentDateTime = rs.getDate("RCUBE_FILE_SENT_DATE_TIME");
//        this.rcubeFileSentDateTime = rs.getTimestamp("RCUBE_FILE_SENT_DATE_TIME");
//        this.rcubeDocumentCreatedCode = rs.getInt("RCUBE_DOCUMENT_CREATED_CODE");
//        this.uploadedOn = rs.getTimestamp("UPLOADED_ON");
//        this.createdDate = rs.getTimestamp("CREATED_DATE");
//        this.createdBy = rs.getString("CREATED_BY");
//        this.updatedDate = rs.getTimestamp("UPDATED_DATE");
//        this.updatedBy = rs.getString("UPDATED_BY");
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getReferenceIdPolicyholder() {
        return referenceIdPolicyholder;
    }

    public void setReferenceIdPolicyholder(String referenceIdPolicyholder) {
        this.referenceIdPolicyholder = referenceIdPolicyholder;
    }

    public String getReferenceIdPolicy() {
        return referenceIdPolicy;
    }

    public void setReferenceIdPolicy(String referenceIdPolicy) {
        this.referenceIdPolicy = referenceIdPolicy;
    }

    public String getArchivedRecordTag() {
        return archivedRecordTag;
    }

    public void setArchivedRecordTag(String archivedRecordTag) {
        this.archivedRecordTag = archivedRecordTag;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getRcubeSentFlag() {
        return rcubeSentFlag;
    }

    public void setRcubeSentFlag(String rcubeSentFlag) {
        this.rcubeSentFlag = rcubeSentFlag;
    }

    public Timestamp getRcubeFileSentDateTime() {
        return rcubeFileSentDateTime;
    }

    public void setRcubeFileSentDateTime(Timestamp rcubeFileSentDateTime) {
        this.rcubeFileSentDateTime = rcubeFileSentDateTime;
    }

    public int getRcubeDocumentCreatedCode() {
        return rcubeDocumentCreatedCode;
    }

    public void setRcubeDocumentCreatedCode(int rcubeDocumentCreatedCode) {
        this.rcubeDocumentCreatedCode = rcubeDocumentCreatedCode;
    }

    public Date getUploadedOn() {
        return uploadedOn;
    }

    public void setUploadedOn(Date uploadedOn) {
        this.uploadedOn = uploadedOn;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRcubeErrorMessage() {
		return rcubeErrorMessage;
	}

	public void setRcubeErrorMessage(String rcubeErrorMessage) {
		this.rcubeErrorMessage = rcubeErrorMessage;
	}

	@Override
	public String toString() {
		return "FileDetails [id=" + id + ", systemId=" + systemId + ", uploadedBy=" + uploadedBy + ", filename="
				+ filename + ", filetype=" + filetype + ", uploadedOn=" + uploadedOn + ", attachmentType="
				+ attachmentType + ", referenceIdPolicyholder=" + referenceIdPolicyholder + ", referenceIdPolicy="
				+ referenceIdPolicy + ", archivedRecordTag=" + archivedRecordTag + ", hash=" + hash + ", rcubeSentFlag="
				+ rcubeSentFlag + ", rcubeFileSentDateTime=" + rcubeFileSentDateTime + ", rcubeErrorMessage="
				+ rcubeErrorMessage + ", rcubeDocumentCreatedCode=" + rcubeDocumentCreatedCode + ", createdDate="
				+ createdDate + ", createdBy=" + createdBy + ", updatedDate=" + updatedDate + ", updatedBy=" + updatedBy
				+ "]";
	}

}
