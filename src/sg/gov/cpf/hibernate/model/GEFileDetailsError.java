package sg.gov.cpf.hibernate.model;

import javax.persistence.Entity;

@Entity(name = "T_ERR_FILE_DETAILS_GE")
public class GEFileDetailsError extends FileDetailsError {

    public GEFileDetailsError() {
		super();
	}
}
