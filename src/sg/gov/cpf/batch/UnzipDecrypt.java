package sg.gov.cpf.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sg.gov.cpf.utilities.MigrationUtil;
import sg.gov.cpf.utilities.SliftUtil;

public class UnzipDecrypt {
	private static Logger logger = LogManager.getLogger(UnzipDecrypt.class.getName());

	private static String sourceFolder;
	private static String destinationFolder;
	
	private static final String ZIP_FILE_TYPE = "zip";
	private static final String ENCRYPTED_FILE_TYPE = "p7";
	private static final String PRIVATE_KEY_FILE_TYPE = "pfx";
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		logger.info("UnzipDecrypt batch job arguments: {0}", Arrays.toString(args));
		/*
		 * arg 0 - source folder for zip files to be unzipped & decrypted
		 * arg 1 - destination folder for populating output files
		 * arg 2 - private key file for decryption
		 */
		
		if(!inputIsValid(args)) {
			System.exit(1);
		}
		logger.info("Valid input arguments found.");
		setSourceFolder(args[0]);
		setDestinationFolder(args[1]);

		// Get total number of zip files
		File[] zipFiles = MigrationUtil.getFilesByType(sourceFolder, ZIP_FILE_TYPE);
		logger.info(() -> "Total " + zipFiles.length + " zip files found in source directory : " + sourceFolder);
		
		if(zipFiles.length==0) {
			logger.warn(UDText.ERROR_NO_ZIP_FILES_FOUND.text, sourceFolder);
		}
		
		int validatedMD5Count = 0;
		int unzippedFileCount = 0;
		
		List<File> unzippedFiles = new ArrayList<>();
		for(int i = 0; i < zipFiles.length; i++) {
			// Perform MD5 checksum
			String zipFilePath = zipFiles[i].getAbsolutePath();
			if(!validateFileMD5(zipFilePath)) {
				logger.warn(UDText.ERROR_MD5_VALIDATION.text, zipFilePath);
				// if MD5 fails, will skip unzip for current file
				continue;
			}
			validatedMD5Count++;
			// Unzip
			unzippedFiles.addAll(unzipFile(zipFilePath, destinationFolder));
			if(unzippedFiles.isEmpty()) {
				logger.info(UDText.WARNING_NO_FILES_UNZIPPED.text, zipFilePath);
			}
			unzippedFileCount++;		
		}
		logger.info(UDText.INFO_MD5_VALIDATION.text, validatedMD5Count);
		logger.info(UDText.INFO_FILES_UNZIPPED.text, unzippedFileCount);
		
		List<File> encryptedFiles = new ArrayList<>();
		// Ensure that files unzipped are of valid encrypted file type
		unzippedFiles.stream().forEach(f -> {
			if(!MigrationUtil.validateFileExtension(f, ENCRYPTED_FILE_TYPE)) {
				logger.warn(UDText.ERROR_NOT_ENCRYPTED_FILE.text, f.getAbsolutePath());
			} else {
				encryptedFiles.add(f);
			}
		});
		
		if(encryptedFiles.isEmpty()) {
			logger.warn(UDText.WARNING_NO_ENCRYPTED_FILES.text);
//			return;
		}
		// Perform decryption
		logger.info(UDText.INFO_DECRYPTION_START.text, encryptedFiles.size());
		List<File> decryptedFiles = SliftUtil.runDecryption(encryptedFiles, destinationFolder);
		logger.info(UDText.INFO_DECRYPTION_COMPLETED.text, decryptedFiles.size());
		
		// Remove original encrypted files
		decryptedFiles.stream().forEach(File::delete);
		
		long timeTakenMillis = System.currentTimeMillis() - start;
		logger.info(UDText.INFO_UNZIP_DECRYPT_COMPLETED.text, MigrationUtil.convertMillisecondsToDuration(timeTakenMillis));
	}
	
	private static boolean inputIsValid(String[] args) {
		boolean result = true;

		if (args.length >= 3) {
			File sourceDir = new File(args[0]);
			if (!sourceDir.exists() || !sourceDir.isDirectory()) {
				logger.warn("Invalid source folder : {0}", args[0]);
				result = false;
			}
			File destDir = new File(args[1]);
			if (!destDir.exists() || !destDir.isDirectory()) {
				logger.warn("Invalid destination folder : {0}", args[1]);
				result = false;
			}
			File decryptKeyFile = new File(args[2]);
			
			if (!decryptKeyFile.exists() || !decryptKeyFile.isFile()) {
				logger.warn("Invalid private key file : {0}", args[2]);
				result = false;
				return result;
			}
			
			if(!MigrationUtil.validateFileExtension(decryptKeyFile, PRIVATE_KEY_FILE_TYPE)) {
				logger.warn("Invalid private key file type : {0}", decryptKeyFile.getName());
				result = false;
			}
			return result;
			
		}
		
		logger.warn("Invalid number of inputs passed into batch program,"
				+ " check that arg 1 - Source folder containing zip files,"
				+ " arg 2 - Destination folder for output unzipped decrpted files,"
				+ " arg 3 - Private Key file path for decryption");
		
		return false;
	}

	/**
	 * Calculates MD5 of a file and checks against MD5 value provided
	 * in a .md5 file of the same name within the same folder.
	 * @param filePath - path of target file to be validated
	 * @return true - MD5 match <br> false - MD5 do not match
	 */
	private static boolean validateFileMD5(String filePath) {
		String expectedMD5 = "expected";
		// Check corresponding md5 file
		String md5Location = filePath.substring(0, filePath.lastIndexOf(".")) + ".md5";
		try {
			BufferedReader br = new BufferedReader(new FileReader(md5Location));
			expectedMD5 = br.readLine();
			logger.trace(UDText.INFO_MD5_EXPECTED.text, expectedMD5);
			br.close();
		} catch (FileNotFoundException e1) {
			logger.warn(UDText.ERROR_MD5_FILE_NOT_FOUND.text, md5Location);
			return false;
		} catch (IOException e2) {
			logger.warn(UDText.ERROR_MD5_FILE_UNABLE_TO_READ.text, md5Location);
			logger.warn(e2.getMessage());
			return false;
		}
		String actualMD5 = "actual";
		try (InputStream is = Files.newInputStream(Paths.get(filePath))) {
			actualMD5 = DigestUtils.md2Hex(is);
			logger.trace(UDText.INFO_MD5_ACTUAL.text, actualMD5);
		} catch (IOException e) {
			e.printStackTrace();
			logger.warn(e.getMessage());
		}
		if (actualMD5.equalsIgnoreCase(expectedMD5)) {
			return true;
		} else {
			logger.trace(UDText.ERROR_MD5_NOT_MATCH.text);
			return false;
		}
	}
	
	/**
	 * Unzip file to target directory
	 * @param filePath - path of zip file
	 * @param destination - output directory
	 * @return List of File objects (unzipped) in destination folder
	 */
	private static List<File> unzipFile(String filePath, String destination) {
		List<File> unzippedFiles = new ArrayList<>();
		byte[] buffer = new byte[1024];
		try {
			File file = new File(filePath);
			if (!file.exists()) {
				return Collections.emptyList();
			}
			ZipInputStream zis = new ZipInputStream(new FileInputStream(filePath));
			ZipEntry zipEntry = zis.getNextEntry();
			while (zipEntry != null) {
				File newFile = newFile(new File(destination), zipEntry);
				if(newFile != null) {
					FileOutputStream fos = new FileOutputStream(newFile);
					int len;
					while ((len = zis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
					fos.close();
					logger.log(Level.TRACE, UDText.INFO_FILE_UNZIPPED.text, newFile.getName());
					unzippedFiles.add(newFile);
				}
				zipEntry = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
			logger.warn(e.getMessage());
		}
		return unzippedFiles;
	}
	
	private static File newFile(File destination, ZipEntry zipEntry) throws IOException {
		File destFile = new File(destination, zipEntry.getName());
		/* if file already exists, return */
		if (destFile.exists()) {
			logger.log(Level.TRACE, UDText.ERROR_UNZIP_FILE_EXISTS.text, destFile.getName());
			return null;
		}
		String destDirPath = destination.getCanonicalPath();
		String destFilePath = destFile.getCanonicalPath();
		if (!destFilePath.startsWith(destDirPath + File.separator)) {
			throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
		}
		return destFile;
	}

	public static String getSourceFolder() {
		return sourceFolder;
	}

	public static void setSourceFolder(String sourceFolder) {
		UnzipDecrypt.sourceFolder = sourceFolder;
	}

	public static String getDestinationFolder() {
		return destinationFolder;
	}

	public static void setDestinationFolder(String destinationFolder) {
		UnzipDecrypt.destinationFolder = destinationFolder;
	}
	
	public enum UDText {
		DECRYPTION_COMMAND("sliftc /d \"%s\" \"%s\" /pfx \"%s\" %s"),
		ERROR_MD5_FILE_NOT_FOUND("Required {0} not found"),
		ERROR_MD5_FILE_UNABLE_TO_READ("Unable to read MD5 file : {0}"),
		ERROR_MD5_NOT_MATCH("MD5 do not match"),
		ERROR_MD5_VALIDATION("File MD5 validation. Source file: {0}"),
		ERROR_NO_ZIP_FILES_FOUND("No zip files found in source directory: {0}"),
		ERROR_NOT_ZIP_FILE("Unable to unzip, selected file is not a zip file. {0}"),
		ERROR_NOT_ENCRYPTED_FILE("<{0}> is not an encrypted file."),
		ERROR_UNZIP_FILE_EXISTS("Unable to unzip <{0}>, file with same name already exists in target directory."),
		INFO_DECRYPTION_START("Decryption start: {0} files to decrypt."),
		INFO_DECRYPTION_COMPLETED("Decryption completed: {0} files decrypted successfully."),
		INFO_LOCATIONS_VALIDATED("File locations validated. Source file: {0}, Target directory: {1}"),
		INFO_MD5_ACTUAL("Actual MD5 = {0}"),
		INFO_MD5_EXPECTED("Expected MD5 provided = {0}"),
		INFO_MD5_VALIDATED("File MD5 validated. {0}"),
		INFO_MD5_VALIDATION("Validated {0} zip files MD5 successfully"),
		INFO_FILE_UNZIPPED("File : <{0}> successfully created in target directory."),
		INFO_FILES_UNZIPPED("Unzipped {0} files successfully"),
		INFO_FILES_UNZIPPED_TOTAL("Unzip Summary - Total {0} files unzipped from {1} zip files in {2}"),
		INFO_UNZIPPING_FILE("Unzipping file: <{0}>"),
		INFO_UNZIP_DECRYPT_COMPLETED("Completed unzip and decryption. ({0})"),
		WARNING_NO_FILES_UNZIPPED("No files unzipped from source file: {0}"),
		WARNING_NO_ENCRYPTED_FILES("No encypted files found in unzipped files.");
		
		private final String text;
		private UDText(String text) {
			this.text = text;
		}
		@Override
		public String toString() {
			return text;
		}
	}
	
	private static final String format(String format, Object... arguments) {
		return java.text.MessageFormat.format(format, arguments);
	}
}
