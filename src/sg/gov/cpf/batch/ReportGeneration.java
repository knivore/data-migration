package sg.gov.cpf.batch;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sg.gov.cpf.utilities.Constants;
import sg.gov.cpf.utilities.JdbcDatabaseUtil;

public class ReportGeneration {

	private static Logger logger = LogManager.getLogger(UploadMetaData.class.getName());

    private static String insurerOptions = Constants.AVI_INSURER_NAME + "/" + Constants.NTUC_INSURER_NAME + "/" + Constants.GE_INSURER_NAME;
    private static String reportTypeOptions = Constants.REPORT_TYPE_DETAIL + "/" + Constants.REPORT_TYPE_EXCEPTION 
    																	   + "/" + Constants.REPORT_TYPE_SUMMARY 
    																	   + "/" + Constants.REPORT_TYPE_POSTLOAD
    																	   + "/" + Constants.REPORT_TYPE_ALL_DETAIL
    																	   + "/" + Constants.REPORT_TYPE_ALL_EXCEPTION;
    																	   

	private static String insurer;
	private static String reportType;
	private static String locationToSaveReport;
	private static String startDate;
	private static String endDate;

	public static void main(String[] args) {
		logger.info("ReportGeneration batch job arguments: " + Arrays.toString(args));

		//TODO SUMMARY and POSTLOAD
		//TODO Format DETAIL/EXCEPTION report to insurer format

		// arg 0 - insurer type(AVI/GE/NTUC)
		// arg 1 - report start date
		// arg 2 - report end date
		// arg 3 - location to save report
		// arg 4 - insurer reportType(DETAIL/EXCEPTION/SUMMARY/POSTLOAD,ALL_DETAIL,ALL_EXCEPTION)
		if (inputIsValid(args)) {

			setInsurer(args[0]);
			setStartDate(args[1]);
			setEndDate(args[2]);
			setLocationToSaveReport(args[3]);
			setReportType(args[4]);
			logger.info("Valid input arguments found.");

			
			try {
				switch (getReportType()) {
				case Constants.REPORT_TYPE_ALL_DETAIL:
					exportMasterTable(getInsurer(), getStartDate(), getEndDate(), getLocationToSaveReport());
					break;
				case Constants.REPORT_TYPE_ALL_EXCEPTION:
					exportErrorTable(getInsurer(), getStartDate(), getEndDate(), getLocationToSaveReport());
					break;

				default:
					break;
				}

			} catch (SQLException | IOException e) {
				logger.warn("Error generating " + reportType + " report " + e.getMessage());
			}
			
			
			
		} else {
			logger.warn("Invalid input arguments, terminating job.");
			System.exit(1);
		}

	}

	private static boolean inputIsValid(String[] args) {
		boolean result = true;

		if (args.length >= 5) {

			if (!isInsurerTypeValid(args[0])) {
				result = false;
			}
			
			if (!isStartAndEndDateValid(args[1], args[2], DateTimeFormatter.BASIC_ISO_DATE))			{
				result = false;
	
			}

			if (!isLocationToSaveReportValid(args[3])) {
				result = false;
			}

			if (!isReportTypeValid(args[4])) {
				result = false;
			}

		} else {
			result = false;
			logger.warn("Invalid number of inputs passed into batch program,"
					+ " check that arg 1 - Insurer Type(" + insurerOptions + "),"
					+ " arg 2 - Report Type(" + reportTypeOptions + "),"
					+ " arg 3 - Location To Save Report");
		}
		return result;
	}

	public static boolean isValidDate(String dateStr, DateTimeFormatter dateTimeFormatter) {
        try {
            LocalDate.parse(dateStr, dateTimeFormatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

	private static boolean isStartAndEndDateValid(String startDate, String endDate, DateTimeFormatter dateTimeFormatter) {
		boolean result = true;

		if(!isValidDate(startDate, dateTimeFormatter)) {
			logger.warn("Invalid start date(yyyymmdd) " + startDate);
			result = false;
		}
		if(!isValidDate(endDate, dateTimeFormatter)) {
			logger.warn("Invalid end date(yyyymmdd)  " + endDate);
			result = false;
		}
		return result;
	}

	public static void exportMasterTable(String insurer, String startDate, String endDate, String destination)
			throws SQLException, IOException {
		JdbcDatabaseUtil.exportMasterTable(retrieveInsurerFileDetailTable(insurer), startDate, endDate, destination);
	}

	public static void exportErrorTable(String insurer, String startDate, String endDate, String destination)
			throws SQLException, IOException {
		JdbcDatabaseUtil.exportExceptionTable(retrieveInsurerFileDetailErrorTable(insurer), startDate, endDate, destination);

	}

	private static String retrieveInsurerFileDetailErrorTable(String insurer) {
		String errorTableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case "AVI":
				errorTableName = Constants.AVI_ERROR_FILE_DETAILS_TABLE;
				break;
			case "NTUC":
				errorTableName = Constants.NTUC_ERROR_FILE_DETAILS_TABLE;
				break;
			case "GE":
				errorTableName = Constants.GE_ERROR_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warn("In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return errorTableName;
	}
	
	private static String retrieveInsurerFileDetailTable(String insurer) {

		String tableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case "AVI":
				tableName = Constants.AVI_FILE_DETAILS_TABLE;
				break;
			case "NTUC":
				tableName = Constants.NTUC_FILE_DETAILS_TABLE;
				break;
			case "GE":
				tableName = Constants.GE_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warn("In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return tableName;
	}
	private static boolean isReportTypeValid(String reportType) {
		boolean result = true;
		List<String> reportTypes = Arrays.asList(new String[] { Constants.REPORT_TYPE_DETAIL, Constants.REPORT_TYPE_EXCEPTION, Constants.REPORT_TYPE_SUMMARY, Constants.REPORT_TYPE_POSTLOAD, Constants.REPORT_TYPE_ALL_DETAIL, Constants.REPORT_TYPE_ALL_EXCEPTION});
		if (!reportTypes.contains(reportType)) {
			result = false;
			logger.warn("Invalid Report Type passed into batch program : " + reportType);
		}
		return result;
	}

	private static boolean isLocationToSaveReportValid(String locationToSaveReport) {
		boolean result = true;

		if (!doesFileExists(locationToSaveReport)) {
			logger.warn("Location To Save Report does not exists at : " + locationToSaveReport);
			result = false;
		}

		return result;
	}

	public static boolean doesFileExists(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}

	private static boolean isInsurerTypeValid(String insurerType) {
		boolean result = true;
		List<String> insurerList = Arrays.asList(new String[]{Constants.AVI_INSURER_NAME, Constants.NTUC_INSURER_NAME, Constants.GE_INSURER_NAME});
		if (!insurerList.contains(insurerType)) {
			result = false;
			logger.warn("Invalid insurer type passed into batch program : " + insurerType);
		}
		return result;
	}

	public static String getInsurer() {
		return insurer;
	}

	public static void setInsurer(String insurer) {
		ReportGeneration.insurer = insurer;
	}

	public static String getReportType() {
		return reportType;
	}

	public static void setReportType(String reportType) {
		ReportGeneration.reportType = reportType;
	}

	public static String getLocationToSaveReport() {
		return locationToSaveReport;
	}

	public static void setLocationToSaveReport(String locationToSaveReport) {
		ReportGeneration.locationToSaveReport = locationToSaveReport;
	}

	public static String getStartDate() {
		return startDate;
	}

	public static void setStartDate(String startDate) {
		ReportGeneration.startDate = startDate;
	}

	public static String getEndDate() {
		return endDate;
	}

	public static void setEndDate(String endDate) {
		ReportGeneration.endDate = endDate;
	}

}
