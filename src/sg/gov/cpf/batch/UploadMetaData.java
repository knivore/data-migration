package sg.gov.cpf.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sg.gov.cpf.meta.model.RecordDetail;
import sg.gov.cpf.meta.model.RecordHeader;
import sg.gov.cpf.meta.model.RecordTrailer;
import sg.gov.cpf.utilities.Constants;
import sg.gov.cpf.utilities.JdbcDatabaseUtil;
import sg.gov.cpf.utilities.MigrationUtil;
import sg.gov.cpf.utilities.RecordMapperUtil;

public class UploadMetaData {
	
	private static Logger logger = LogManager.getLogger(UploadMetaData.class.getName());
	public static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	private static String filePathSeperator = MigrationUtil.getSystemPathSeperator();
	private static String insurer;
	private static String metadataLocation;
	private static String insurerImageDocumentRoot;
	private static String snapShotDate;	

    private static String insurerOptions = Constants.AVI_INSURER_NAME + "/" + Constants.NTUC_INSURER_NAME + "/" + Constants.GE_INSURER_NAME;

	private static List<String> records;

	public static void main(String[] args) {
		

		logger.info("UploadMetaData batch job arguments: " + Arrays.toString(args));
		//arg 0 - insurer type
		//arg 1 - insurer meta data location
		//arg 2 - insurer image location 
		if(inputIsValid(args)){
			
			setInsurer(args[0]);
			setMetadataLocation(args[1]);
			setInsurerImageDocumentRoot(args[2]);
			
			logger.info("Valid input arguments found.");
			boolean processMetaData = true;

			try {

				setRecords(MigrationUtil.readFile(getMetadataLocation()));
				
				String validateMetaDataHeaderTrailerFormatResult = validateMetaDataHeaderTrailerFormat(getRecords(), getInsurer());
				if(!validateMetaDataHeaderTrailerFormatResult.isEmpty()) {
					logger.warn(validateMetaDataHeaderTrailerFormatResult);
					processMetaData = false;
				}
				
				String validateTrailerDetailCountCorrectResult = validateTrailerDetailCountCorrect(getRecords());
				if (!validateTrailerDetailCountCorrectResult.isEmpty()) {
					logger.warn(validateTrailerDetailCountCorrectResult);
					processMetaData = false;
				}
				
				//Begin upload process
				if(processMetaData) {
					assignInsurerToAndSnapshotDate(getRecords());
					
					//TODO may have to change this part, depending on how insurer images are stored in document root.
					processMetaData(getRecords(), getInsurerImageDocumentRoot());
					
				}
				
			} catch (IOException | ParseException | SQLException e) {
				logger.warn("Error processing meta data file " + getMetadataLocation() + " : " + e.getMessage());
				System.exit(1);
			}
			
		} else {
			logger.warn("Invalid inputs passed into batch program,"
					+ " check that arg 1 - Insurer Type(" + insurerOptions + "),"
					+ " arg 2 - Insurer's Metadata Location, "
					+ " arg 3 - Insurer's Image Location");
			System.exit(1);
		}
		
	}
	

	/**
	 * Check insurer type, meta data location and image location.
	 * @param args
	 * @return boolean 
	 */
	private static boolean inputIsValid(String[] args) {

		boolean result = true;

		if(args.length >= 3) { 

			if(!isInsurerTypeValid(args[0])) {
				result = false;
			}
			
			if(!isMetaDataLocationValid(args[1])) {
				result = false;
			}

			if(!isImageLocationValid(args[2])) {
				result = false;
			}
			
		} else {
			result =  false;

			logger.warn("Invalid number of inputs passed into batch program,"
					+ " check that arg 1 - Insurer Type(" + insurerOptions + "),"
					+ " arg 2 - Insurer's Metadata Location, "
					+ " arg 3 - Insurer's Image Location");
		}
		return result;
	}




	private static boolean isInsurerTypeValid(String insurerType) {
		boolean result = true;
		List<String> insurerList = Arrays.asList(new String[]{Constants.AVI_INSURER_NAME, Constants.NTUC_INSURER_NAME, Constants.GE_INSURER_NAME});
		if(!insurerList.contains(insurerType)) {
			result = false;
			logger.warn("Invalid insurer type passed into batch program : " + insurerType);
		}
		return result;
	}

	private static boolean isMetaDataLocationValid(String metaDataFileLocation) {
		boolean result = true;
		if(!metaDataFileLocation.endsWith(".csv")) {
			logger.warn("Metadata is not csv format: " + metaDataFileLocation);
			result = false;
		}

		if(!doesFileExists(metaDataFileLocation)) {
			logger.warn("Metadata file does not exists at : " + metaDataFileLocation);
			result = false;
		}

		return result;
	}

	private static boolean isImageLocationValid(String imageLocation) {
		boolean result = true;
		if(!doesFileExists(imageLocation)) {
			logger.warn("Image Location does not exists at : " + imageLocation);
			result = false;
		}
		return result;
	}


	private static void processMetaData(List<String> metaDataRecords, String insurerImageDocumentRoot) throws ParseException, SQLException, IOException {

		//Store indexes instead of creating a list of RecordDetail/Error objects, to save on JVM space.
		List<Integer> recordDetailBatchIndexes = new ArrayList<Integer>();
		List<Integer> recordDetailErrorBatchIndexes = new ArrayList<Integer>();

		for (int rowNumber = 1; rowNumber < metaDataRecords.size() - 1; rowNumber++) { // Detail Group, row 1 to ...

			String[] detokenizedMetaDataRecord = metaDataRecords.get(rowNumber).split(Constants.CSV_DELIMITER_FOR_SPLIT);

			RecordDetail recordDetail = RecordMapperUtil.mapToRecordDetail(detokenizedMetaDataRecord);

			Set<ConstraintViolation<RecordDetail>> recordDetailViolations = validator.validate(recordDetail);

			boolean computedMd5OfRecordDetail = isRecordDetailMd5Correct(recordDetail);

			boolean doesFileExists = doesFileExists(insurerImageDocumentRoot + filePathSeperator + recordDetail.getFileName());

			if (!recordDetailViolations.isEmpty() || !computedMd5OfRecordDetail || !doesFileExists) {
				recordDetailErrorBatchIndexes.add(rowNumber);
			} else {
				recordDetailBatchIndexes.add(rowNumber);
			}
		}

		if (!recordDetailBatchIndexes.isEmpty()) {
			logger.info("In processMetaData Start Time recordDetailBatchIndexes insert: " + LocalDateTime.now());
			JdbcDatabaseUtil.insertRecordDetailBatch(recordDetailBatchIndexes, metaDataRecords, getSnapShotDate(), retrieveInsurerFileDetailTable(getInsurer()));
			logger.info("In processMetaData End Time recordDetailBatchIndexes insert: " + LocalDateTime.now());
		}

		if (!recordDetailErrorBatchIndexes.isEmpty()) {
			logger.info("In processMetaData Start Time recordDetailErrorBatchIndexes insert: " + LocalDateTime.now());
			JdbcDatabaseUtil.insertRecordDetailErrorBatch(recordDetailErrorBatchIndexes, metaDataRecords, getSnapShotDate(), retrieveInsurerFileDetailErrorTable(getInsurer()));
			logger.info("In processMetaData End Time recordDetailErrorBatchIndexes insert: " + LocalDateTime.now());
		}


	}

	private static String retrieveInsurerFileDetailErrorTable(String insurer) {
		String errorTableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case Constants.AVI_INSURER_NAME:
				errorTableName = Constants.AVI_ERROR_FILE_DETAILS_TABLE;
				break;
			case Constants.NTUC_INSURER_NAME:
				errorTableName = Constants.NTUC_ERROR_FILE_DETAILS_TABLE;
				break;
			case Constants.GE_INSURER_NAME:
				errorTableName = Constants.GE_ERROR_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warn("In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return errorTableName;
	}
	
	private static String retrieveInsurerFileDetailTable(String insurer) {

		String tableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case Constants.AVI_INSURER_NAME:
				tableName = Constants.AVI_FILE_DETAILS_TABLE;
				break;
			case Constants.NTUC_INSURER_NAME:
				tableName = Constants.NTUC_FILE_DETAILS_TABLE;
				break;
			case Constants.GE_INSURER_NAME:
				tableName = Constants.GE_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warn("In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return tableName;
	}


	public static String validateMetaDataHeaderTrailerFormat(List<String> records, String insurer) throws FileNotFoundException, IOException {

		StringBuilder stringBuilder = new StringBuilder();

		if (records.size() >= 2) { // Contains header and trailer
			// First record
			RecordHeader recordHeader = RecordMapperUtil
					.mapToRecordHeader(records.get(0).split(Constants.CSV_DELIMITER_FOR_SPLIT));

			// Last record
			RecordTrailer recordTrailer = RecordMapperUtil
					.mapToRecordTrailer(records.get(records.size() - 1).split(Constants.CSV_DELIMITER_FOR_SPLIT));

			if (recordHeader != null && recordTrailer != null) {

				Set<ConstraintViolation<RecordHeader>> recordHeaderViolations = validator.validate(recordHeader);
				Set<ConstraintViolation<RecordTrailer>> recordTrailerViolations = validator.validate(recordTrailer);
				

				if (!recordHeaderViolations.isEmpty()) {
					stringBuilder.append("Error in Record Header.");
					stringBuilder.append("\n");
					stringBuilder.append(retrieveErrorFromConstraintViolation(recordHeaderViolations));
				}

				//Check batch job param insurer is same as meta data header insurer
				if(!recordHeader.getInsurerTag().equals(insurer)) {
					if(stringBuilder.toString().isEmpty()) {
						stringBuilder.append("Error in Record Header.");
						stringBuilder.append("\n");
					}
					stringBuilder.append("Insurer parameter : " + insurer + " does not match header insurer tag:  " + recordHeader.insurerTag);
					stringBuilder.append("\n");
				}

				if (!recordTrailerViolations.isEmpty()) {
					stringBuilder.append("Error in Record Trailer.");
					stringBuilder.append("\n");
					stringBuilder.append(retrieveErrorFromConstraintViolation(recordTrailerViolations));
				}

			} else {
				stringBuilder.append("Unable to generate Record Header or Trailer, please check it's format.");

			}
		}
		return stringBuilder.toString();
	}

	public static String validateTrailerDetailCountCorrect(List<String> records) throws FileNotFoundException, IOException {

		StringBuilder stringBuilder = new StringBuilder();

		// Validate detail count using trailer record
		RecordTrailer recordTrailer = RecordMapperUtil
				.mapToRecordTrailer(records.get(records.size() - 1).split(Constants.CSV_DELIMITER_FOR_SPLIT));

		if (recordTrailer != null) {

			final int HEADER_LINE_AND_TRAILER_LINE_COUNT = 2;

			int detailCountFromReadingFile = records.size() - HEADER_LINE_AND_TRAILER_LINE_COUNT;

			int detailCountInRecordTrailer = Integer.parseInt(recordTrailer.getRecordCount());

			if (detailCountFromReadingFile != detailCountInRecordTrailer) {

				stringBuilder.append("Error in Detail Count provided. Total detail records found in meta file: "
						+ detailCountFromReadingFile);
				stringBuilder.append("\n");
				stringBuilder
						.append("Indicated Detail Count from meta data file is: " + recordTrailer.getRecordCount());
				stringBuilder.append("\n");
			}
		} else {
			stringBuilder.append(
					"Unable to deserialize record trailer, please check that the record trailer format is correct");
		}
		return stringBuilder.toString();
	}

	private static void assignInsurerToAndSnapshotDate(List<String> metaDataRecords) throws FileNotFoundException, IOException {
		RecordHeader recordHeader = RecordMapperUtil
				.mapToRecordHeader(metaDataRecords.get(0).split(Constants.CSV_DELIMITER_FOR_SPLIT));

		setInsurer(recordHeader.getInsurerTag());
		setSnapShotDate(recordHeader.getSnapshotDate());

	}

	public static boolean isRecordDetailMd5Correct(RecordDetail recordDetail) {
		String algorithm = "MD5";

		String toCheck = recordDetail.getSystemId() + recordDetail.getFileName()
				+ recordDetail.getPolicyHolderReferenceId() + recordDetail.getPolicyReferenceId();

		String hash = "";

		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(toCheck.getBytes());

			byte[] digest = messageDigest.digest();

			hash = DatatypeConverter.printHexBinary(digest).toUpperCase();

		} catch (NoSuchAlgorithmException e) {
			System.out.println("Invalid Algorithm Selected : " + algorithm);
		}

		return recordDetail.getHash().contentEquals(hash);
	}

	/**
	 * Retrieves error from constraint violation. 
	 * @param <T>
	 * @param constraintViolation
	 * @return
	 */
	public static <T> String retrieveErrorFromConstraintViolation(Set<ConstraintViolation<T>> constraintViolation) {
		StringBuilder stringBuilder = new StringBuilder();


		constraintViolation.forEach(c -> {
			stringBuilder.append(c.getMessage());
			stringBuilder.append(Constants.CSV_DELIMITER);
		});

		String result = stringBuilder.toString();
		if(!result.isEmpty()) {
			return result.substring(0, result.length() - Constants.CSV_DELIMITER.length());
			
		}else {
			return "";
		}
	}

	
	public static boolean doesFileExists(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}


	public static List<String> getRecords() {
		return records;
	}


	public static void setRecords(List<String> records) {
		UploadMetaData.records = records;
	}


	public static String getInsurer() {
		return insurer;
	}


	public static void setInsurer(String insurer) {
		UploadMetaData.insurer = insurer;
	}


	public static String getSnapShotDate() {
		return snapShotDate;
	}


	public static void setSnapShotDate(String snapShotDate) {
		UploadMetaData.snapShotDate = snapShotDate;
	}


	public static String getInsurerImageDocumentRoot() {
		return insurerImageDocumentRoot;
	}


	public static void setInsurerImageDocumentRoot(String insurerImageDocumentRoot) {
		UploadMetaData.insurerImageDocumentRoot = insurerImageDocumentRoot;
	}


	public static String getMetadataLocation() {
		return metadataLocation;
	}


	public static void setMetadataLocation(String metadataLocation) {
		UploadMetaData.metadataLocation = metadataLocation;
	}

}
