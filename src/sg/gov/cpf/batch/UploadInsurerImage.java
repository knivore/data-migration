package sg.gov.cpf.batch;

import java.io.File;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.xml.ws.soap.SOAPFaultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sg.gov.cpf.meta.model.UnsentImages;
import sg.gov.cpf.utilities.Constants;
import sg.gov.cpf.utilities.JdbcDatabaseUtil;
import sg.gov.cpf.utilities.MigrationUtil;
import sg.gov.cpf.utilities.RCubeService;

public class UploadInsurerImage {

	private static Logger logger = LogManager.getLogger(UploadInsurerImage.class.getName());
	private static String filePathSeperator = MigrationUtil.getSystemPathSeperator();
	
	private static String insurer;
	private static String insurerImageDocumentRoot;
	private static int numberOfImagesToTransfer;		
	private static int rcubeReAuthenticationInterval; 
	
	//TODO Retrieve from configuration file
	private static String rcubeUserName;
	private static String rcubePassword;
   
	private static String insurerOptions = Constants.AVI_INSURER_NAME + "/" + Constants.NTUC_INSURER_NAME + "/" + Constants.GE_INSURER_NAME;
	
	private static int fileSentCount = 0;

	private static RCubeService rcubeService = new RCubeService();

	public static void main(String[] args) {

		logger.info("UploadInsurerImage batch job arguments: " + Arrays.toString(args));

		//arg 0 - insurer type
		//arg 1 - insurer image location 
		//arg 2 - number of images to transfer(number) // can max this? //logs
		//arg 3 - RCube re-authentication interval

		if (inputIsValid(args)) {
			logger.info("valid arguments");

			//Call setters
			setInsurer(args[0]);
			setInsurerImageDocumentRoot(args[1]);
			setNumberOfImagesToTransfer(Integer.parseInt(args[2]));
			setRcubeReAuthenticationInterval(Integer.parseInt(args[3]));

			//Call retrieve token
			try {
				
				List<UnsentImages> unsentImages = JdbcDatabaseUtil.retrieveUnsentImages(getNumberOfImagesToTransfer(), retrieveInsurerFileDetailTable(getInsurer()));
				
				String authenticationToken = rcubeService.retrieveAuthenticationToken(getRcubeUserName(), getRcubePassword());
//				String authenticationToken  = "abc";
				
				if(authenticationToken != null) {
					for (int x = 0; x < unsentImages.size(); x++) {

						UnsentImages unsentImagesTemp = unsentImages.get(x);
						logger.info("UnsentImages info " + unsentImagesTemp.toString());

						String filePath = getInsurerImageDocumentRoot() + filePathSeperator + unsentImagesTemp.getFileName();

						//RCube folder
						int rcubeFolderParentId = retrieveRCubeFolderParentId(unsentImagesTemp.getAttachmentType());
						
						String rcubeUploadedRes = rcubeService.uploadFile(authenticationToken, filePath, rcubeFolderParentId);
						fileSentCount++;

						//Renew authentication token
						if(fileSentCount >= rcubeReAuthenticationInterval) {
							authenticationToken = rcubeService.retrieveAuthenticationToken(getRcubeUserName(), getRcubePassword());
						}

						try {
							
							if (isValidNumber(rcubeUploadedRes)) { // document created code found, file has been uploaded
								JdbcDatabaseUtil.updateImageSentStatus(retrieveInsurerFileDetailTable(getInsurer()), unsentImagesTemp.getDbid(), 'Y', null, Integer.parseInt(rcubeUploadedRes));
							} else {
								JdbcDatabaseUtil.updateImageSentStatus(retrieveInsurerFileDetailTable(getInsurer()), unsentImagesTemp.getDbid(), 'N', rcubeUploadedRes, -1);
							}
						} catch (SQLException e) {
							logger.warn("Unable to update Image Sent Status for file " + unsentImagesTemp.getFileName() + ": " + e.getMessage());
						}
					}
				} else {
					logger.warn("Unable to retrieve initial RCube Auth token, check if RCube can be accessed from this machine.");
					logger.warn("Batch program exiting");
					System.exit(1);
				}
				
			} catch (SOAPFaultException | SQLException sfe) {
				logger.warn(sfe.getMessage());
			}
		} else {
			logger.warn("Invalid inputs passed into batch program,"
					+ " check that arg 1 - Insurer Type(" + insurerOptions + "),"
					+ " arg 2 - Insurer's Image Location,"
					+ " arg 3 - Number of images to transfer,"
					+ " arg 4 - Reauthentication interval for files sent to RCube");
			System.exit(1);
		}
	}

	private static int retrieveRCubeFolderParentId(String attachmentType) {
		
		int parentId = 0;

		switch (attachmentType) {
		case Constants.APPLICATION_ATTACHMENT_TYPE:
			parentId = Constants.RCUBE_APPLICATION_FOLDER_PARENT_ID; //2519959;
			break;

		case Constants.CORRESPONDENCE_ATTACHMENT_TYPE:
			parentId = Constants.RCUBE_CORRESPONDENCE_FOLDER_PARENT_ID; 
			parentId = 2520397;
			break;
		default:
			parentId = -1;
			break;
		}
		
		
		return parentId;
	}

	
	private static boolean inputIsValid(String[] args) {
		boolean result = true;

		if(args.length >= 4) { 

			if(!isInsurerTypeValid(args[0])) {
				result = false;
			}
			
			
			if(!isImageLocationValid(args[1])) {
				result = false;
			}
			
			if(!isValidNumber(args[2])) {
				result = false;
			} else {
				if(Integer.parseInt(args[2]) <= 0) {
					result = false;
				}
				
			}

			if(!isValidNumber(args[3])) {
				result = false;
			} else {
				if(Integer.parseInt(args[2]) <= 0) {
					result = false;
				}
			}
			
		} else {
			result =  false;

			logger.warn("Invalid number of inputs passed into batch program,"
					+ " check that arg 1 - Insurer Type(" + insurerOptions + "),"
					+ " arg 2 - Insurer's Image Location,"
					+ " arg 3 - Number of images to transfer,"
					+ " arg 4 - Reauthentication interval for files sent to RCube");
		}
		return result;
	}

	/**
	 * Checks is input is a number and >= 0
	 * @param number
	 * @return
	 */
	private static boolean isValidNumber(String number) {
		try {
			Integer.parseInt(number);
		}catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	private static boolean isInsurerTypeValid(String insurerType) {
		boolean result = true;
		List<String> insurerList = Arrays.asList(new String[]{Constants.AVI_INSURER_NAME, Constants.NTUC_INSURER_NAME, Constants.GE_INSURER_NAME});
		if(!insurerList.contains(insurerType)) {
			result = false;
			logger.warn("Invalid insurer type passed into batch program : " + insurerType);
		}
		return result;
	}

	private static boolean isImageLocationValid(String imageLocation) {
		boolean result = true;
		if (!doesFileExists(imageLocation)) {
			logger.warn("Image Location does not exists at : " + imageLocation);
			result = false;
		}
		return result;
	}



	public static boolean doesFileExists(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}
	
	private static String retrieveInsurerFileDetailTable(String insurer) {

		String tableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case Constants.AVI_INSURER_NAME:
				tableName = Constants.AVI_FILE_DETAILS_TABLE;
				break;
			case Constants.NTUC_INSURER_NAME:
				tableName = Constants.NTUC_FILE_DETAILS_TABLE;
				break;
			case Constants.GE_INSURER_NAME:
				tableName = Constants.GE_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warn("In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return tableName;
	}
	public static String getInsurer() {
		return insurer;
	}

	public static void setInsurer(String insurer) {
		UploadInsurerImage.insurer = insurer;
	}

	public static String getInsurerImageDocumentRoot() {
		return insurerImageDocumentRoot;
	}

	public static void setInsurerImageDocumentRoot(String insurerImageDocumentRoot) {
		UploadInsurerImage.insurerImageDocumentRoot = insurerImageDocumentRoot;
	}

	public static int getNumberOfImagesToTransfer() {
		return numberOfImagesToTransfer;
	}

	public static void setNumberOfImagesToTransfer(int numberOfImagesToTransfer) {
		UploadInsurerImage.numberOfImagesToTransfer = numberOfImagesToTransfer;
	}

	public static int getRcubeReAuthenticationInterval() {
		return rcubeReAuthenticationInterval;
	}

	public static void setRcubeReAuthenticationInterval(int rcubeReAuthenticationInterval) {
		UploadInsurerImage.rcubeReAuthenticationInterval = rcubeReAuthenticationInterval;
	}

	public static String getRcubeUserName() {
		return rcubeUserName;
	}

	public static void setRcubeUserName(String rcubeUserName) {
		UploadInsurerImage.rcubeUserName = rcubeUserName;
	}

	public static String getRcubePassword() {
		return rcubePassword;
	}

	public static void setRcubePassword(String rcubePassword) {
		UploadInsurerImage.rcubePassword = rcubePassword;
	}
}
