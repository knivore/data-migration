package sg.gov.cpf;

import org.apache.commons.codec.digest.DigestUtils;

import sg.gov.cpf.utilities.DMLogger;
import sg.gov.cpf.utilities.MigrationUtil;
import sg.gov.cpf.utilities.SliftDecryptor;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
/**
 * @deprecated
 * @author John
 *
 */
public class Step1 {

	private JTextField step1BrowseTextField;
	private JTextField step1DestinationTextField;
	private JTextField step1DecryptionKeyTextField;
	private JLabel lblStatus;
	private JButton step1ExecuteBtn;
	private static final String UI_FONT = "Tahoma";
	private static final String DECRYPTION_KEY_EXTENSION = "pfx";
	private static final String ENCRYPTED_FILE_EXTENSION = "p7";
	private static final DMLogger logger = new DMLogger(Step1.class);

	/**
	 * Initialize the contents of step 1 panel.
	 */
	public void initializeStep1(JTabbedPane tabbedPane) {
		/*
		 * Setting up Panel for Step 1
		 */
		JPanel step1Panel = new JPanel();
		step1Panel.setToolTipText(Step1Text.TOOL_TIP_PANEL.text);
		step1Panel.setLayout(null);
		tabbedPane.addTab(Step1Text.HTML_TAB_PANEL.text, step1Panel);

		JLabel step1Lbl = new JLabel(Step1Text.LABEL_PANEL.text);
		step1Lbl.setBounds(170, 19, 484, 19);
		step1Lbl.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		step1Panel.add(step1Lbl);

		/* Label & Textbox for File Location */
		JLabel lblFileLocation = new JLabel(Step1Text.LABEL_FILE_LOCATION.text);
		lblFileLocation.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		lblFileLocation.setBounds(54, 71, 111, 23);
		step1Panel.add(lblFileLocation);

		step1BrowseTextField = new JTextField();
		step1BrowseTextField.setEnabled(false);
		step1BrowseTextField.setBounds(221, 66, 320, 29);
		step1BrowseTextField.setColumns(10);
		step1BrowseTextField.setToolTipText(lblFileLocation.getText());
		step1Panel.add(step1BrowseTextField);

		JButton step1BrowseBtn = new JButton(Step1Text.BUTTON_TEXT_BROWSE_SOURCE_FILE.text);
		step1BrowseBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				/* Select directory where all zip files are located */
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle(Step1Text.DIALOG_TITLE_BROWSE_SOURCE_FILE.text);
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fileChooser.setAcceptAllFileFilterUsed(false);
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					step1BrowseTextField.setText(selectedFile.getAbsolutePath());
					logger.print(Step1Text.INFO_SOURCE_DIRECTORY_SELECTED.text, selectedFile.getAbsolutePath());
					/* Check zip count and md5 count and show in status */
					int zipCount = MigrationUtil.getFilesByType(selectedFile.getAbsolutePath(), "zip").length;
					int md5Count = MigrationUtil.getFilesByType(selectedFile.getAbsolutePath(), "md5").length;
					lblStatus.setText(format(Step1Text.INFO_SOURCE_DIRECTORY_FILE_COUNT.text, zipCount, md5Count));
					
				}
				
			}
		});
		step1BrowseBtn.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		step1BrowseBtn.setBounds(575, 64, 186, 31);
		step1Panel.add(step1BrowseBtn);

		/* Label & Textbox for File Destination */
		JLabel lblFileDestination = new JLabel(Step1Text.LABEL_FILE_DESTINATION.text);
		lblFileDestination.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		lblFileDestination.setBounds(54, 120, 111, 23);
		step1Panel.add(lblFileDestination);

		step1DestinationTextField = new JTextField();
		step1DestinationTextField.setEnabled(false);
		step1DestinationTextField.setColumns(10);
		step1DestinationTextField.setBounds(221, 115, 320, 29);
		step1DestinationTextField.setToolTipText(lblFileDestination.getText());
		step1Panel.add(step1DestinationTextField);

		JButton destinationBtn = new JButton(Step1Text.BUTTON_TEXT_BROWSE_DESTINATION.text);
		destinationBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle(Step1Text.DIALOG_TITLE_BROWSE_DESTINATION.text);
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					step1DestinationTextField.setText(selectedFile.getAbsolutePath());
					logger.print(Step1Text.INFO_DESTINATION_SELECTED.text, selectedFile.getAbsolutePath());
				}

			}
		});
		destinationBtn.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		destinationBtn.setBounds(575, 114, 186, 31);
		step1Panel.add(destinationBtn);
		
		/* Label & Textbox for SLIFT private key */
		JLabel lblDecryptionKeyLocation = new JLabel(Step1Text.LABEL_DECRYPTION_KEY.text);
		lblDecryptionKeyLocation.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		lblDecryptionKeyLocation.setBounds(54, 169, 111, 23);
		step1Panel.add(lblDecryptionKeyLocation);
		
		step1DecryptionKeyTextField = new JTextField();
		step1DecryptionKeyTextField.setEnabled(false);
		step1DecryptionKeyTextField.setColumns(10);
		step1DecryptionKeyTextField.setBounds(221, 164, 320, 29);
		step1DecryptionKeyTextField.setToolTipText(lblDecryptionKeyLocation.getText());
		step1Panel.add(step1DecryptionKeyTextField);
		
		JButton decryptionKeyBtn = new JButton(Step1Text.BUTTON_TEXT_DECRYPTION_KEY.text);
		decryptionKeyBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle(Step1Text.DIALOG_TITLE_BROWSE_DECRYPTION_KEY.text);
				fileChooser.setAcceptAllFileFilterUsed(false);
		        FileNameExtensionFilter filter = 
		        		new FileNameExtensionFilter("." + DECRYPTION_KEY_EXTENSION + " files", DECRYPTION_KEY_EXTENSION);
		        fileChooser.addChoosableFileFilter(filter);
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					step1DecryptionKeyTextField.setText(selectedFile.getAbsolutePath());
					logger.print(Step1Text.INFO_DECRYPTION_KEY_SELECTED.text, selectedFile.getAbsolutePath());
				}

			}
		});
		decryptionKeyBtn.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		decryptionKeyBtn.setBounds(575, 164, 186, 31);
		step1Panel.add(decryptionKeyBtn);

		/* Execute Button for Step 1 */
		step1ExecuteBtn = new JButton(Step1Text.BUTTON_TEXT_EXECUTE.text);
		step1ExecuteBtn.addActionListener(new ExecuteAction());
		step1ExecuteBtn.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		step1ExecuteBtn.setBounds(531, 259, 131, 31);
		step1Panel.add(step1ExecuteBtn);

		/* Export Logs Button for Step 1 */
		JButton step1ExportLogsBtn = new JButton(Step1Text.BUTTON_TEXT_EXPORT_LOGS.text);
		step1ExportLogsBtn.addActionListener(new ExportAction());
		step1ExportLogsBtn.setFont(new Font(UI_FONT, Font.PLAIN, 15));
		step1ExportLogsBtn.setBounds(10, 259, 170, 31);
		step1Panel.add(step1ExportLogsBtn);
		
		/* label for status */
		lblStatus = new JLabel();
		lblStatus.setFont(new Font(UI_FONT, Font.PLAIN, 12));
		lblStatus.setBounds(221, 324, 440, 23);
		step1Panel.add(lblStatus);
		
		MigrationUtil.terminateLoggerThread(logger);
	}
	
	private class ExportAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			/* export Logs to text file */
			try {
				MigrationUtil.alert(Step1Text.INFO_LOG_EXPORTED.text,logger.export());
			} catch (IOException e1) {
				e1.printStackTrace();
				logger.print(e1.getMessage());
			}
		}
	}

	private class ExecuteAction implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			step1ExecuteBtn.setEnabled(false);
			final String sourceDirectory = step1BrowseTextField.getText();
			final String destination = step1DestinationTextField.getText();
			final String decryptionKeyLocation = step1DecryptionKeyTextField.getText();
			/* Step 1: Validate all inputs - Not empty, Valid file location */
			lblStatus.setText(Step1Text.STATUS_CHECKING_FILE_LOCATIONS.text);
			if (!MigrationUtil.validateFileLocation(logger, false, sourceDirectory, destination, decryptionKeyLocation)) {
				lblStatus.setText(Step1Text.STATUS_EXECUTION_FAILED_CHECK_INPUTS.text);
				step1ExecuteBtn.setEnabled(true);
				return;
			}
			logger.print(Step1Text.INFO_LOCATIONS_VALIDATED.text, sourceDirectory, destination);
			
			/* Step 2 - 3: Open file from location text box and MD5 checksum */
			lblStatus.setText(Step1Text.STATUS_CHECKING_FILE_INTEGRITY.text);
			File[] zipFiles = MigrationUtil.getFilesByType(sourceDirectory, "zip");
			/* No zip files present in source directory, stop execution */
			if(zipFiles.length == 0) {
				logger.print(Step1Text.ERROR_NO_ZIP_FILES_FOUND.text, sourceDirectory);
				lblStatus.setText(format(Step1Text.STATUS_EXECUTION_FAILED_NO_ZIP.text, sourceDirectory));
				step1ExecuteBtn.setEnabled(true);
				return;
			}
			/* Validate each zip file's MD5 checksum using .MD5 file of same name */
			List<File> filesToUnzip = processFilesMD5(zipFiles, sourceDirectory);
			if(filesToUnzip.isEmpty()) {
				logger.print(Step1Text.STATUS_EXECUTION_STOPPED.text);
				lblStatus.setText(Step1Text.STATUS_EXECUTION_STOPPED.text);
				step1ExecuteBtn.setEnabled(true);
				return;
			}
			/* Step 4: Unzip file to destination */
			lblStatus.setText(Step1Text.STATUS_UNZIPPING_FILES.text);
			
			List<File> unzippedFiles = new ArrayList<>();
			filesToUnzip.forEach(file -> {
				String path = file.getAbsolutePath();
				logger.print(Step1Text.INFO_UNZIPPING_FILE.text, path);
				List<File> unzippedFilesSingle = unzipFile(path, destination);
				if(unzippedFilesSingle.isEmpty()) {
					logger.print(Step1Text.WARNING_NO_FILES_UNZIPPED.text, path);
				} else {
					unzippedFiles.addAll(unzippedFilesSingle);
					logger.print(Step1Text.INFO_FILES_UNZIPPED.text, unzippedFilesSingle.size(), path, destination);
				}
			});
			logger.print(Step1Text.INFO_FILES_UNZIPPED_TOTAL.text, unzippedFiles.size(), filesToUnzip.size(), sourceDirectory);
			/*
			 *  Step 5: Verify all unzipped files are encrypted,
			 *  then decrypt them using SLIFT with private key
			 */
			lblStatus.setText(Step1Text.STATUS_DECRYPTING_FILES.text);
			/* Ensure that files are of the valid encrypted file type */
			List<File> encryptedFiles = new ArrayList<>();
			for(File file : unzippedFiles) {
				if(MigrationUtil.validateFileExtension(logger, file, false, ENCRYPTED_FILE_EXTENSION)) {
					encryptedFiles.add(file);
				} else {
					logger.print(Step1Text.ERROR_NOT_ENCRYPTED_FILE.text, file.getName(), ENCRYPTED_FILE_EXTENSION);
				}
			}
			if(encryptedFiles.isEmpty()) {
				logger.print(Step1Text.WARNING_NO_ENCRYPTED_FILES.text);
				lblStatus.setText(format(Step1Text.STATUS_EXECUTION_COMPLETED_NO_ENCRYPTED_FILES.text, unzippedFiles.size()));
				logger.print(Step1Text.STATUS_EXECUTION_COMPLETED_NO_ENCRYPTED_FILES.text, unzippedFiles.size());
				step1ExecuteBtn.setEnabled(true);
				return;
			}

			SliftDecryptor decryptor = new SliftDecryptor(
					decryptionKeyLocation,Step1Text.DECRYPTION_COMMAND.text, destination, logger);
			
			List<String> decryptedFiles = decryptor.decryptFiles(encryptedFiles);
			lblStatus.setText(format(Step1Text.STATUS_EXECUTION_COMPLETED_FULL.text, unzippedFiles.size(), decryptedFiles.size(), encryptedFiles.size()));
			logger.print(Step1Text.STATUS_EXECUTION_COMPLETED_FULL.text, unzippedFiles.size(), decryptedFiles.size(), encryptedFiles.size());
			step1ExecuteBtn.setEnabled(true);
			
			//TODO: to remove the original .p7 files of the decrypted files from the destination folder
		}
		private boolean validateFileMD5(String filePath) {
			String expectedMD5 = "expected";
			/* Check corresponding .md5 file */
			String md5Location = filePath.substring(0, filePath.lastIndexOf(".")) + ".md5";
			try {
				BufferedReader br = new BufferedReader(new FileReader(md5Location));
				expectedMD5 = br.readLine();
				logger.print(Step1Text.INFO_MD5_EXPECTED.text, expectedMD5, md5Location);
				br.close();
			} catch (FileNotFoundException e1) {
				logger.print(Step1Text.ERROR_MD5_FILE_NOT_FOUND.text, md5Location);
				return false;
			} catch (IOException e2) {
				logger.print(Step1Text.ERROR_MD5_FILE_UNABLE_TO_READ.text, md5Location);
				logger.print(e2.getMessage());
				return false;
			}
			String actualMD5 = "actual";
			try (InputStream is = Files.newInputStream(Paths.get(filePath))) {
				actualMD5 = DigestUtils.md2Hex(is);
				logger.print(Step1Text.INFO_MD5_ACTUAL.text, actualMD5, filePath);
			} catch (IOException e) {
				e.printStackTrace();
				logger.print(e.getMessage());
			}
			if (actualMD5.equalsIgnoreCase(expectedMD5)) {
				return true;
			} else {
				logger.print(Step1Text.ERROR_MD5_NOT_MATCH.text);
				return false;
			}
		}
		private List<File> processFilesMD5(File[] zipFiles, String sourceDirectory){
			List<File> validatedMD5ZipFiles = new ArrayList<>();
			for(File zipFile : zipFiles) {
				if (!validateFileMD5(zipFile.getAbsolutePath())) {
					logger.print(Step1Text.ERROR_MD5_VALIDATION.text, sourceDirectory);
				} else {
					logger.print(Step1Text.INFO_MD5_VALIDATED.text, sourceDirectory);
					validatedMD5ZipFiles.add(zipFile);
				}
			}
			logger.print(Step1Text.INFO_MD5_VALIDATION_SUMMARY.text, sourceDirectory, zipFiles.length, validatedMD5ZipFiles.size());
			/* Prompt user if not all zip files passed MD5 validation */
			boolean unzipAll = true;
			if(validatedMD5ZipFiles.size() < zipFiles.length) {
				int result = JOptionPane.showConfirmDialog(null,
						format(Step1Text.DIALOG_QUESTION_MD5_RESULT.text,zipFiles.length - validatedMD5ZipFiles.size()),
						"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if(result == JOptionPane.NO_OPTION) {
					return Collections.emptyList();
				} else {
					int result2 = JOptionPane.showConfirmDialog(null,
							Step1Text.DIALOG_QUESTION_MD5_UNZIP_ALL.text,
							"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
					if(result2 == JOptionPane.NO_OPTION) {
						unzipAll = false;
						logger.print(Step1Text.INFO_USER_SELECT_UNZIP_MD5.text);
					} else {
						logger.print(Step1Text.INFO_USER_SELECT_UNZIP_ALL.text);
					}
				}
			}
			return unzipAll ? Arrays.asList(zipFiles) : validatedMD5ZipFiles;
		}
		private List<File> unzipFile(String filePath, String destination) {
			List<File> unzippedFiles = new ArrayList<>();
			byte[] buffer = new byte[1024];
			try {
				File file = new File(filePath);
				if (!file.exists()) {
					return Collections.emptyList();
				}
				ZipInputStream zis = new ZipInputStream(new FileInputStream(filePath));
				ZipEntry zipEntry = zis.getNextEntry();
				while (zipEntry != null) {
					File newFile = newFile(new File(destination), zipEntry);
					if(newFile != null) {
						FileOutputStream fos = new FileOutputStream(newFile);
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
						fos.close();
						logger.print(Step1Text.INFO_FILE_UNZIPPED.text, newFile.getName(), destination);
						unzippedFiles.add(newFile);
					}
					zipEntry = zis.getNextEntry();
				}
				zis.closeEntry();
				zis.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.print(e.getMessage());
			}
			return unzippedFiles;
		}
		private File newFile(File destination, ZipEntry zipEntry) throws IOException {
			File destFile = new File(destination, zipEntry.getName());
			/* if file already exists, return */
			if (destFile.exists()) {
				logger.print(Step1Text.ERROR_UNZIP_FILE_EXISTS.text, destFile.getName(), destination);
				return null;
			}
			String destDirPath = destination.getCanonicalPath();
			String destFilePath = destFile.getCanonicalPath();
			if (!destFilePath.startsWith(destDirPath + File.separator)) {
				throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
			}
			return destFile;
		}
	}
	public enum Step1Text {
		BUTTON_TEXT_BROWSE_SOURCE_FILE("Browse to File Location"),
		BUTTON_TEXT_BROWSE_DESTINATION("Browse to Destination"),
		BUTTON_TEXT_DECRYPTION_KEY("Browse"),
		BUTTON_TEXT_EXECUTE("Execute"),
		BUTTON_TEXT_EXPORT_LOGS("Export Logs"),
		DECRYPTION_COMMAND("sliftc /d \"%s\" \"%s\" /pfx \"%s\" %s"),
		DIALOG_TITLE_BROWSE_SOURCE_FILE("Select sorce ZIP file location"),
		DIALOG_TITLE_BROWSE_DESTINATION("Select destination file directory"),
		DIALOG_TITLE_BROWSE_DECRYPTION_KEY("Select SLIFT private key file"),
		DIALOG_QUESTION_MD5_RESULT("{0} file(s) failed MD5 validation. Proceed to unzip?"),
		DIALOG_QUESTION_MD5_UNZIP_ALL("Unzip all files? Select No to unzip files that passed MD5 validation only"),
		ERROR_MD5_FILE_NOT_FOUND("ERROR: {0} not found"),
		ERROR_MD5_FILE_UNABLE_TO_READ("ERROR : unable to read MD5 file : {0}"),
		ERROR_MD5_NOT_MATCH("ERROR: MD5 do not match"),
		ERROR_MD5_VALIDATION("ERROR : file MD5 validation. Source file: {0}"),
		ERROR_NO_ZIP_FILES_FOUND("No zip files found in source directory: {0}"),
		ERROR_NOT_ZIP_FILE("ERROR : Unable to unzip, selected file is not a zip file. {0}"),
		ERROR_NOT_ENCRYPTED_FILE("ERROR : <{0}> is not an encrypted file. Extension required: .{1}"),
		ERROR_UNZIP_FILE_EXISTS("ERROR : Unable to unzip <{0}>, file with same name already exists in target directory {1}"),
		HTML_TAB_PANEL("<html><body><table width='260'>Step 1: MD5, Unzip & Decrypt</table></body></html>"),
		INFO_SOURCE_DIRECTORY_SELECTED("source directory directory selected : {0}"),
		INFO_SOURCE_DIRECTORY_FILE_COUNT("Detected {0} x zip files and {1} x md5 files in source directory"),
		INFO_DESTINATION_SELECTED("file destination location selected : {0}"),
		INFO_DECRYPTION_KEY_SELECTED("decryption key selected : {0}"),
		INFO_LOG_EXPORTED("Log exported to {0}"),
		INFO_LOCATIONS_VALIDATED("file locations validated. Source file: {0}, Target directory: {1}"),
		INFO_MD5_ACTUAL("actual MD5 = {0} ({1})"),
		INFO_MD5_EXPECTED("expected MD5 provided = {0} ({1})"),
		INFO_MD5_VALIDATED("file MD5 validated. {0}"),
		INFO_MD5_VALIDATION_SUMMARY("MD5 Validation Summary - Directory: {0}, total zip files: {1}, MD5 validated zip files: {2}"),
		INFO_FILE_UNZIPPED("File : <{0}> successfully created in target directory: {1}"),
		INFO_FILES_UNZIPPED("{0} files unzipped. Source file: {1}, Target directory: {2}"),
		INFO_FILES_UNZIPPED_TOTAL("Unzip Summary - Total {0} files unzipped from {1} zip files in {2}"),
		INFO_UNZIPPING_FILE("Unzipping file: <{0}>"),
		INFO_USER_SELECT_UNZIP_ALL("User selected to unzip all files including files that failed MD5 validation"),
		INFO_USER_SELECT_UNZIP_MD5("User selected to unzip files that passed MD5 validation only"),
		LABEL_PANEL("Step 1: MD5 Checksum Verification, Unzip & Decryption on File Destination"),
		LABEL_FILE_LOCATION("File Location :"),
		LABEL_FILE_DESTINATION("File Destination :"),
		LABEL_DECRYPTION_KEY("Decryption Key :"),
		STATUS_CHECKING_FILE_LOCATIONS("Checking input file locations..."),
		STATUS_CHECKING_FILE_INTEGRITY("Validating MD5 Checksum..."),
		STATUS_UNZIPPING_FILES("Unzipping files..."),
		STATUS_DECRYPTING_FILES("Decrypting unzipped files using SLIFT..."),
		STATUS_EXECUTION_FAILED_CHECK_INPUTS("Execution failed to complete, please check inputs and try again."),
		STATUS_EXECUTION_FAILED_NO_ZIP("Execution failed to start, no zip files found in source directory."),
		STATUS_EXECUTION_COMPLETED_NO_UNZIPPED_FILES("Execution completed, no files unzipped."),
		STATUS_EXECUTION_COMPLETED_NO_ENCRYPTED_FILES("Execution completed: {0} files unzipped, 0 encrypted files."),
		STATUS_EXECUTION_COMPLETED_FULL("Execution completed: {0} files unzipped. {1}/{2} files decrypted."),
		STATUS_EXECUTION_STOPPED("Execution stopped."),
		TOOL_TIP_PANEL("MD5 Checksum Verification, Unzip & Decryption of File"),
		WARNING_NO_FILES_UNZIPPED("WARNING : no files unzipped. Source file: {0}"),
		WARNING_NO_ENCRYPTED_FILES("WARNING : no encypted files found in unzipped files.");
		
		private final String text;
		private Step1Text(String text) {
			this.text = text;
		}
		@Override
		public String toString() {
			return text;
		}
	}
	
	private static final String format(String format, Object... arguments) {
		return java.text.MessageFormat.format(format, arguments);
	}
}
