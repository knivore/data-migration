package sg.gov.cpf.meta.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class RecordHeader {

	@NotEmpty(message = "Record Type cannot be empty")
	@Pattern(regexp = "1|2|9", message = "Record Type can only be 1, 2 or 9")
	public String recordType;

	@NotEmpty(message = "Insurer Tag cannot be empty")
	@Pattern(regexp = "AVI|GEL|NTU", message = "Insurer Tag can only be AVI, GEL or NTU")
	public String insurerTag;
	
	@NotEmpty(message = "Snapshot Date cannot be empty")
	@Pattern(regexp = "^\\d{4}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])$", message = "Snapshot Date must be in YYYYMMDD format")
	public String snapshotDate;

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getInsurerTag() {
		return insurerTag;
	}

	public void setInsurerTag(String insurerTag) {
		this.insurerTag = insurerTag;
	}

	public String getSnapshotDate() {
		return snapshotDate;
	}

	public void setSnapshotDate(String snapshotDate) {
		this.snapshotDate = snapshotDate;
	}

	@Override
	public String toString() {
		return "RecordHeader [recordType=" + recordType + ", insurerTag=" + insurerTag + ", snapshotDate="
				+ snapshotDate + "]";
	}

}
