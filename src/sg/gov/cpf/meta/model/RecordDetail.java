package sg.gov.cpf.meta.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class RecordDetail extends RCubeStatus{
	
	@NotEmpty(message = "INCORRECT RECORD TYPE EMPTY VALUE")
	@Pattern(regexp = "1|2|9", message = "INCORRECT RECORD TYPE VALUE")
	@Size(max=1, message = "INCORRECT RECORD TYPE LENGTH")
	public String recordType;

	@NotEmpty(message = "INCORRECT SYSTEM ID EMPTY VALUE")
	@Size(max=32, message = "INCORRECT SYSTEM ID LENGTH")
	public String systemId;

	@NotEmpty(message = "INCORRECT UPLOADED BY EMPTY VALUE")
	@Size(max=13, message = "INCORRECT UPLOADED BY LENGTH")
	@Pattern(regexp = "AVI|GEL|NTU", message = "INCORRECT UPLOADED BY VALUE")
	public String uploadedBy;

	@NotEmpty(message = "INCORRECT FILE NAME EMPTY VALUE")
	@Size(max=212, message = "INCORRECT FILE NAME LENGTH")
	@Pattern(regexp = "^.*\\.(|jpg|jpeg|jpe|gif|png|doc|docx|ppt|xls|xlsx|pdf|txt|tiff|tif|msg|htm|html|log|xps|)$", message = "INCORRECT FILE NAME EXTENSION")
	public String fileName;
	
	@NotEmpty(message = "INCORRECT FILE TYPE EMPTY VALUE")
	@Size(max=4, message = "INCORRECT FILE TYPE LENGTH")
	@Pattern(regexp = "^(|jpg|jpeg|jpe|gif|png|doc|docx|ppt|xls|xlsx|pdf|txt|tiff|tif|msg|htm|html|log|xps|)$", message = "INCORRECT FILE TYPE VALUE")
	public String fileType;

	@NotEmpty(message = "INCORRECT UPLOADED ON EMPTY VALUE")
	@Size(min=14, max=14, message="INCORRECT UPLOADED ON LENGTH") //20200131093000
	@Pattern(regexp = "([0-9]{4})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])(2[0-3]|[01][0-9])([0-5][0-9])([0-5][0-9])", message = "INCORRECT UPLOADED ON VALUE")
	public String uploadedOn;

	@NotEmpty(message = "INCORRECT ATTACHMENT TYPE EMPTY VALUE")
	@Size(max=14, message="INCORRECT ATTACHMENT TYPE LENGTH")
	@Pattern(regexp = "^[A-Z]+$", message="INCORRECT ATTACHMENT TYPE NOT UPPERCASE")
	@Pattern(regexp = "APPLICATION|CORRESPONDENCE", message="INCORRECT ATTACHMENT TYPE VALUE")
	public String attachmentType;

	@NotEmpty(message = "INCORRECT POLICY HOLDER REFERENCE ID EMPTY VALUE")
	@Size(max=20, message="INCORRECT POLICY HOLDER REFERENCE ID LENGTH")
	public String policyHolderReferenceId;

	@NotEmpty(message = "INCORRECT POLICY REFERENCE ID EMPTY VALUE")
	@Size(max=10, message="INCORRECT POLICY REFERENCE ID LENGTH")
	public String policyReferenceId;

	@Size(max=1, message="INCORRECT ARCHIVED RECORD TAG LENGTH")
	@Pattern(regexp = "Y|N", message="INCORRECT ARCHIVED RECORD TAG VALUE")
	public String archivedRecordTag;

	@NotEmpty(message = "INCORRECT HASH EMPTY VALUE")
	@Size(max=128, message="INCORRECT HASH LENGTH")
	public String hash;

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(String uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getPolicyHolderReferenceId() {
		return policyHolderReferenceId;
	}

	public void setPolicyHolderReferenceId(String policyHolderReferenceId) {
		this.policyHolderReferenceId = policyHolderReferenceId;
	}

	public String getPolicyReferenceId() {
		return policyReferenceId;
	}

	public void setPolicyReferenceId(String policyReferenceId) {
		this.policyReferenceId = policyReferenceId;
	}

	public String getArchivedRecordTag() {
		return archivedRecordTag;
	}

	public void setArchivedRecordTag(String archivedRecordTag) {
		this.archivedRecordTag = archivedRecordTag;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	@Override
	public String toString() {
		return "RecordDetail [recordType=" + recordType + ", systemId=" + systemId + ", uploadedBy=" + uploadedBy
				+ ", fileName=" + fileName + ", fileType=" + fileType + ", uploadedOn=" + uploadedOn
				+ ", attachmentType=" + attachmentType + ", policyHolderReferenceId=" + policyHolderReferenceId
				+ ", policyReferenceId=" + policyReferenceId + ", archivedRecordTag=" + archivedRecordTag + ", hash="
				+ hash + "]";
	}

}
