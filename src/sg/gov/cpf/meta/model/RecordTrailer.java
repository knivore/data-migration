package sg.gov.cpf.meta.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class RecordTrailer {

	@NotNull(message = "Record Type cannot be empty")
	@Pattern(regexp = "1|2|9", message = "Record Type can only be 1, 2 or 9")
	public String recordType;

	@NotNull(message = "Record Count cannot be empty")
	@Pattern(regexp = "^[1-9]\\d*$", message = "Record Count should be a positive number")
	public String recordCount;

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}

	@Override
	public String toString() {
		return "RecordTrailer [recordType=" + recordType + ", recordCount=" + recordCount + "]";
	}

}
