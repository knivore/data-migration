package sg.gov.cpf.meta.model;

public class RecordDetailError extends AuditFields {

	private String snapShotDate;
	private String systemId;
	private String filename;
	private String referenceIdPolicyholder;
	private String referenceIdPolicy;
	private String errorRecord;
	private String errorMessage;

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getReferenceIdPolicyholder() {
		return referenceIdPolicyholder;
	}

	public void setReferenceIdPolicyholder(String referenceIdPolicyholder) {
		this.referenceIdPolicyholder = referenceIdPolicyholder;
	}

	public String getReferenceIdPolicy() {
		return referenceIdPolicy;
	}

	public void setReferenceIdPolicy(String referenceIdPolicy) {
		this.referenceIdPolicy = referenceIdPolicy;
	}

	public String getErrorRecord() {
		return errorRecord;
	}

	public void setErrorRecord(String errorRecord) {
		this.errorRecord = errorRecord;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getSnapShotDate() {
		return snapShotDate;
	}

	public void setSnapShotDate(String snapShotDate) {
		this.snapShotDate = snapShotDate;
	}

	@Override
	public String toString() {
		return "RecordDetailError [snapShotDate=" + snapShotDate + ", systemId=" + systemId + ", filename=" + filename
				+ ", referenceIdPolicyholder=" + referenceIdPolicyholder + ", referenceIdPolicy=" + referenceIdPolicy
				+ ", errorRecord=" + errorRecord + ", errorMessage=" + errorMessage + "]";
	}

}
