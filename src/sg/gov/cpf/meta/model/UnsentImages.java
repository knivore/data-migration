package sg.gov.cpf.meta.model;

public class UnsentImages {

	private int dbid;
	private String fileName;
	private String attachmentType;

	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getDbid() {
		return dbid;
	}
	public void setDbid(int dbid) {
		this.dbid = dbid;
	}
	public String getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	

	@Override
	public String toString() {
		return "UnsentImages [dbid=" + dbid + ", fileName=" + fileName + ", attachmentType=" + attachmentType + "]";
	}
	
	
	
}
