package sg.gov.cpf.meta.model;

//TODO validation here
public class RCubeStatus extends AuditFields {

	public String snapShotDate;

	public char rcubeSentFlag;

	public String rcubeFileSentDate;

	public String rcubeErrorMessage;

	public int rcubeDocumentCreatedCode;



	public char getRcubeSentFlag() {
		return rcubeSentFlag;
	}

	public void setRcubeSentFlag(char rcubeSentFlag) {
		this.rcubeSentFlag = rcubeSentFlag;
	}

	public String getRcubeFileSentDate() {
		return rcubeFileSentDate;
	}

	public void setRcubeFileSentDate(String rcubeFileSentDate) {
		this.rcubeFileSentDate = rcubeFileSentDate;
	}

	public String getRcubeErrorMessage() {
		return rcubeErrorMessage;
	}

	public void setRcubeErrorMessage(String rcubeErrorMessage) {
		this.rcubeErrorMessage = rcubeErrorMessage;
	}

	public int getRcubeDocumentCreatedCode() {
		return rcubeDocumentCreatedCode;
	}

	public void setRcubeDocumentCreatedCode(int rcubeDocumentCreatedCode) {
		this.rcubeDocumentCreatedCode = rcubeDocumentCreatedCode;
	}

	public String getSnapShotDate() {
		return snapShotDate;
	}

	public void setSnapShotDate(String snapShotDate) {
		this.snapShotDate = snapShotDate;
	}

}
