package sg.gov.cpf.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.xml.bind.DatatypeConverter;

import sg.gov.cpf.meta.model.RecordDetail;
import sg.gov.cpf.meta.model.RecordHeader;
import sg.gov.cpf.meta.model.RecordTrailer;
import sg.gov.cpf.utilities.Constants;
import sg.gov.cpf.utilities.DMLogger;
import sg.gov.cpf.utilities.JdbcDatabaseUtil;
import sg.gov.cpf.utilities.MigrationUtil;
import sg.gov.cpf.utilities.RecordMapperUtil;

public class ControllerForStep2 {

	private Logger logger = Logger.getLogger(ControllerForStep2.class.getName());
	private DMLogger dmLogger = new DMLogger(ControllerForStep2.class);
	public static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	
	private String filePathSeperator = MigrationUtil.getSystemPathSeperator();
	private String insurer;
	private String snapShotDate;

	public void processMetaData(File metaDataFile, String documentRoot) throws ParseException, SQLException, IOException {


		List<String> metaDataRecords = MigrationUtil.readFile(metaDataFile.getAbsolutePath());

		//Store indexes instead of creating a list of RecordDetail/Error objects, to save on JVM space.
		List<Integer> recordDetailBatchIndexes = new ArrayList<Integer>();
		List<Integer> recordDetailErrorBatchIndexes = new ArrayList<Integer>();

		for (int rowNumber = 1; rowNumber < metaDataRecords.size() - 1; rowNumber++) { // Detail Group, row 1 to ...

			String[] detokenizedMetaDataRecord = metaDataRecords.get(rowNumber).split(Constants.CSV_DELIMITER_FOR_SPLIT);

			RecordDetail recordDetail = RecordMapperUtil.mapToRecordDetail(detokenizedMetaDataRecord);

			Set<ConstraintViolation<RecordDetail>> recordDetailViolations = validator.validate(recordDetail);

			boolean computedMd5OfRecordDetail = isRecordDetailMd5Correct(recordDetail);

			boolean doesFileExists = doesFileExists(documentRoot + filePathSeperator + recordDetail.getFileName());

			if (!recordDetailViolations.isEmpty() || !computedMd5OfRecordDetail || !doesFileExists) {
				recordDetailErrorBatchIndexes.add(rowNumber);
			} else {
				recordDetailBatchIndexes.add(rowNumber);
			}
		}

		if (!recordDetailBatchIndexes.isEmpty()) {
			logger.info(this.getClass().getCanonicalName() + " In processMetaData Start Time recordDetailBatchIndexes insert: " + LocalDateTime.now());
			JdbcDatabaseUtil.insertRecordDetailBatch(recordDetailBatchIndexes, metaDataRecords, getSnapShotDate(), retrieveInsurerFileDetailTable(getInsurer()));
			logger.info(this.getClass().getCanonicalName() + " In processMetaData End Time recordDetailBatchIndexes insert: " + LocalDateTime.now());
		}

		if (!recordDetailErrorBatchIndexes.isEmpty()) {
			logger.info(this.getClass().getCanonicalName() + " In processMetaData Start Time recordDetailErrorBatchIndexes insert: " + LocalDateTime.now());
			JdbcDatabaseUtil.insertRecordDetailErrorBatch(recordDetailErrorBatchIndexes, metaDataRecords, getSnapShotDate(), retrieveInsurerFileDetailErrorTable(getInsurer()));
			logger.info(this.getClass().getCanonicalName() + " In processMetaData End Time recordDetailErrorBatchIndexes insert: " + LocalDateTime.now());
		}


	}

	
	
	public void exportMasterTable(String insurer, String destination) throws SQLException, IOException {
//		JdbcDatabaseUtil.exportMasterTable(retrieveInsurerFileDetailTable(insurer));
		
	}
	public void exportErrorTable(String insurer2, String string) throws SQLException, IOException {
//		JdbcDatabaseUtil.exportErrorTable(retrieveInsurerFileDetailErrorTable(insurer));
	}
	
	private String retrieveInsurerFileDetailErrorTable(String insurer) {
		String errorTableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case "AVI":
				errorTableName = Constants.AVI_ERROR_FILE_DETAILS_TABLE;
				break;
			case "NTUC":
				errorTableName = Constants.NTUC_ERROR_FILE_DETAILS_TABLE;
				break;
			case "GE":
				errorTableName = Constants.GE_ERROR_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warning(this.getClass().getSimpleName() + " In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return errorTableName;
	}
	
	private String retrieveInsurerFileDetailTable(String insurer) {

		String tableName = "";
		
		if (insurer != null) {
			switch (insurer) {
			case "AVI":
				tableName = Constants.AVI_FILE_DETAILS_TABLE;
				break;
			case "NTUC":
				tableName = Constants.NTUC_FILE_DETAILS_TABLE;
				break;
			case "GE":
				tableName = Constants.GE_FILE_DETAILS_TABLE;
				break;
			default:
				logger.warning(this.getClass().getSimpleName() + " In retrieveInsurerTable: no valid insurer found.");
				break;
			}
		}
		return tableName;
	}

	/*

	public static Timestamp getCurrentTimeStamp(String dateFormat) throws ParseException {
		LocalDate now = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		String formattedDate = now.format(formatter);
		LocalDateTime localDateTime = LocalDateTime.parse(formattedDate, formatter);
		return Timestamp.valueOf(localDateTime);
	}
*/
	/*
	public String isValidMetaDataExtension(File file) throws IllegalArgumentException, IOException {
		StringBuilder stringBuilder = new StringBuilder();
		if (file != null) {
			if (!FilenameUtils.getExtension(file.getCanonicalPath()).equals("csv")) {
				stringBuilder.append("Please check that metadata file exist and ends with .csv extension");
			}
		}
		return stringBuilder.toString();
	}
	*/

	//TODO decom
	public String isMetaDataHeaderTrailerFormatValid(File metaDataFile) throws FileNotFoundException, IOException {

		List<String> records = MigrationUtil.readFile(metaDataFile.getAbsolutePath());

		StringBuilder stringBuilder = new StringBuilder();

		if (records.size() >= 2) { // Contains header and trailer
			// First record
			RecordHeader recordHeader = RecordMapperUtil
					.mapToRecordHeader(records.get(0).split(Constants.CSV_DELIMITER_FOR_SPLIT));

			// Last record
			RecordTrailer recordTrailer = RecordMapperUtil
					.mapToRecordTrailer(records.get(records.size() - 1).split(Constants.CSV_DELIMITER_FOR_SPLIT));

			if (recordHeader != null && recordTrailer != null) {

				Set<ConstraintViolation<RecordHeader>> recordHeaderViolations = validator.validate(recordHeader);
				Set<ConstraintViolation<RecordTrailer>> recordTrailerViolations = validator.validate(recordTrailer);
				

				if (!recordHeaderViolations.isEmpty()) {
					stringBuilder.append("Error in Record Header.");
					stringBuilder.append("\n");
					stringBuilder.append(retrieveErrorFromConstraintViolation(recordHeaderViolations));
				}

				if(!recordHeader.getInsurerTag().equals(this.getInsurer())) {
					if(stringBuilder.toString().isEmpty()) {
						stringBuilder.append("Error in Record Header.");
						stringBuilder.append("\n");
					}
					stringBuilder.append("Insurer parameter : " + this.getInsurer() + " does not match header insurer tag:  " + recordHeader.insurerTag);
					stringBuilder.append("\n");
				}

				if (!recordTrailerViolations.isEmpty()) {
					stringBuilder.append("Error in Record Trailer.");
					stringBuilder.append("\n");
					stringBuilder.append(retrieveErrorFromConstraintViolation(recordTrailerViolations));
				}

			} else {
				stringBuilder.append("Unable to generate Record Header or Trailer, please check it's format.");

			}
		}
		return stringBuilder.toString();
	}

	public String isMetaDataHeaderTrailerFormatValid(String filePath) throws FileNotFoundException, IOException {

		List<String> records = MigrationUtil.readFile(filePath);

		StringBuilder stringBuilder = new StringBuilder();

		if (records.size() >= 2) { // Contains header and trailer
			// First record
			RecordHeader recordHeader = RecordMapperUtil
					.mapToRecordHeader(records.get(0).split(Constants.CSV_DELIMITER_FOR_SPLIT));

			// Last record
			RecordTrailer recordTrailer = RecordMapperUtil
					.mapToRecordTrailer(records.get(records.size() - 1).split(Constants.CSV_DELIMITER_FOR_SPLIT));

			if (recordHeader != null && recordTrailer != null) {

				Set<ConstraintViolation<RecordHeader>> recordHeaderViolations = validator.validate(recordHeader);
				Set<ConstraintViolation<RecordTrailer>> recordTrailerViolations = validator.validate(recordTrailer);
				

				if (!recordHeaderViolations.isEmpty()) {
					stringBuilder.append("Error in Record Header.");
					stringBuilder.append("\n");
					stringBuilder.append(retrieveErrorFromConstraintViolation(recordHeaderViolations));
				}

				if(!recordHeader.getInsurerTag().equals(this.getInsurer())) {
					if(stringBuilder.toString().isEmpty()) {
						stringBuilder.append("Error in Record Header.");
						stringBuilder.append("\n");
					}
					stringBuilder.append("Insurer parameter : " + this.getInsurer() + " does not match header insurer tag:  " + recordHeader.insurerTag);
					stringBuilder.append("\n");
				}

				if (!recordTrailerViolations.isEmpty()) {
					stringBuilder.append("Error in Record Trailer.");
					stringBuilder.append("\n");
					stringBuilder.append(retrieveErrorFromConstraintViolation(recordTrailerViolations));
				}

			} else {
				stringBuilder.append("Unable to generate Record Header or Trailer, please check it's format.");

			}
		}
		return stringBuilder.toString();
	}

	public String isTrailerDetailCountCorrect(File metaDataFile) throws FileNotFoundException, IOException {

		List<String> records = MigrationUtil.readFile(metaDataFile.getAbsolutePath());

		StringBuilder stringBuilder = new StringBuilder();

		// Validate detail count using trailer record
		RecordTrailer recordTrailer = RecordMapperUtil
				.mapToRecordTrailer(records.get(records.size() - 1).split(Constants.CSV_DELIMITER_FOR_SPLIT));

		if (recordTrailer != null) {

			final int HEADER_LINE_AND_TRAILER_LINE_COUNT = 2;

			int detailCountFromReadingFile = records.size() - HEADER_LINE_AND_TRAILER_LINE_COUNT;

			int detailCountInRecordTrailer = Integer.parseInt(recordTrailer.getRecordCount());

			if (detailCountFromReadingFile != detailCountInRecordTrailer) {

				stringBuilder.append("Error in Detail Count provided. Total detail records found in meta file: "
						+ detailCountFromReadingFile);
				stringBuilder.append("\n");
				stringBuilder
						.append("Indicated Detail Count from meta data file is: " + recordTrailer.getRecordCount());
				stringBuilder.append("\n");
			}
		} else {
			stringBuilder.append(
					"Unable to deserialize record trailer, please check that the record trailer format is correct");
		}
		return stringBuilder.toString();
	}

	/**
	 * Assigns header info to the controller.
	 * 
	 * 
	 * @param metaDataFile
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void assignInsurerToAndSnapshotDateToController(File metaDataFile) throws FileNotFoundException, IOException {
		List<String> metaDataRecords = MigrationUtil.readFile(metaDataFile.getAbsolutePath());
		RecordHeader recordHeader = RecordMapperUtil
				.mapToRecordHeader(metaDataRecords.get(0).split(Constants.CSV_DELIMITER_FOR_SPLIT));

		this.setInsurer(recordHeader.getInsurerTag());
		this.setSnapShotDate(recordHeader.getSnapshotDate());
	}

	public static boolean isRecordDetailMd5Correct(RecordDetail recordDetail) {
		String algorithm = "MD5";

		String toCheck = recordDetail.getSystemId() + recordDetail.getFileName()
				+ recordDetail.getPolicyHolderReferenceId() + recordDetail.getPolicyReferenceId();

		String hash = "";

		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(toCheck.getBytes());

			byte[] digest = messageDigest.digest();

			hash = DatatypeConverter.printHexBinary(digest).toUpperCase();

		} catch (NoSuchAlgorithmException e) {
			System.out.println("Invalid Algorithm Selected : " + algorithm);
		}

		return recordDetail.getHash().contentEquals(hash);
	}

	/**
	 * Retrieves error from constraint violation. 
	 * @param <T>
	 * @param constraintViolation
	 * @return
	 */
	public static <T> String retrieveErrorFromConstraintViolation(Set<ConstraintViolation<T>> constraintViolation) {
		StringBuilder stringBuilder = new StringBuilder();


		constraintViolation.forEach(c -> {
			stringBuilder.append(c.getMessage());
			stringBuilder.append(Constants.CSV_DELIMITER);
		});

		String result = stringBuilder.toString();
		if(!result.isEmpty()) {
			return result.substring(0, result.length() - Constants.CSV_DELIMITER.length());
			
		}else {
			return "";
		}
	}

	/**
	 * Checks if a file exists
	 * 
	 * @param filePath
	 * @return boolean
	 */
	public static boolean doesFileExists(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}

	public String getInsurer() {
		return insurer;
	}

	public void setInsurer(String insurer) {
		this.insurer = insurer;
	}

	public String getSnapShotDate() {
		return snapShotDate;
	}

	public void setSnapShotDate(String snapShotDate) {
		this.snapShotDate = snapShotDate;
	}

	
}
