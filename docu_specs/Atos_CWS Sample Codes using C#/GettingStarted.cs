﻿using System;
using System.ServiceModel;

using GettingStarted.CWS;

namespace GettingStarted
{
    class Program
    {
        // The user's credentials
        const string USERNAME = "username";
        const string PASSWORD = "password";

        static void Main(string[] args)
        {
            // --------------------------------------------------------------------------
            // 1) Authenticate the user
            // --------------------------------------------------------------------------

            // Create the Authentication service client
            AuthenticationClient authClient = new AuthenticationClient();

            // Store the authentication token
            string authToken = null;

            // Call the AuthenticateUser() method to get an authentication token
            try
            {
                Console.Write("Authenticating User...");
                authToken = authClient.AuthenticateUser(USERNAME, PASSWORD);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                authClient.Close();
            }

            // --------------------------------------------------------------------------
            // 2) Get the user's favorites
            // --------------------------------------------------------------------------

            // Create the DocumentManagement service client
            DocumentManagementClient docManClient = new DocumentManagementClient();

            // Create the OTAuthentication object and set the authentication token
            OTAuthentication otAuth = new OTAuthentication();
            otAuth.AuthenticationToken = authToken;

            // Store the favorites
            Node[] favorites = null;

            // Call the GetAllFavorites() method to get the user's favorites
            try
            {
                Console.Write("Getting the user's favorites...");
                favorites = docManClient.GetAllFavorites(ref otAuth);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                docManClient.Close();
            }

            // Output the user's favorites
            Console.WriteLine("User's Favorites:\n");
            if (favorites != null)
            {
                foreach (Node node in favorites)
                {
                    Console.WriteLine(node.Name);
                }
            }
            else
            {
                Console.WriteLine("No Favorites.");
            }
            Console.WriteLine();

        }
    }
}
