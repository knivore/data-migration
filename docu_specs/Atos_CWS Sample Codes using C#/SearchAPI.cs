using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Net;
using System.Xml;

namespace SearchAPI
{
    class Program
    {
        static void Main(string[] args)
        {         
            //Create cookie container to store the llcookie.
            CookieContainer CC = new CookieContainer();

            //Create the HttpWebRequest object to log the user in.  Set the correct hostname and path for your environment in the URI object.
            HttpWebRequest loginRequest = (HttpWebRequest)HttpWebRequest.Create(new Uri("http://yourserver.com/otcs/livelink.exe"));

            //Set up the POST data.  User credentials can be adjusted as needed.
            var postData = "func=ll.login";
            postData += "&Username=admin";
            postData += "&Password=livelink";
            var loginPost = Encoding.ASCII.GetBytes(postData);

            //Set up the required request parameters for the login POST.
            loginRequest.ContentLength = loginPost.Length;
            loginRequest.ContentType = "application/x-www-form-urlencoded";
            loginRequest.Method = "POST";
            loginRequest.CookieContainer = CC;
            using (var stream = loginRequest.GetRequestStream())
            {
                stream.Write(loginPost, 0, loginPost.Length);
            }
            
            //Send the login POST.  This will deposit the required cookies in the cookie container.
            HttpWebResponse loginResponse = (HttpWebResponse)loginRequest.GetResponse();
            loginResponse.Close();


            //Create a new HttpWebRequest object to run the search.  Set the correct hostname and path for your environment in the URI object.
            HttpWebRequest searchRequest = (HttpWebRequest)WebRequest.Create(new Uri("http://yourserver.com/otcs/livelink.exe"));

            //Set up the request parameters for the search POST.  Adjust or add parameters as desired.
            var postData2 = "func=search";
            postData2 += "&where1=test";
            postData2 += "&outputformat=xml";
            var searchPost = Encoding.ASCII.GetBytes(postData2);

            //Set up the required request parameters for the search POST.
            searchRequest.ContentLength = searchPost.Length;
            searchRequest.ContentType = "application/x-www-form-urlencoded";
            searchRequest.Method = "POST";
            searchRequest.CookieContainer = CC;
            //Always set the referer to match the FQDN of the server or the request will be rejected.
            searchRequest.Referer = "http://yourserver.com";
            using (var stream = searchRequest.GetRequestStream())
            {
                stream.Write(searchPost, 0, searchPost.Length);
            }
            HttpWebResponse searchResponse = (HttpWebResponse)searchRequest.GetResponse();

            /*Search API can also be used via the GET verb, which may be a suitable solution and doesn't require setting up as many parameters.  That would look like this:
            HttpWebRequest searchRequest = (HttpWebRequest)WebRequest.Create(new Uri("http://yourserver.com/otcs/livelink.exe?func=search&where1=test&outputformat=xml"));
            searchRequest.ContentLength = 0;
            searchRequest.Method = "GET";
            searchRequest.CookieContainer = CC;
            HttpWebResponse searchResponse = (HttpWebResponse)searchRequest.GetResponse();
            */

            //Stream search results to string, ready to parse.
            Stream dataStream = searchResponse.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            
            //Output to console for sanity checking.
            Console.WriteLine(responseFromServer);
            
            //Close off remaining objects.
            searchResponse.Close();
            reader.Close();
            dataStream.Close();
        }
    }
} 