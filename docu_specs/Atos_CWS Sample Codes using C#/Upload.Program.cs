﻿using System;
using System.IO;
using System.ServiceModel;

using CWS;

namespace Upload
{
    class Program
    {
        // The user's credentials
        const string USERNAME = "username";
        const string PASSWORD = "password";

        // The local file path of the file to upload
        const string FILE_PATH = @"C:\temp\small.txt";

        // The ID of the parent container to add the document to
        const int PARENT_ID = 2000;

        static void Main(string[] args)
        {
            // --------------------------------------------------------------------------
            // 1) Authenticate the user
            // --------------------------------------------------------------------------

            // Create the Authentication service client
            AuthenticationClient authClient = new AuthenticationClient();

            // Store the authentication token
            string authToken = null;

            // Call the AuthenticateUser() method to get an authentication token
            try
            {
                Console.Write("Authenticating User...");
                authToken = authClient.AuthenticateUser(USERNAME, PASSWORD);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                authClient.Close();
            }

            // --------------------------------------------------------------------------
            // 2) Store the metadata for the upload in a method context on the server
            // --------------------------------------------------------------------------

            // Store the information for the local file
            FileInfo fileInfo = null;

            try
            {
                fileInfo = new FileInfo(FILE_PATH);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}\n", e.Message);
                return;
            }

            // Create the DocumentManagement service client
            DocumentManagementClient docManClient = new DocumentManagementClient();

            // Create the OTAuthentication object and set the authentication token
            OTAuthentication otAuth = new OTAuthentication();
            otAuth.AuthenticationToken = authToken;

            // Store the context ID for the upload
            string contextID = null;

            // Call the CreateDocumentContext() method to create the context ID
            try
            {
                Console.Write("Generating context ID...");
                contextID = docManClient.CreateDocumentContext(ref otAuth, PARENT_ID, fileInfo.Name, null, false, null);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                docManClient.Close();
            }

            // --------------------------------------------------------------------------
            // 3) Upload the file
            // --------------------------------------------------------------------------

            // Create a file stream to upload the file with
            FileStream fileStream = null;

            try
            {
                fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}\n", e.Message);
                return;
            }

            // Create the ContentService service client
            ContentServiceClient contentServiceClient = new ContentServiceClient();

            // Create the FileAtts object to send in the upload call
            FileAtts fileAtts = new FileAtts();
            fileAtts.CreatedDate = fileInfo.CreationTime;
            fileAtts.FileName = fileInfo.Name;
            fileAtts.FileSize = fileInfo.Length;
            fileAtts.ModifiedDate = fileInfo.LastWriteTime;

            // Call the UploadContent() method to upload the file
            try
            {
                Console.Write("Uploading file...");
                string objectID = contentServiceClient.UploadContent(ref otAuth, contextID, fileAtts, fileStream);
                Console.WriteLine("Success!");
                Console.WriteLine("New document uploaded with ID = {0}\n", objectID);
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the stream
                fileStream.Close();

                // Always close the client
                contentServiceClient.Close();
            }

            return;
        }
    }
}