﻿using OTDSAuthentication.CWS;
using System;
using System.ServiceModel;

namespace OTDSAuthentication
{
    class Program
    {
        // The user's credentials
        const string USERNAME = "username@OTCS";
        const string PASSWORD = "password";

        static void Main(string[] args)
        {
            // --------------------------------------------------------------------------
            // 1) Authenticate the user with OTDS
            // --------------------------------------------------------------------------

            // Create the OTDS Authentication service client
            // Note: Since we have two "Authentication" services, the generated client proxies
            // appended a "1" to the name of the OTDS Authentication service client.
            Authentication1Client otdsAuthClient = new Authentication1Client();
            OTAuthentication otAuth = new OTAuthentication();

            // Store the OTDS authentication token
            string otdsToken = null;

            // Call the Authenticate() method to get an authentication token
            try
            {
                Console.Write("Authenticating User With OTDS...");
                otdsToken = otdsAuthClient.Authenticate(ref otAuth, USERNAME, PASSWORD);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                otdsAuthClient.Close();
            }

            // --------------------------------------------------------------------------
            // 2) Validate the OTDS token with Content Server to get a Content Server authentication token
            // --------------------------------------------------------------------------

            // Create the CWS Authentication service client
            AuthenticationClient cwsAuthClient = new AuthenticationClient();

            // Store the authentication token
            string cwsToken = null;

            // Call the ValidateUser() method
            try
            {
                Console.Write("Validating OTDS authentication token with Content Server...");
                cwsToken = cwsAuthClient.ValidateUser(otdsToken);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                cwsAuthClient.Close();
            }

            // --------------------------------------------------------------------------
            // 3) Get the user's favorites
            // --------------------------------------------------------------------------

            // Create the DocumentManagement service client
            DocumentManagementClient docManClient = new DocumentManagementClient();

            // Set the authentication token on the OTAuthentication object
            otAuth.AuthenticationToken = cwsToken;

            // Store the favorites
            Node[] favorites = null;

            // Call the GetAllFavorites() method to get the user's favorites
            try
            {
                Console.Write("Getting the user's favorites...");
                favorites = docManClient.GetAllFavorites(ref otAuth);
                favorites = docManClient.GetAllFavorites(ref otAuth);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                docManClient.Close();
            }

            // Output the user's favorites
            Console.WriteLine("User's Favorites:\n");
            if (favorites != null)
            {
                foreach (Node node in favorites)
                {
                    Console.WriteLine(node.Name);
                }
            }
            else
            {
                Console.WriteLine("No Favorites.");
            }
            Console.WriteLine();

        }
    }
}
