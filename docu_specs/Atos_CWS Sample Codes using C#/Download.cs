﻿using System;
using System.IO;
using System.ServiceModel;

using Download.CWS;

namespace Download
{
    class Program
    {
        // The user's credentials
        const string USERNAME = "username";
        const string PASSWORD = "password";

        // The ID of the document to download
        const int DOCUMENT_ID = 5074;

        // The local file path to download the document to
        const string FILE_PATH = @"C:\temp\test.pdf";

        // The file download buffer size (4 KB)
        const int BUFFER_SIZE = 4096;

        static void Main(string[] args)
        {
            // --------------------------------------------------------------------------
            // 1) Authenticate the user
            // --------------------------------------------------------------------------

            // Create the Authentication service client
            AuthenticationClient authClient = new AuthenticationClient();

            // Store the authentication token
            string authToken = null;

            // Call the AuthenticateUser() method to get an authentication token
            try
            {
                Console.Write("Authenticating User...");
                authToken = authClient.AuthenticateUser(USERNAME, PASSWORD);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                authClient.Close();
            }

            // --------------------------------------------------------------------------
            // 2) Store the metadata for the download in a method context on the server
            // --------------------------------------------------------------------------

            // Create the DocumentManagement service client
            DocumentManagementClient docManClient = new DocumentManagementClient();

            // Create the OTAuthentication object and set the authentication token
            OTAuthentication otAuth = new OTAuthentication();
            otAuth.AuthenticationToken = authToken;

            // Store the context ID for the download
            string contextID = null;

            // Call the GetVersionContentsContext() method to create the context ID
            try
            {
                Console.Write("Generating context ID...");
                contextID = docManClient.GetVersionContentsContext(ref otAuth, DOCUMENT_ID, 0);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                docManClient.Close();
            }

            // --------------------------------------------------------------------------
            // 3) Download the file
            // --------------------------------------------------------------------------

            // Create the ContentService service client
            ContentServiceClient contentServiceClient = new ContentServiceClient();

            // Create a stream to download the file with
            Stream downloadStream = null;

            // Call the DownloadContent() method to get the download stream 
            try
            {
                Console.Write("Downloading file...");
                downloadStream = contentServiceClient.DownloadContent(ref otAuth, contextID);
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                contentServiceClient.Close();
            }

            // Create a file stream to write the contents to the local file
            FileStream fileStream = null;

            try
            {
                fileStream = new FileStream(FILE_PATH, FileMode.Create);

                // Create a buffer to read the download stream into
                byte[] buffer = new byte[BUFFER_SIZE];

                // Keep track of the size of the file
                long fileSize = 0;

                // Read the contents from download stream and write them to the file stream
                for (int read = downloadStream.Read(buffer, 0, buffer.Length); read > 0; read = downloadStream.Read(buffer, 0, buffer.Length))
                {
                    fileStream.Write(buffer, 0, read);
                    fileSize += read;
                }

                Console.WriteLine("Success!\n");
                Console.WriteLine("Downloaded {0} bytes to {1}.\n", fileSize, FILE_PATH);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0}\n", e.Message);
                return;
            }
            finally
            {
                // Always close the streams
                if (fileStream != null)
                {
                    fileStream.Close();
                }
                downloadStream.Close();
            }

            return;
        }
    }
}