﻿using System;
using System.IO;
using System.ServiceModel;

using CWS;

namespace CatsAndAtts
{
    class Program
    {
        // The user's credentials
        const string USERNAME = "username";
        const string PASSWORD = "password";

        // The document to upload
        const string FILE_PATH = @"C:\temp\TheMatrix.jpg";

        // The parent folder ID to create new nodes in
        const int PARENT_ID = 2000;

        static void Main(string[] args)
        {
            // --------------------------------------------------------------------------
            // 1) Authenticate the user
            // --------------------------------------------------------------------------

            // Create the Authentication service client
            AuthenticationClient authClient = new AuthenticationClient();

            // Store the authentication token
            string authToken = null;

            // Call the AuthenticateUser() method to get an authentication token
            try
            {
                Console.Write("Authenticating User...");
                authToken = authClient.AuthenticateUser(USERNAME, PASSWORD);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                return;
            }
            finally
            {
                // Always close the client
                authClient.Close();
            }

            // --------------------------------------------------------------------------
            // 2) Create a category
            //
            //    The category will have the following attributes:
            //    - Directed By (Text Field)
            //    - Starring (Text Field)
            //    - Release Date (Date Field)
            //    - Running Time (Integer Field)
            // --------------------------------------------------------------------------

            // Create the DocumentManagement service client
            DocumentManagementClient docManClient = new DocumentManagementClient();

            // Create the OTAuthentication object and set the authentication token
            OTAuthentication otAuth = new OTAuthentication();
            otAuth.AuthenticationToken = authToken;

            // Directed By
            StringAttribute directedBy = new StringAttribute();
            directedBy.DisplayLength = 64;
            directedBy.DisplayName = "Directed By";
            directedBy.MaxLength = 64;
            directedBy.MaxValues = 5;
            directedBy.MinValues = 1;
            directedBy.ReadOnly = new BooleanObject();
            directedBy.ReadOnly.Value = false;
            directedBy.Required = false;
            directedBy.Searchable = true;

            // Starring
            StringAttribute starring = new StringAttribute();
            starring.DisplayLength = 64;
            starring.DisplayName = "Starring";
            starring.MaxLength = 64;
            starring.MaxValues = 20;
            starring.MinValues = 1;
            starring.ReadOnly = new BooleanObject();
            starring.ReadOnly.Value = false;
            starring.Required = false;
            starring.Searchable = true;

            // Release Date
            DateAttribute releaseDate = new DateAttribute();
            releaseDate.DisplayName = "Release Date";
            releaseDate.MaxValues = 1;
            releaseDate.MinValues = 1;
            releaseDate.ReadOnly = new BooleanObject();
            releaseDate.ReadOnly.Value = false;
            releaseDate.Required = false;
            releaseDate.Searchable = true;
            releaseDate.ShowTime = false;

            // Running Time
            IntegerAttribute runningTime = new IntegerAttribute();
            runningTime.DisplayName = "Running Time (minutes)";
            runningTime.MaxValues = 1;
            runningTime.MinValues = 1;
            runningTime.ReadOnly = new BooleanObject();
            runningTime.ReadOnly.Value = false;
            runningTime.Required = false;
            runningTime.Searchable = true;

            CWS.Attribute[] attributes = new CWS.Attribute[] { directedBy, starring, releaseDate, runningTime };

            Node movieInfo = null;
            try
            {
                Console.Write("Creating Category...");
                movieInfo = docManClient.CreateCategory(ref otAuth, PARENT_ID, "MovieInfo", null, attributes, null);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                docManClient.Close();
                return;
            }

            // --------------------------------------------------------------------------
            // 3) Get the category template
            // --------------------------------------------------------------------------

            AttributeGroup categoryTemplate = null;
            try
            {
                Console.Write("Getting Movie Info Category Template...");
                categoryTemplate = docManClient.GetCategoryTemplate(ref otAuth, movieInfo.ID);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
                docManClient.Close();
                return;
            }

            // --------------------------------------------------------------------------
            // 4) Create a document with the category
            // --------------------------------------------------------------------------
            
            FileInfo fileInfo = new FileInfo(FILE_PATH);
            byte[] contents = File.ReadAllBytes(FILE_PATH);

            Attachment attachment = new Attachment();
            attachment.Contents = contents;
            attachment.CreatedDate = fileInfo.CreationTime;
            attachment.FileName = fileInfo.Name;
            attachment.FileSize = contents.Length;
            attachment.ModifiedDate = fileInfo.LastWriteTime;

            StringValue directedByValue = categoryTemplate.Values[0] as StringValue;
            directedByValue.Values = new string[] { "Andy Wachowski", "Larry Wachowski" };

            StringValue starringValue = categoryTemplate.Values[1] as StringValue;
            starringValue.Values = new string[] { "Keanu Reeves", "Laurence Fishburne", "Carrie-Anne Moss", "Hugo Weaving", "Joe Pantoliano" };

            DateValue releaseDateValue = categoryTemplate.Values[2] as DateValue;
            releaseDateValue.Values = new DateTime?[] { new DateTime(1999, 3, 31) };

            IntegerValue runningTimeValue = categoryTemplate.Values[3] as IntegerValue;
            runningTimeValue.Values = new long?[] { 136 };

            Metadata metadata = new Metadata();
            metadata.AttributeGroups = new AttributeGroup[] { categoryTemplate };

            Node theMatrix = null;
            try
            {
                Console.Write("Creating Document With Category...");
                theMatrix = docManClient.CreateDocument(ref otAuth, PARENT_ID, "The Matrix", null, false, metadata, attachment);
                Console.WriteLine("Success!\n");
            }
            catch (FaultException e)
            {
                Console.WriteLine("Failed!");
                Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
            }
            finally
            {
                docManClient.Close();
            }
        }
    }
}
