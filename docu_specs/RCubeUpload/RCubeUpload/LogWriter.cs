﻿using System;
using System.IO;

namespace RCubeUpload
{
    public class LogWriter
    {
        private string fullPath = null;
        private string filePath = null;
        private string orgFileName, rollingFileName = null;

        int counter = 1, fileMaxSize = 0;

        public LogWriter(string logPath, string logName, int maxSize)
        {
            string today = DateTime.Now.ToString("ddMMyyyy");
            string[] split = logName.Split('.');
            if (split.Length == 2)
            {
                filePath = logPath;
                fileMaxSize = maxSize;
                orgFileName = split[0] + "_" + today + "." + split[1];
                fullPath = Path.Combine(filePath, orgFileName);
                string directoryName = Path.GetDirectoryName(fullPath);
                Directory.CreateDirectory(directoryName);
            }
            else
            {
                Console.WriteLine("Invalid file path:" + logPath);
            }
        }

        private void checkRollFile()
        {
            while (true)
            {
                if (!File.Exists(fullPath))
                {
                    break;
                }
                var fileSize = Convert.ToDecimal((new FileInfo(fullPath).Length));
                if (fileSize > fileMaxSize)
                {
                    string today = DateTime.Now.ToString("ddMMyyyy");
                    string[] split = orgFileName.Split('.');
                    rollingFileName = split[0] + "_" + counter + "." + split[1];
                    fullPath = Path.Combine(filePath, rollingFileName);
                    counter++;
                    continue;
                }
                else
                {
                    break;
                }
            }
        }

        public void log(string message)
        {
            checkRollFile();
            string output = "[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + message + Environment.NewLine;
            File.AppendAllText(fullPath, output);
            Console.Write(output);
        }

        public void log(Exception e)
        {
            checkRollFile();
            string output = "[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine;
            File.AppendAllText(fullPath, output);
            Console.Write(output);
        }
    }
}
