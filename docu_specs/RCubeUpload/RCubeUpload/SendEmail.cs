﻿using System;
using System.Net;
using System.Net.Mail;

namespace RCubeUpload
{
    class SendEmail
    {
        SmtpClient smtp = null;
        MailMessage message = null;

        public SendEmail(string fromEmail, string password, string[] toEmails, string smtpServer, int smtpPort)
        {
            try
            {
                smtp = new SmtpClient();
                smtp.Host = smtpServer;
                smtp.Port = smtpPort;
                smtp.EnableSsl = false;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(fromEmail, password);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                message = new MailMessage();
                message.From = new MailAddress(fromEmail);
                foreach (string toEmail in toEmails)
                {
                    message.To.Add(new MailAddress(toEmail.Trim()));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void sendMessage(string subject, string body, bool isHtmlBody)
        {
            try
            {
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = isHtmlBody;
                smtp.Send(message);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
