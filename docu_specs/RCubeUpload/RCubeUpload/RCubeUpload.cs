﻿using Microsoft.SharePoint;
using RCubeUpload.CWS;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Xml;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using Microsoft.Office.Server.Utilities;
using System.Threading;

namespace RCubeUpload
{
    class RCubeUpload
    {
        static string rcubeContentServiceURL = ConfigurationManager.AppSettings["rcubeContentServiceURL"];
        static string sharePointSiteURL = ConfigurationManager.AppSettings["sharePoint.siteURL"];

        static string configurationSiteURL = ConfigurationManager.AppSettings["configurationSiteURL"];
        static string configurationListTitle = ConfigurationManager.AppSettings["configurationListTitle"];
        static string listHeaderTitle = ConfigurationManager.AppSettings["list.header.title"];
        static string listHeaderValue = ConfigurationManager.AppSettings["list.header.value"];
        static string listTitleUsername = ConfigurationManager.AppSettings["list.title.username"];
        static string listTitlePassword = ConfigurationManager.AppSettings["list.title.password"];
        static string listTitleStopRun = ConfigurationManager.AppSettings["list.title.stopRun"];

        static string sharePointJobAssignStr = ConfigurationManager.AppSettings["sharePoint.jobAssign"];
        static string sharePointJobName = ConfigurationManager.AppSettings["sharePoint.jobName"];
        static string sharePointDocListTitle = ConfigurationManager.AppSettings["sharePoint.documentList.title"];
        static string sharePointSubFolderTitle = ConfigurationManager.AppSettings["sharePoint.subFolder.title"];

        static string sharePointPzInsKey = ConfigurationManager.AppSettings["sharePoint.meta.pzInsKey"];
        static string sharePointCaseRefNo = ConfigurationManager.AppSettings["sharePoint.meta.caseRefNo"];
        static string sharePointOffloadStatus = ConfigurationManager.AppSettings["sharePoint.meta.offloadStatus"];
        static string sharePointRCubeDocID = ConfigurationManager.AppSettings["sharePoint.meta.rcubeDocId"];


        static string updateOffloadStatus = ConfigurationManager.AppSettings["sharePoint.update.offloadStatus"];
        static string updateOffloadStatusError = ConfigurationManager.AppSettings["sharePoint.update.offloadStatusError"];

        static string sharePointQueryArchiveDate = ConfigurationManager.AppSettings["sharePoint.query.meta.archiveDate"];
        static string sharePointQueryOffloadStatus = ConfigurationManager.AppSettings["sharePoint.query.meta.offloadStatus"];
        static string sharePointQueryOffloadStatusValue = ConfigurationManager.AppSettings["sharePoint.query.meta.offloadStatus.value"];
        static string sharePointQuerypzInsKey = ConfigurationManager.AppSettings["sharePoint.query.meta.pzInsKey"];
        static string sharePointQueryCaseRefNo = ConfigurationManager.AppSettings["sharePoint.query.meta.caseRefNo"];
        static string sharePointQueryJobAssign = ConfigurationManager.AppSettings["sharePoint.query.meta.jobAssign"];
        static uint sharePointQueryMaxRows = uint.Parse(ConfigurationManager.AppSettings["sharePoint.query.max.rows"]);

        static string rcubeCategoryNICE = ConfigurationManager.AppSettings["rcube.category.nice"];
        static string rcubeNICEPzInsKey = ConfigurationManager.AppSettings["rcube.meta.nice.pzInsKey"];
        static string rcubeNICECaseRefNo = ConfigurationManager.AppSettings["rcube.meta.nice.caseRefNo"];

        static string rcubeCategoryRecord = ConfigurationManager.AppSettings["rcube.category.record"];
        static string rcubeRecordRecordType = ConfigurationManager.AppSettings["rcube.meta.record.recordType"];
        static string rcubeRecordRecordTypeValue = ConfigurationManager.AppSettings["rcube.meta.record.recordType.value"];

        static string fourthLevel = ConfigurationManager.AppSettings["rcube.folder.name"];
        static int rcubeBaseFolderId = int.Parse(ConfigurationManager.AppSettings["rcube.base.folder.id"]);
        static string rcubeBaseFolderNICE = ConfigurationManager.AppSettings["rcube.base.folder.nice"];

        static string logFilePath = ConfigurationManager.AppSettings["log.file.path"];
        static string logFileName = ConfigurationManager.AppSettings["log.file.name"];
        static int logFileMaxSize = int.Parse(ConfigurationManager.AppSettings["log.file.maxSize"]);

        static string emailSmtpHost = ConfigurationManager.AppSettings["email.smtpHost"];
        static int emailSmtpPort = int.Parse(ConfigurationManager.AppSettings["email.smtpPort"]);
        static string emailSubject = ConfigurationManager.AppSettings["email.subject"];
        static string emailUsername = ConfigurationManager.AppSettings["email.username"];
        static string emailPassword = ConfigurationManager.AppSettings["email.password"];
        static string[] emailRecipients = ConfigurationManager.AppSettings["email.recipients"].Split(',');

        static bool runIncludeFailed = bool.Parse(ConfigurationManager.AppSettings["run.includeFailed"]);
        static int sleepSeconds = int.Parse(ConfigurationManager.AppSettings["sleep.seconds"]);

        static void Main(string[] args)
        {
            LogWriter logWriter = new LogWriter(logFilePath, logFileName, logFileMaxSize);

            int sharePointJobAssign = 0;

            if (!sharePointJobAssignStr.Trim().Equals(String.Empty))
            {
                sharePointJobAssign = int.Parse(sharePointJobAssignStr);
            }

            int totalCount = 0, totalSuccess = 0, totalError = 0;
            int sleepTime = sleepSeconds * 1000;

            try
            {
                SPUserToken userToken = null;
                using (SPSite site = new SPSite(sharePointSiteURL))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPUser user = web.AllUsers["i:0#.w|" + System.Security.Principal.WindowsIdentity.GetCurrent().Name];
                        userToken = user.UserToken;
                    }
                }

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(sharePointSiteURL, userToken))
                    {
                        using (SPSite configSite = new SPSite(configurationSiteURL, userToken))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                using (SPWeb configWeb = configSite.OpenWeb())
                                {
                                    logWriter.log("Begin SharePoint Upload Job.");
                                    
                                    bool stopRun = false;
                                    //logWriter.log("Checking Stop Run Flag.");
                                    SPList configList = configWeb.Lists[configurationListTitle];

                                    var configListItems = configList.GetItems();

                                    foreach (SPListItem listItem in configListItems)
                                    {
                                        if (listItem[listHeaderTitle].ToString().Equals(listTitleStopRun))
                                        {
                                            stopRun = bool.Parse(listItem[listHeaderValue].ToString());
                                        }
                                    }

                                    if (stopRun)
                                    {
                                        logWriter.log("Stop Run flag enabled. Exiting.");
                                        Environment.Exit(0);
                                    }

                                    logWriter.log("Retrieving username and password from SharePoint.");

                                    // retrieve username and password for authentication
                                    string username = null, password = null;

                                    foreach (SPListItem listItem in configListItems)
                                    {
                                        if (listItem[listHeaderTitle].ToString().Equals(listTitleUsername))
                                        {
                                            username = listItem[listHeaderValue].ToString();
                                        }

                                        if (listItem[listHeaderTitle].ToString().Equals(listTitlePassword))
                                        {
                                            password = listItem[listHeaderValue].ToString();
                                        }
                                    }

                                    if (username != null && password != null)
                                    {
                                        string decrypt_USERNAME = DecryptData(username);
                                        string decrypt_PASSWORD = DecryptData(password);
                                        
                                        logWriter.log("Authenticating to RCube server.");

                                        // authenticate user to RCube
                                        OTAuthentication otAuth = AuthenticateUser(decrypt_USERNAME, decrypt_PASSWORD);

                                        logWriter.log("Authentication to RCube - Successful.");

                                        // retrieve documents from SharePoint
                                        SPList attachmentList = web.Lists[sharePointDocListTitle];
                                        SPQuery attachmentQuery = new SPQuery();

                                        string offloadStatusQuery = "", filterQuery = "";
                                        string statusFilter = "";
                                        if (sharePointQueryOffloadStatus == null || sharePointQueryOffloadStatusValue.Trim().Equals(string.Empty))
                                        {
                                            statusFilter = "<IsNull><FieldRef Name = '" + sharePointQueryOffloadStatus + "' /></IsNull>";
                                        }
                                        else
                                        {
                                            statusFilter = "<Eq><FieldRef Name='" + sharePointQueryOffloadStatus + "' /><Value Type='Text'>" + sharePointQueryOffloadStatusValue + "</Value></Eq>";
                                        }

                                        if (runIncludeFailed)
                                        {
                                            offloadStatusQuery = "<Or>" +
                                                                 statusFilter +
                                                                 "<Eq><FieldRef Name = '" + sharePointQueryOffloadStatus + "' /><Value Type='Text'>" + updateOffloadStatusError + "</Value></Eq>" +
                                                                 "</Or>";
                                        }
                                        else
                                        {
                                            offloadStatusQuery = statusFilter;
                                        }

                                        if (sharePointJobAssign > 0)
                                        {
                                            filterQuery = "<And>" +
                                                        "<Lt><FieldRef Name='" + sharePointQueryArchiveDate + "' /><Value Type='DateTime'>{0}</Value></Lt>" +
                                                        "<Eq><FieldRef Name='" + sharePointQueryJobAssign + "' /><Value Type='Integer'>" + sharePointJobAssign + "</Value></Eq>" +
                                                        "</And>";
                                        }
                                        else
                                        {
                                            filterQuery = "<Lt><FieldRef Name='" + sharePointQueryArchiveDate + "' /><Value Type='DateTime'>{0}</Value></Lt>";
                                        }

                                        attachmentQuery.Query = String.Format(
                                            "<Where>" +
                                                "<And>" +
                                                    filterQuery +
                                                    offloadStatusQuery +
                                                "</And>" +
                                            "</Where>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ"));
                                        attachmentQuery.ViewAttributes = "Scope=Recursive";
                                        attachmentQuery.ViewFields = "<FieldRef Name='EncodedAbsUrl' /><FieldRef Name='" + sharePointQuerypzInsKey + "' /><FieldRef Name='" + sharePointQueryCaseRefNo + "' /><FieldRef Name='" + sharePointQueryOffloadStatus + "' />";
                                        attachmentQuery.RowLimit = sharePointQueryMaxRows;
                                        attachmentQuery.QueryThrottleMode = SPQueryThrottleOption.Override;
                                        attachmentQuery.Query = attachmentQuery.Query + ContentIterator.ItemEnumerationOrderByNVPField;
                                        
                                        if (sharePointSubFolderTitle != null && sharePointSubFolderTitle.Trim().Length > 0)
                                        {
                                            attachmentQuery.Folder = attachmentList.RootFolder.SubFolders[sharePointSubFolderTitle];
                                        }

                                        if (site.WebApplication.Farm.CurrentUserIsAdministrator())
                                        {
                                            attachmentList.EnableThrottling = false;
                                        }
                                        logWriter.log("List throttling is: " + attachmentList.EnableThrottling);
                                        ContentIterator iterator = new ContentIterator();
                                        iterator.ProcessListItems(attachmentList, attachmentQuery,
                                            delegate (SPListItemCollection items)
                                            {
                                                //logWriter.log("Checking Stop Run Flag.");
                                                configListItems = configList.GetItems();
                                                foreach (SPListItem listItem in configListItems)
                                                {
                                                    if (listItem[listHeaderTitle].ToString().Equals(listTitleStopRun))
                                                    {
                                                        stopRun = bool.Parse(listItem[listHeaderValue].ToString());
                                                    }
                                                }

                                                if (stopRun)
                                                {
                                                    logWriter.log("Stop Run flag enabled. Exiting.");
                                                    iterator.Cancel = true;
                                                }
                                                else
                                                {
                                                    int successCount = 0, errorCount = 0, attachmentCount = items.Count;
                                                    logWriter.log("Processing " + attachmentCount + " attachments.");
                                                    if (items != null && attachmentCount > 0)
                                                    {
                                                        using (DocumentManagementClient docManClient = new DocumentManagementClient())
                                                        {
                                                            // get metadata category ids
                                                            //logWriter.log("Getting Category ID.");
                                                            Dictionary<string, long> categoryIDMap = GetCategoryIDMap(decrypt_USERNAME, decrypt_PASSWORD);
                                                            var categoryNICE = docManClient.GetCategoryTemplate(ref otAuth, categoryIDMap[rcubeCategoryNICE]);
                                                            var categoryRecord = docManClient.GetCategoryTemplate(ref otAuth, categoryIDMap[rcubeCategoryRecord]);
                                                            //logWriter.log("Completed getting Category ID.");

                                                            foreach (SPListItem listItem in items)
                                                            {
                                                                string fifthLevel = "", sixthLevel = "", pzInsKey = "", caseRefNo = "";
                                                                long uploadFolderId = 0;

                                                                try
                                                                {
                                                                    string url = listItem["EncodedAbsUrl"].ToString();
                                                                    pzInsKey = listItem[sharePointPzInsKey].ToString();
                                                                    caseRefNo = listItem[sharePointCaseRefNo].ToString();

                                                                    byte[] fileStream = null;

                                                                    //logWriter.log("Retrieving " + listItem.Name +" from SP..");
                                                                    using (WebClient webClient = new WebClient())
                                                                    {
                                                                        webClient.Credentials = CredentialCache.DefaultCredentials;
                                                                        fileStream = webClient.DownloadData(url);
                                                                    }
                                                                    //logWriter.log("File Retrieved..");

                                                                    FileAtts fileAtts = new FileAtts();
                                                                    fileAtts.FileName = listItem.Name;
                                                                    fileAtts.CreatedDate = listItem.File.TimeCreated;
                                                                    fileAtts.ModifiedDate = listItem.File.TimeLastModified;
                                                                    fileAtts.FileSize = fileStream.Length;

                                                                    string parentFolderStr = listItem.File.ParentFolder.ToString();
                                                                    string[] folders = parentFolderStr.Split('/');

                                                                    fifthLevel = folders[folders.Length - 2];
                                                                    sixthLevel = folders[folders.Length - 1];

                                                                    // prepare RCube metadata (NICE category)
                                                                    foreach (StringValue value in categoryNICE.Values)
                                                                    {
                                                                        if (value.Description.Equals(rcubeNICEPzInsKey))
                                                                        {
                                                                            value.Values = new string[] { pzInsKey };
                                                                            continue;
                                                                        }
                                                                        if (value.Description.Equals(rcubeNICECaseRefNo))
                                                                        {
                                                                            value.Values = new string[] { caseRefNo };
                                                                            continue;
                                                                        }
                                                                    }

                                                                    // prepare RCube metadata (Record category)
                                                                    foreach (StringValue value in categoryRecord.Values)
                                                                    {
                                                                        if (value.Description.Equals(rcubeRecordRecordType))
                                                                        {
                                                                            value.Values = new string[] { rcubeRecordRecordTypeValue };
                                                                        }
                                                                    }

                                                                    //logWriter.log("Getting upload folder id");
                                                                    uploadFolderId = GetUploadFolderId(otAuth, docManClient, fourthLevel, fifthLevel, sixthLevel, logWriter);
                                                                    // logWriter.log("Upload folder id retrieved. Uploading File.");
                                                                    long documentID = UploadFile(otAuth, docManClient, fileAtts, fileStream, uploadFolderId);
                                                                    //logWriter.log("Upload file completed. Updating metadata to RCube");

                                                                    // update RCube metadata
                                                                    Metadata metadata = new Metadata();
                                                                    metadata.AttributeGroups = new AttributeGroup[] { categoryNICE, categoryRecord };
                                                                    docManClient.SetNodeMetadata(ref otAuth, documentID, metadata);
                                                                    //logWriter.log("RCube metadata updated.");

                                                                    // update SharePoint metadata
                                                                    listItem[sharePointRCubeDocID] = documentID;
                                                                    listItem[sharePointOffloadStatus] = updateOffloadStatus;
                                                                    listItem.Update();
                                                                    //logWriter.log("Updated SharePoint metadata");
                                                                    successCount++;
                                                                    totalSuccess++;
                                                                    totalCount++;
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    if (e.Message.Contains(listItem.Name) && e.Message.Contains("already exists"))
                                                                    {
                                                                        string errorFolder = listItem.File.ParentFolder.ToString();
                                                                        string errorFile = listItem.Name;

                                                                        try
                                                                        {
                                                                            logWriter.log("Folder: " + errorFolder + " | File: " + errorFile + " already exists. Attempting to update file metadata for RCube and SharePoint...");
                                                                            var options = new GetNodesInContainerOptions();
                                                                            options.MaxDepth = 1;
                                                                            options.MaxResults = int.MaxValue;
                                                                            bool successfulUpdate = false;

                                                                            // get file in current folder
                                                                            var nodes = docManClient.GetNodesInContainer(ref otAuth, uploadFolderId, options);
                                                                            // loop through to find corresponding existing file in RCube
                                                                            foreach (var node in nodes)
                                                                            {
                                                                                if (node.Name.Equals(listItem.Name))
                                                                                {
                                                                                    // update RCube metadata
                                                                                    Metadata metadata = new Metadata();
                                                                                    metadata.AttributeGroups = new AttributeGroup[] { categoryNICE, categoryRecord };
                                                                                    docManClient.SetNodeMetadata(ref otAuth, node.ID, metadata);

                                                                                    logWriter.log("Updated " + errorFile + " - RCube pzInsKey: " + pzInsKey);
                                                                                    logWriter.log("Updated " + errorFile + " - RCube Case Ref No: " + caseRefNo);
                                                                                    logWriter.log("Updated " + errorFile + " - RCube Record Type: " + rcubeRecordRecordTypeValue);

                                                                                    // update SharePoint metadata
                                                                                    listItem[sharePointRCubeDocID] = node.ID;
                                                                                    listItem[sharePointOffloadStatus] = updateOffloadStatus;
                                                                                    listItem.Update();

                                                                                    logWriter.log("Updated " + errorFile + " - SharePoint RCube Doc ID: " + node.ID);
                                                                                    logWriter.log("Updated " + errorFile + " - SharePoint Offload Status: " + updateOffloadStatus);

                                                                                    successfulUpdate = true;
                                                                                    break;
                                                                                }
                                                                            }
                                                                            if (successfulUpdate)
                                                                            {
                                                                                successCount++;
                                                                                totalSuccess++;
                                                                                totalCount++;
                                                                            }
                                                                            else
                                                                            {
                                                                                logWriter.log("Error. Could not find document in folder. Failed to update - Folder: " + errorFolder + " | File: " + errorFile);
                                                                                errorCount++;
                                                                                totalError++;
                                                                                totalCount++;
                                                                                listItem[sharePointOffloadStatus] = updateOffloadStatusError;
                                                                                listItem.Update();
                                                                            }
                                                                        }
                                                                        catch (Exception f)
                                                                        {
                                                                            logWriter.log("Error - Folder: " + errorFolder + " | File: " + errorFile);
                                                                            logWriter.log(f);
                                                                            errorCount++;
                                                                            totalError++;
                                                                            totalCount++;
                                                                            listItem[sharePointOffloadStatus] = updateOffloadStatusError;
                                                                            listItem.Update();
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        logWriter.log("Error - Folder: " + listItem.File.ParentFolder.ToString() + " | File: " + listItem.Name);
                                                                        logWriter.log(e);
                                                                        errorCount++;
                                                                        totalError++;
                                                                        totalCount++;
                                                                        listItem[sharePointOffloadStatus] = updateOffloadStatusError;
                                                                        listItem.Update();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        logWriter.log("Completed current batch with - Total files: " + attachmentCount + " | Success: " + successCount + " | Errors: " + errorCount);
                                                        if (sleepTime > 0)
                                                        {
                                                            Thread.Sleep(sleepTime);
                                                        }

                                                        // reauthenticate user to refresh session
                                                        otAuth = AuthenticateUser(decrypt_USERNAME, decrypt_PASSWORD);
                                                    }
                                                }
                                            },
                                            delegate (SPListItemCollection items, Exception e)
                                            {
                                                return true;
                                            });
                                    }
                                    else
                                    {
                                        logWriter.log("Unable to get username and password from SharePoint. Exiting job.");
                                    }
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                logWriter.log(e);
            }
            logWriter.log("Completed run with - Total files: " + totalCount + " | Success: " + totalSuccess + " | Errors: " + totalError);
            if (totalCount > 0)
            {
                sendEmail(totalCount, totalSuccess, totalError);
                logWriter.log("Email report sent.");
            }
        }

        private static void sendEmail(int totalCount, int successCount, int errorCount)
        {
            SendEmail email = new SendEmail(emailUsername, emailPassword, emailRecipients, emailSmtpHost, emailSmtpPort);

            string body = String.Format("<table style = \"border:1px black solid; border-collapse:collapse; text-align:center;\">" +
                "<tr>" +
                "<td style = \"border:1px black solid; padding:5px; font-weight:bold;\">SharePoint Job Name</td>" +
                "<td style = \"border:1px black solid; padding:5px; font-weight:bold;\">Total Records</td>" +
                "<td style = \"border:1px black solid; padding:5px; font-weight:bold;\">Success</td>" +
                "<td style = \"border:1px black solid; padding:5px; font-weight:bold;\">Error</td>" +
                "</tr>"
                + "<tr>" +
                "<td style = \"border:1px black solid; padding:5px;\">{0}</td>" +
                "<td style = \"border:1px black solid; padding:5px;\">{1}</td>" +
                "<td style = \"border:1px black solid; padding:5px;\">{2}</td>" +
                "<td style = \"border:1px black solid; padding:5px;\">{3}</td>" +
                "</tr>" +
                "</table>", sharePointJobName, totalCount, successCount, errorCount);

            email.sendMessage(emailSubject, body, true);
        }

        private static OTAuthentication AuthenticateUser(string username, string password)
        {
            try
            {
                // --------------------------------------------------------------------------
                // 1) Authenticate the user
                // --------------------------------------------------------------------------

                // Store the authentication token
                string authToken = null;

                // Create the Authentication service client
                using (AuthenticationClient authClient = new AuthenticationClient())
                {
                    // Call the AuthenticateUser() method to get an authentication token
                    //Console.Write("Authenticating User...");
                    authToken = authClient.AuthenticateUser(username, password);
                    //Console.WriteLine("Success!\n");
                }

                OTAuthentication otAuth = new OTAuthentication();
                otAuth.AuthenticationToken = authToken;
                return otAuth;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static string DecryptData(string inputText)
        {
            try
            {
                byte[] iv = { 7, 4, 5, 0, 0, 9, 1, 0, 2, 2, 0, 1, 0, 3, 5, 9 };
                // non-PRD
                byte[] secretKey = Convert.FromBase64String("kPrqBHB93W9dycTFnbJ7Qg+EGGnX0TSoL8yMZepG93Y=");

                byte[] result = null;
                byte[] byteData = Convert.FromBase64String(inputText);

                using (AesCryptoServiceProvider aesCSP = new AesCryptoServiceProvider())
                {
                    aesCSP.Mode = CipherMode.CBC;
                    aesCSP.Key = secretKey;
                    aesCSP.IV = iv;

                    ICryptoTransform decryptor = aesCSP.CreateDecryptor();

                    result = decryptor.TransformFinalBlock(byteData, 0, byteData.Length);
                }

                return Encoding.UTF8.GetString(result);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static long GetUploadFolderId(OTAuthentication otAuth, DocumentManagementClient docManClient,
            string fourthLevel, string fifthLevel, string sixthLevel, LogWriter logWriter)
        {
            try
            {
                // base folder id
                int parentID = rcubeBaseFolderId;

                // get node of NICE base folder
                var node = docManClient.GetNodeByName(ref otAuth, parentID, rcubeBaseFolderNICE);
                //Console.WriteLine("ID: {0}, Name: {1}", node.ID.ToString(), node.Name);

                // Get nodes within
                var options = new GetNodesInContainerOptions();
                options.MaxDepth = 3;
                options.MaxResults = int.MaxValue;
                var niceNodes = docManClient.GetNodesInContainer(ref otAuth, node.ID, options);

                // get fourth level id
                long fourthLevelId = 0;
                foreach (var niceNode in niceNodes)
                {
                    if (niceNode.Type.Equals("Folder") && niceNode.Name.Equals(fourthLevel))
                    {
                        //Console.WriteLine("Fourth Level Id: {0}, Name: {1}", niceNode.ID.ToString(), niceNode.Name);
                        fourthLevelId = niceNode.ID;
                        break;
                    }
                }

                // get node of fifth level folder (Year)
                var fifthLevelNode = docManClient.GetNodeByName(ref otAuth, fourthLevelId, fifthLevel);
                long fifthLevelId = 0;
                
                if (fifthLevelNode != null)
                {
                    fifthLevelId = fifthLevelNode.ID;
                    //Console.WriteLine("Exisiting Fifth Level Folder Id: {0}, Name: {1}", fifthLevelId.ToString(), fifthLevel);
                }
                else
                {
                    try
                    {
                        fifthLevelId = docManClient.CreateSimpleFolder(ref otAuth, fourthLevelId, fifthLevel);
                        // Console.WriteLine("New Fifth Level Folder Id: {0}, Name: {1}", fifthLevelId.ToString(), fifthLevel);
                    }
                    catch (Exception e)
                    {
                        if (e.Message.Contains("already exists"))
                        {
                            logWriter.log(e.Message);
                            logWriter.log("Retrieving folder id...");
                            fifthLevelNode = docManClient.GetNodeByName(ref otAuth, fourthLevelId, fifthLevel);
                            fifthLevelId = fifthLevelNode.ID;
                            logWriter.log("Found folder id: " + fifthLevelId);
                            throw e;
                        }
                        else
                        {
                            throw e;
                        }
                    }                   
                }

                // get node of sixth level folder
                var sixthLevelNode = docManClient.GetNodeByName(ref otAuth, fifthLevelId, sixthLevel);
                long sixthLevelId = 0;
                
                if (sixthLevelNode != null)
                {
                    sixthLevelId = sixthLevelNode.ID;
                    //Console.WriteLine("Existing Sixth Level Folder Id: {0}, Name: {1}", sixthLevelId.ToString(), sixthLevel);
                }
                else
                {
                    try
                    {
                        sixthLevelId = docManClient.CreateSimpleFolder(ref otAuth, fifthLevelId, sixthLevel);
                        //Console.WriteLine("New Sixth Level Folder Id: {0}, Name: {1}", sixthLevelId.ToString(), sixthLevel);
                    }
                    catch (Exception e)
                    {
                        if (e.Message.Contains("already exists"))
                        {
                            logWriter.log(e.Message);
                            logWriter.log("Retrieving folder id...");
                            sixthLevelNode = docManClient.GetNodeByName(ref otAuth, fifthLevelId, sixthLevel);
                            sixthLevelId = sixthLevelNode.ID;
                            logWriter.log("Found folder id: " + sixthLevelId);
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }

                return sixthLevelId;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static long UploadFile(OTAuthentication otAuth, DocumentManagementClient docManClient, FileAtts attachment, byte[] byteArray, long parent_id)
        {
            try
            {

                // Store the context ID for the upload
                string contextID = null;

                // Call the CreateDocumentContext() method to create the context ID
                //Console.Write("Generating Context ID...");

                contextID = docManClient.CreateSimpleDocumentContext(ref otAuth, parent_id, attachment.FileName);
                //Console.WriteLine("Success!\nContext ID:{0}", contextID);

                // --------------------------------------------------------------------------
                // 3) Upload the file
                // --------------------------------------------------------------------------
                long documentID = 0;
                // Create a file stream to upload the file with
                //FileStream fileStream = null;
                using (Stream stream = new MemoryStream(byteArray))
                {
                    // Create the ContentService service client
                    using (ContentServiceClient contentServiceClient = new ContentServiceClient())
                    {
                        // Call the UploadContent() method to upload the file

                        //Console.Write("Uploading file...");
                        string objectID = contentServiceClient.UploadContent(ref otAuth, contextID, attachment, stream);
                        //Console.WriteLine("Success!");
                        //Console.WriteLine("New document uploaded with ID = {0}\n", objectID);
                        documentID = long.Parse(objectID);
                    }
                }
                return documentID;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //private static void DownloadFile(OTAuthentication otAuth, long documentID)
        //{
        //    // Create the DocumentManagement service client
        //    DocumentManagementClient docManClient = new DocumentManagementClient();

        //    // Store the context ID for the download
        //    string contextID = null;

        //    // Call the GetVersionContentsContext() method to create the context ID
        //    try
        //    {
        //        //Console.Write("Generating context ID...");
        //        contextID = docManClient.GetVersionContentsContext(ref otAuth, documentID, 0);
        //        //Console.WriteLine("Success!\n");
        //    }
        //    catch (FaultException e)
        //    {
        //        //Console.WriteLine("Failed!");
        //        //Console.WriteLine("{0} : {1}\n", e.Code.Name, e.Message);
        //        return;
        //    }
        //    finally
        //    {
        //        // Always close the client
        //        docManClient.Close();
        //    }

        //    // Get the Download Stream
        //    Stream downloadStream = null;
        //    ContentServiceClient contentServiceClient = new ContentServiceClient();
        //    try
        //    {
        //        //Console.Write("Downloading file...");
        //        downloadStream = contentServiceClient.DownloadContent(ref otAuth, contextID);
        //    }
        //    finally
        //    {
        //        // Always close the client
        //        contentServiceClient.Close();
        //    }

        //    // Create a file stream to write the contents to the local file
        //    FileStream fileStream = null;
        //    string filePath = @"D:\NICE\Test Files\downloaded2.txt";
        //    try
        //    {
        //        fileStream = new FileStream(filePath, FileMode.Create);

        //        // Create a buffer to read the download stream into (buffer size = 4 KB)
        //        byte[] buffer = new byte[4096];

        //        // Keep track of the size of the file
        //        long fileSize = 0;

        //        // Read the contents from download stream and write them to the file stream
        //        for (int read = downloadStream.Read(buffer, 0, buffer.Length); read > 0; read = downloadStream.Read(buffer, 0, buffer.Length))
        //        {
        //            fileStream.Write(buffer, 0, read);
        //            fileSize += read;
        //        }

        //        //Console.WriteLine("Success!\n");
        //        //Console.WriteLine("Downloaded {0} bytes to {1}.\n", fileSize, filePath);
        //    }
        //    finally
        //    {
        //        // Always close the streams
        //        if (fileStream != null)
        //            fileStream.Close();
        //        if (downloadStream != null)
        //            downloadStream.Close();
        //    }
        //}

        private static Dictionary<string, long> GetCategoryIDMap(string username, string password)
        {
            HttpWebResponse searchResponse = null;
            Stream dataStream = null;
            StreamReader reader = null;

            try
            {
                //Create cookie container to store the llcookie.
                CookieContainer CC = new CookieContainer();

                //Create the HttpWebRequest object to log the user in.  Set the correct hostname and path for your environment in the URI object.
                HttpWebRequest loginRequest = (HttpWebRequest)HttpWebRequest.Create(new Uri(rcubeContentServiceURL + "/otcs/cs.exe"));

                //Set up the POST data.  User credentials can be adjusted as needed.
                var postData = "func=ll.login";
                postData += "&Username=" + username;
                postData += "&Password=" + password;
                var loginPost = Encoding.ASCII.GetBytes(postData);

                //Set up the required request parameters for the login POST.
                loginRequest.ContentLength = loginPost.Length;
                loginRequest.ContentType = "application/x-www-form-urlencoded";
                loginRequest.Method = "POST";
                loginRequest.CookieContainer = CC;
                using (var stream = loginRequest.GetRequestStream())
                {
                    stream.Write(loginPost, 0, loginPost.Length);
                }

                //Send the login POST.  This will deposit the required cookies in the cookie container.
                HttpWebResponse loginResponse = (HttpWebResponse)loginRequest.GetResponse();
                loginResponse.Close();

                /*Search API can also be used via the GET verb, which may be a suitable solution and doesn't require setting up as many parameters.  That would look like this:*/
                HttpWebRequest searchRequest = (HttpWebRequest)WebRequest.Create(new Uri(rcubeContentServiceURL + "/otcs/cs.exe?func=search.getCategoryVolume&outputformat=xml"));
                searchRequest.ContentLength = 0;
                searchRequest.Method = "GET";
                searchRequest.CookieContainer = CC;
                searchResponse = (HttpWebResponse)searchRequest.GetResponse();

                //Stream search results to string, ready to parse.
                dataStream = searchResponse.GetResponseStream();
                reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();

                //Output to console for sanity checking.
                //Console.WriteLine(responseFromServer);

                //Output to XmlReader
                string categoryVolumeURL = findXml(responseFromServer, "CategoryVolume", "URL");

                /*Search API can also be used via the GET verb, which may be a suitable solution and doesn't require setting up as many parameters.  That would look like this:*/
                searchRequest = (HttpWebRequest)WebRequest.Create(new Uri(rcubeContentServiceURL + "/otcs/cs.exe" + categoryVolumeURL + "&outputformat=xml"));
                searchRequest.ContentLength = 0;
                searchRequest.Method = "GET";
                searchRequest.CookieContainer = CC;
                searchResponse = (HttpWebResponse)searchRequest.GetResponse();

                //Stream search results to string, ready to parse.
                dataStream = searchResponse.GetResponseStream();
                reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                Dictionary<string, long> categoryIDMap = new Dictionary<string, long>();

                string niceCategoryID = findXml(responseFromServer, "llnode", "name", rcubeCategoryNICE, "id");
                categoryIDMap.Add(rcubeCategoryNICE, long.Parse(niceCategoryID));
                string recordCategoryID = findXml(responseFromServer, "llnode", "name", rcubeCategoryRecord, "id");
                categoryIDMap.Add(rcubeCategoryRecord, long.Parse(recordCategoryID));

                return categoryIDMap;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                //Close off remaining objects.
                if (reader != null)
                {
                    reader.Close();
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                }
                if (searchResponse != null)
                {
                    searchResponse.Close();
                }
            }
        }

        private static string findXml(string xmlString, string elementName, string attributeName)
        {
            try
            {
                return findXml(xmlString, elementName, attributeName, null, null);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static string findXml(string xmlString, string elementName, string attributeName, string attributeValue, string targetAttribute)
        {
            try
            {
                string targetAttributeValue = "";

                //{"For security reasons DTD is prohibited in this XML document. 
                //To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse 
                //and pass the settings into XmlReader.Create method."}
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString), settings);
                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (attributeValue == null)
                            {
                                if (xmlReader.Name.Equals(elementName))
                                {
                                    //Console.WriteLine("Element Name:" + elementName);
                                    //Console.WriteLine("Attribute Name: " + attributeName + " Value: " + xmlReader.GetAttribute(attributeName));
                                    targetAttributeValue = xmlReader.GetAttribute(attributeName);
                                }
                            }
                            else
                            {
                                if (xmlReader.Name.Equals(elementName))
                                {
                                    if (xmlReader.GetAttribute(attributeName).Equals(attributeValue))
                                    {
                                        //Console.WriteLine("Element Name:" + elementName);
                                        //Console.WriteLine("Attribute Name: " + attributeName + " Value: " + xmlReader.GetAttribute(attributeName));
                                        //Console.WriteLine("Attribute Name: " + targetAttribute + " Value: " + xmlReader.GetAttribute(targetAttribute));
                                        targetAttributeValue = xmlReader.GetAttribute(targetAttribute);
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                return targetAttributeValue;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}